(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-new-course-new-course-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/new-course/new-course/new-course.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/new-course/new-course/new-course.component.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"row\" style=\"display:flex; margin-left: 18%; width: 130%;\">\r\n    <div class=\"col-sm-6\">\r\n      <div class=\"card\">\r\n        \r\n<form [formGroup]=\"formGroup\" class=\"text-center border border-light p-5\" action=\"#!\">\r\n\r\n    <p class=\"h2 mb-4\"><font size=\"6\">Nuovo Corso</font></p>\r\n\r\n    <div class=\"form-group ui-g-12 ui-md-6\">\r\n            <label for=\"nome\"><font size=\"3\">Tipo*</font></label><br>\r\n            <p-dropdown\r\n            *ngIf=(listaTipi)\r\n            [options] = \"descrizioneTipo\"\r\n            optionLabel=\"label\"\r\n            formControlName=\"tipo\"\r\n            ></p-dropdown>\r\n    </div>\r\n    <div class=\"form-group ui-g-12 ui-md-6\">\r\n            <label for=\"nome\"><font size=\"3\">Tipologia*</font></label><br>\r\n            <p-dropdown\r\n            *ngIf=(listaTipologie)\r\n            [options] = \"descrizioneTipologia\"\r\n            optionLabel=\"label\"\r\n            formControlName=\"tipologia\"\r\n            ></p-dropdown>\r\n    </div>\r\n    <div class=\"form-group ui-g-12 ui-md-12\">\r\n        <label for=\"nome\"><font size=\"3\">Descrizione*</font></label>\r\n        <input type=\"text\" class=\"form-control\" placeholder=\"Descrizione*\" formControlName=\"descrizione\" required>\r\n    </div>\r\n    <div class=\"form-group ui-g-12 ui-md-6\">\r\n        <label for=\"nome\"><font size=\"3\">Edizione*</font></label>\r\n        <input type=\"text\" class=\"form-control\" placeholder=\"Edizione*\" formControlName=\"edizione\" required>\r\n    </div>\r\n    <div class=\"form-group ui-g-12 ui-md-6\">\r\n        <label for=\"nome\"><font size=\"3\">Previsto*</font></label>\r\n        <input type=\"text\" class=\"form-control\" placeholder=\"Previsto*\" formControlName=\"previsto\" required>\r\n    </div>\r\n    <div class=\"form-group ui-g-12 ui-md-6\">\r\n        <label for=\"nome\"><font size=\"3\">Erogato*</font></label>\r\n        <input type=\"text\" class=\"form-control\" placeholder=\"Erogato*\" formControlName=\"erogato\" required>\r\n    </div>\r\n    <div class=\"form-group ui-g-12 ui-md-2\">\r\n        <label for=\"nome\"><font size=\"3\">Durata*</font></label>\r\n        <input type=\"text\" class=\"form-control\" placeholder=\"Durata*\" formControlName=\"durata\" required style=\"width: 130%;\">\r\n    </div>\r\n    <div class=\"form-group ui-g-12 ui-md-4\">\r\n        <label for=\"nome\"><font size=\"3\">Misura*</font></label><br>\r\n        <p-dropdown\r\n        *ngIf=(listaMisure)\r\n        [options] = \"descrizioneMis\"\r\n        optionLabel=\"label\"\r\n        formControlName=\"misura\"\r\n        ></p-dropdown>\r\n</div>\r\n<div class=\"form-group ui-g-12 ui-md-4\">\r\n    \r\n</div>\r\n    <div class=\"form-group ui-g-12 ui-md-4\">\r\n            <label for=\"nome\"><font size=\"3\">Data erogazione</font></label><br>\r\n            <p-calendar formControlName=\"dataErogazione\" required dateFormat=\"dd/mm/yy\" showButtonBar=\"true\" placeholder=\"Seleziona Data\" [showIcon]=\"true\"></p-calendar>\r\n    </div>\r\n    <div class=\"form-group ui-g-12 ui-md-4\">\r\n        <label for=\"nome\"><font size=\"3\">Data chiusura</font></label><br>\r\n        <p-calendar formControlName=\"dataChiusura\" required dateFormat=\"dd/mm/yy\" showButtonBar=\"true\" placeholder=\"Seleziona Data\" [showIcon]=\"true\"></p-calendar>\r\n    </div>\r\n    <div class=\"form-group ui-g-12 ui-md-4\">\r\n        <label for=\"nome\"><font size=\"3\">Data censimento</font></label><br>\r\n        <p-calendar formControlName=\"dataCensimento\" required dateFormat=\"dd/mm/yy\" showButtonBar=\"true\" placeholder=\"Seleziona Data\" [showIcon]=\"true\"></p-calendar>\r\n    </div>\r\n    <div class=\"form-group ui-g-12 ui-md-6\">\r\n        <label for=\"nome\"><font size=\"3\">Luogo</font></label>\r\n        <input type=\"text\" class=\"form-control\" placeholder=\"Luogo\" formControlName=\"luogo\" required>\r\n    </div>\r\n    <div class=\"form-group ui-g-12 ui-md-6\">\r\n        <label for=\"nome\"><font size=\"3\">Ente</font></label>\r\n        <input type=\"text\" class=\"form-control\" placeholder=\"Ente\" formControlName=\"ente\" required>\r\n    </div>\r\n    <div class=\"form-group ui-g-12 ui-md-6\">\r\n        <label for=\"nome\"><font size=\"3\">Interno</font></label>\r\n        <input type=\"text\" class=\"form-control\" placeholder=\"Interno\" formControlName=\"interno\" required>\r\n    </div>\r\n    <div class=\"form-group ui-g-12 ui-md-12\" style=\"width: 60%; margin-left: 20%;\">\r\n            <label for=\"nome\"><font size=\"3\">Note</font></label>\r\n            <textarea rows=\"5\" cols=\"30\" pInputTextarea class=\"form-control\" placeholder=\"Aggiungi note...\" formControlName=\"note\" required></textarea>\r\n    </div>\r\n</form>\r\n<!-- Default form contact -->\r\n        \r\n        <div><br>\r\n    <button class=\"btn btn-info btn-block\" (click)=\"conferma()\" type=\"button\" style=\"width: 25%; margin-left: 37.5%;\">\r\n        <i class=\"fa fa-check mr-2\"\r\n        aria-hidden=\"true\"></i>Conferma\r\n    </button><br></div></div>\r\n        </div>\r\n      </div>\r\n");

/***/ }),

/***/ "./src/app/views/new-course/new-course-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/views/new-course/new-course-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: NewCourseRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCourseRoutingModule", function() { return NewCourseRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _new_course_new_course_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./new-course/new-course.component */ "./src/app/views/new-course/new-course/new-course.component.ts");




var routes = [{
        path: '',
        component: _new_course_new_course_component__WEBPACK_IMPORTED_MODULE_3__["NewCourseComponent"],
        data: {
            title: 'new-course'
        }
    },
];
var NewCourseRoutingModule = /** @class */ (function () {
    function NewCourseRoutingModule() {
    }
    NewCourseRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], NewCourseRoutingModule);
    return NewCourseRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/new-course/new-course.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/views/new-course/new-course.module.ts ***!
  \*******************************************************/
/*! exports provided: NewCourseModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCourseModule", function() { return NewCourseModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _new_course_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./new-course-routing.module */ "./src/app/views/new-course/new-course-routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _new_course_new_course_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./new-course/new-course.component */ "./src/app/views/new-course/new-course/new-course.component.ts");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/fesm5/primeng-dropdown.js");
/* harmony import */ var primeng_calendar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/calendar */ "./node_modules/primeng/fesm5/primeng-calendar.js");
/* harmony import */ var primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/inputtextarea */ "./node_modules/primeng/fesm5/primeng-inputtextarea.js");
/* harmony import */ var primeng_spinner__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/spinner */ "./node_modules/primeng/fesm5/primeng-spinner.js");










var NewCourseModule = /** @class */ (function () {
    function NewCourseModule() {
    }
    NewCourseModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_new_course_new_course_component__WEBPACK_IMPORTED_MODULE_5__["NewCourseComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _new_course_routing_module__WEBPACK_IMPORTED_MODULE_3__["NewCourseRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                primeng_dropdown__WEBPACK_IMPORTED_MODULE_6__["DropdownModule"],
                primeng_calendar__WEBPACK_IMPORTED_MODULE_7__["CalendarModule"],
                primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_8__["InputTextareaModule"],
                primeng_spinner__WEBPACK_IMPORTED_MODULE_9__["SpinnerModule"]
            ]
        })
    ], NewCourseModule);
    return NewCourseModule;
}());



/***/ }),

/***/ "./src/app/views/new-course/new-course/new-course.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/views/new-course/new-course/new-course.component.css ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL25ldy1jb3Vyc2UvbmV3LWNvdXJzZS9uZXctY291cnNlLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/views/new-course/new-course/new-course.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/views/new-course/new-course/new-course.component.ts ***!
  \*********************************************************************/
/*! exports provided: NewCourseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCourseComponent", function() { return NewCourseComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_services_course_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../core/services/course.service */ "./src/app/core/services/course.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var NewCourseComponent = /** @class */ (function () {
    function NewCourseComponent(fb, courseService, router) {
        this.fb = fb;
        this.courseService = courseService;
        this.router = router;
    }
    NewCourseComponent.prototype.ngOnInit = function () {
        this.getMisura();
        this.getTipo();
        this.getTipologia();
        this.formGroup = this.fb.group({
            tipo: [''],
            tipologia: [''],
            descrizione: [''],
            edizione: [''],
            previsto: [''],
            erogato: [''],
            durata: [''],
            misura: [''],
            note: [''],
            luogo: [''],
            ente: [''],
            dataErogazione: [''],
            dataChiusura: [''],
            dataCensimento: [''],
            interno: [''],
            tipoId: [''],
            tipologiaId: [''],
            misuraId: ['']
        });
    };
    NewCourseComponent.prototype.conferma = function () {
        var _this = this;
        this.corsoAgg = this.formGroup.value;
        if (this.corsoAgg) {
            this.corsoAgg.tipo = this.corsoAgg.tipo.value.element;
            this.corsoAgg.tipologia = this.corsoAgg.tipologia.value.element;
            this.corsoAgg.misura = this.corsoAgg.misura.value.element;
            this.courseService.add(this.corsoAgg).subscribe(function () {
                _this.router.navigate(['/new-course-users']);
            });
        }
    };
    NewCourseComponent.prototype.getMisura = function () {
        var _this = this;
        this.descrizioneMis = [
            { label: 'Seleziona Misura', value: null },
        ];
        this.courseService.getMisura().subscribe(function (resp) {
            _this.listaMisure = resp.response;
            if (_this.listaMisure) {
                _this.listaMisure.forEach(function (element) {
                    _this.descrizioneMis.push({ label: element.descrizione, value: { element: element } });
                });
            }
        });
    };
    NewCourseComponent.prototype.getTipo = function () {
        var _this = this;
        this.descrizioneTipo = [
            { label: 'Seleziona Tipo', value: null },
        ];
        this.courseService.getTipo().subscribe(function (resp) {
            _this.listaTipi = resp.response;
            if (_this.listaTipi) {
                _this.listaTipi.forEach(function (element) {
                    _this.descrizioneTipo.push({ label: element.descrizione, value: { element: element } });
                });
            }
        });
    };
    NewCourseComponent.prototype.getTipologia = function () {
        var _this = this;
        this.descrizioneTipologia = [
            { label: 'Seleziona Tipologia', value: null },
        ];
        this.courseService.getTipologia().subscribe(function (resp) {
            _this.listaTipologie = resp.response;
            if (_this.listaTipologie) {
                _this.listaTipologie.forEach(function (element) {
                    _this.descrizioneTipologia.push({ label: element.descrizione, value: { element: element } });
                });
            }
        });
    };
    NewCourseComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
        { type: _core_services_course_service__WEBPACK_IMPORTED_MODULE_2__["CourseService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
    ]; };
    NewCourseComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-new-course',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./new-course.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/new-course/new-course/new-course.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./new-course.component.css */ "./src/app/views/new-course/new-course/new-course.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _core_services_course_service__WEBPACK_IMPORTED_MODULE_2__["CourseService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], NewCourseComponent);
    return NewCourseComponent;
}());



/***/ })

}]);
//# sourceMappingURL=views-new-course-new-course-module.js.map