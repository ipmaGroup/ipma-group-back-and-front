(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-employees-employees-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/employees/employees/employees.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/employees/employees/employees.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div>\r\n  <p-dropdown\r\n    [options]=\"tipo2\"\r\n    [(ngModel)]=\"filtroSelezionato\"\r\n    placeholder=\"Filter by type\"\r\n    optionLabel=\"name\"\r\n    [showClear]=\"true\"></p-dropdown>\r\n  </div>\r\n  <div class=\"pull-right mb-2\">\r\n      <button (click)=\"filtraAvanzato()\" class=\"btn btn-light \">\r\n        <i class=\"fa fa-gear\"></i>\r\n      </button>\r\n      <button class=\"btn btn-warning\" (click)=\"removeFilter()\">\r\n        <i class=\"fa fa-trash\"></i>\r\n      </button>\r\n    </div>\r\n  <form [formGroup]=\"formGroup\" style=\"display: flex;\">\r\n  <!--     <input \r\n      style=\"border-radius:3px;\r\n      border: 1px\t#c0c0c0 solid ;\r\n      padding: 6.006px ;\r\n      display: inline;\"\r\n      type=\"text\"\r\n      formControlName=\"soggFiltrato\"\r\n      class=\"mt-2 mr-1\"\r\n      required\r\n    />\r\n    <button (click)=\"filtra()\" class=\"btn btn-light\" style=\"position: relative;\">Filter</button>\r\n\r\n    <button (click)=\"filtraAvanzato()\" class=\"btn btn-light \">\r\n      <i class=\"fa fa-gear\"></i>\r\n    </button>\r\n\r\n    <button class=\"btn btn-warning mt-3\" (click)=\"removeFilter()\">\r\n        <i class=\" fa fa-trash\"></i>\r\n      </button> -->\r\n\r\n      <div class=\"input-group mb-1 mt-1\" style=\"width: 16%;\">\r\n        <input type=\"text\" class=\"form-control\"formControlName=\"soggFiltrato\" style=\"border: 1px solid #cacaca;\">\r\n        <div class=\"input-group-append\">\r\n          <button (click)=\"filtra()\"class=\"btn btn-dark\" type=\"button\" id=\"button-addon2\" >\r\n          <i class=\"fa fa-filter\"></i>\r\n          </button>\r\n        </div>\r\n      </div>\r\n    \r\n  </form>\r\n\r\n<table class=\"table table-striped mt-3\">\r\n\r\n<thead class=\"thead-dark\">\r\n\r\n  <th scope=\"col\">first name</th>\r\n  <th scope=\"col\">surname</th>\r\n  <th scope=\"col\">freshman</th>\r\n  <th scope=\"col\">society</th>\r\n  <th scope=\"col\"></th>\r\n\r\n</thead>\r\n  <tr *ngFor=\"let valore of lista\" style=\"width: 100%;\">\r\n   \r\n   \r\n    <td>{{ valore.nome }}</td>\r\n    <td>{{ valore.cognome }}</td>\r\n    <td>{{ valore.matricola }}</td>\r\n    <td>{{ valore.societa?.ragioneSociale }}</td>\r\n    <td> \r\n      <button\r\n        pButton\r\n        class=\"btn btn-info mr-2\"\r\n        type=\"button\"\r\n        (click)=\"show(valore)\">\r\n        <i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i>\r\n      </button>\r\n    </tr>\r\n</table>\r\n<button type=\"button\" class=\"btn btn-primary\" (click)=\"dietro()\">\r\n  <i class=\"pi pi-backward\"></i>\r\n</button>\r\n<font class=\"ml-2 mr-2\" style=\"font-weight: bold;\">{{ pagina }}</font>\r\n<button type=\"button\" class=\"btn btn-primary\" (click)=\"avanti()\">\r\n  <i class=\"pi pi-forward\"></i>\r\n</button>\r\n\r\n<p-sidebar\r\n  [(visible)]=\"visibleSidebar5\"\r\n  position=\"right\"\r\n  [baseZIndex]=\"100000\"\r\n  [fullScreen]=\"true\"\r\n  [style]=\"{'overflow-y': 'scroll'}\"\r\n  \r\n>\r\n  <div>\r\n      <div class=\"text-center\">\r\n          <h2>employees details</h2>\r\n      </div>\r\n    <div class=\"row justify-content-center\" >\r\n    <table class=\"table table-striped mt-3 table-hover\" style=\"width: 50%; \">\r\n        <tbody >\r\n          <tr>\r\n            <td>Id:</td>\r\n            <td> {{ utCorr?.dipendenteId }} </td>\r\n          </tr>\r\n          <tr>\r\n            <td>freshman:</td>\r\n            <td>{{utCorr?.matricola }}</td>\r\n          </tr>\r\n          <tr>\r\n            <td> society:</td>\r\n            <td>{{ utCorr?.societa?.ragioneSociale }}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>surname:</td>\r\n            <td>{{ utCorr?.cognome }}</td>\r\n          </tr>\r\n          <tr>\r\n            <td> first name:</td>\r\n            <td>{{ utCorr?.nome }}</td>\r\n          </tr>\r\n          <tr>\r\n              <td> gender:</td>\r\n              <td>{{ utCorr?.sesso }}</td>\r\n            </tr>\r\n            <tr>\r\n                <td>  date of birth:</td>\r\n                <td>{{ utCorr?.dataNascita }}</td>\r\n              </tr>\r\n              <tr>\r\n                  <td>  birth place:</td>\r\n                  <td> {{ utCorr?.luogoDiNascita }}</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>  marital status:</td>\r\n                    <td>{{ utCorr?.statoCivile }}</td>\r\n                  </tr>\r\n                  <tr>\r\n                      <td>  educational qualification:</td>\r\n                      <td>{{ utCorr?.titoloStudio }}</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td> achieved at:</td>\r\n                        <td>{{ utCorr?.conseguitoPresso }}</td>\r\n                      </tr>\r\n                      <tr>\r\n                          <td>  Year Graduated:</td>\r\n                          <td>{{ utCorr?.annoConseguimento }}</td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td> Employee type: </td>\r\n                            <td>{{ utCorr?.tipoDipendente }}</td>\r\n                          </tr>\r\n                          <tr>\r\n                              <td> qualification:</td>\r\n                              <td> {{ utCorr?.qualifica }}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td> level:</td>\r\n                                <td>{{ utCorr?.livello }}</td>\r\n                              </tr>\r\n                              <tr>\r\n                                  <td>  assumption date:</td>\r\n                                  <td>{{ utCorr?.dataAssunzione }}</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td> area manager:</td>\r\n                                    <td>{{ utCorr?.responsabileArea }}</td>\r\n                                  </tr>\r\n                                  <tr>\r\n                                      <td>  Resource manager:</td>\r\n                                      <td>{{ utCorr?.responsabileRisorsa }}</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td> End of relationship date:</td>\r\n                                        <td>{{ utCorr?.dataFineRapporto }}</td>\r\n                                      </tr>\r\n                                      <tr>\r\n                                          <td>  Contract Expiration date:</td>\r\n                                          <td>{{ utCorr?.dataScadenzaContratto }}</td>\r\n                                        </tr>\r\n          </tbody>\r\n    </table>\r\n  </div>\r\n    <div class=\"row justify-content-center\">\r\n    <table class=\"table table-light mt-3\" style=\"width: 50%; \">\r\n      <thead class=\"bg-info\">\r\n      <th scope=\"col\">description course</th>\r\n      <th scope=\"col\">tipology</th>\r\n      <th scope=\"col\">type</th>\r\n    </thead>\r\n      <tr *ngFor=\"let valore of listaAllCourses\">\r\n        <!--  <td>{{ valore.docenteId }}</td> -->\r\n        <td>{{ valore?.descrizione }}</td>\r\n        <td>{{ valore?.tipologia?.descrizione }}</td>\r\n        <td>{{ valore?.tipo?.descrizione }}</td>\r\n      </tr>\r\n    </table>\r\n  </div>\r\n</div>\r\n</p-sidebar>\r\n\r\n<!-- <p-sidebar\r\n  [(visible)]=\"visibleSidebartop\"\r\n  position=\"top\"\r\n  [baseZIndex]=\"100000\"\r\n  [style]=\"{ height: '100%' }\"\r\n>\r\n  <div class=\"advanced-form\">\r\n    <form [formGroup]=\"formGroup2\">\r\n      employees Id:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"dipendenteId\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      freshman:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"matricola\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      surname:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"cognome\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      first name:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"nome\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      gender:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"sesso\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      date of birth:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"dataNascita\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      birth place:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"luogoNascita\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      marital status:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"statoCivile\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      educational qualification:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"titoloStudio\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      achieved at:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"conseguitoPresso\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      Year Graduated:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"annoConseguimento\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      Employee type:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"tipoDipendente\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      qualification:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"qualifica\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      level:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"livello\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      assumption date:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"dataAssunzione\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      Resource manager:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"responsabileRisorsa\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      Area manager:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"responsabileArea\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      End of relationship date:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"dataFineRapporto\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      Contract Expiration date:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"dataScadenzaContratto\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      <button (click)=\"filtraA()\" class=\"btn btn-light\">filter</button>\r\n    </form>\r\n  </div>\r\n</p-sidebar>\r\n -->\r\n\r\n <p-sidebar [(visible)]=\"visibleSidebartop\" position=\"top\" [baseZIndex]=\"100000\" [fullScreen]=\"true\"\r\n [style]=\"{'overflow-y': 'scroll'}\">\r\n <div class=\"row justify-content-center\">\r\n   <h2>Advanced Filter</h2>\r\n </div>\r\n\r\n <div class=\"advanced-form\">\r\n   <form [formGroup]=\"formGroup2\">\r\n     <div class=\"form-group\">\r\n       <input type=\"text\" class=\"form-control\" formControlName=\"dipendenteId\" required placeholder=\"dipendenteId\" />\r\n     </div>\r\n     <div class=\"form-group\">\r\n       <input type=\"text\" class=\"form-control\" formControlName=\"matricola\" required placeholder=\"matricola\" />\r\n     </div>\r\n     <div class=\"form-group\">\r\n       <input type=\"text\" class=\"form-control\" formControlName=\"cognome\" c required placeholder=\"cognome\" />\r\n     </div>\r\n     <div class=\"form-group\">\r\n       <input type=\"text\" class=\"form-control\" formControlName=\"nome\" required placeholder=\"nome\" />\r\n     </div>\r\n     <div class=\"form-group\">\r\n       <input type=\"text\" class=\"form-control\" formControlName=\"sesso\" required placeholder=\"sesso\" />\r\n     </div>\r\n     <div class=\"form-group\">\r\n       <input type=\"text\" class=\"form-control\" formControlName=\"dataNascita\" required placeholder=\"dataNascita\" />\r\n     </div>\r\n     <div class=\"form-group\">\r\n       <input type=\"text\" class=\"form-control\" formControlName=\"luogoNascita\" required placeholder=\"luogoNascita\" />\r\n     </div>\r\n\r\n     <div class=\"form-group\">\r\n       <input type=\"text\" class=\"form-control\" formControlName=\"statoCivile\" required placeholder=\"statoCivile\" />\r\n     </div>\r\n     <div class=\"form-group\">\r\n       <input type=\"text\" class=\"form-control\" formControlName=\"titoloStudio\" required placeholder=\"titoloStudio\" />\r\n     </div>\r\n     <div class=\"form-group\">\r\n       <input type=\"text\" class=\"form-control\" formControlName=\"conseguitoPresso\" required\r\n         placeholder=\"conseguitoPresso\" />\r\n     </div>\r\n     <div class=\"form-group\">\r\n       <input type=\"text\" class=\"form-control\" formControlName=\"annoConseguimento\" required placeholder=\"annoConseguimento\" />\r\n     </div>\r\n     <div class=\"form-group\">\r\n       <input type=\"text\" class=\"form-control\" formControlName=\"tipoDipendente\" required\r\n         placeholder=\"tipoDipendente\" />\r\n     </div>\r\n     <div class=\"form-group\">\r\n       <input type=\"text\" class=\"form-control\" formControlName=\"qualifica\" required placeholder=\"qualifica\" />\r\n     </div>\r\n     <div class=\"form-group\">\r\n       <input type=\"text\" class=\"form-control\" formControlName=\"livello\" required placeholder=\"livello\" />\r\n     </div>\r\n     <div class=\"form-group\">\r\n       <input type=\"text\" class=\"form-control\" formControlName=\"dataAssunzione\" required placeholder=\"dataAssunzione\" />\r\n     </div>\r\n     <div class=\"form-group\">\r\n       <input type=\"text\" class=\"form-control\" formControlName=\"responsabileRisorsa\" required placeholder=\"responsabileRisorsa\" />\r\n     </div>\r\n     <div class=\"form-group\">\r\n        <input type=\"text\" class=\"form-control\" formControlName=\"responsabileArea\" required placeholder=\"responsabileArea\" />\r\n      </div>\r\n      <div class=\"form-group\">\r\n          <input type=\"text\" class=\"form-control\" formControlName=\"dataFineRapporto\" required placeholder=\"dataFineRapporto\" />\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input type=\"text\" class=\"form-control\" formControlName=\"dataScadenzaContratto\" required placeholder=\"dataScadenzaContratto\" />\r\n          </div>\r\n     <div class=\"form-group\">\r\n       <button (click)=\"filtraA()\" class=\"btn btn-info btn-lg btn-block\">filter</button>\r\n     </div>\r\n   </form>\r\n </div>\r\n</p-sidebar>\r\n");

/***/ }),

/***/ "./src/app/core/services/employees.services.ts":
/*!*****************************************************!*\
  !*** ./src/app/core/services/employees.services.ts ***!
  \*****************************************************/
/*! exports provided: EmployeesServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeesServices", function() { return EmployeesServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api.service */ "./src/app/core/services/api.service.ts");
/* harmony import */ var _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/enviroment */ "./src/app/shared/enviroment.ts");




var EmployeesServices = /** @class */ (function () {
    function EmployeesServices(api) {
        this.api = api;
        this.pathFilter = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.employeesControllerFilter;
        this.path = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.employeesController;
        this.listSoggetti = [];
    }
    EmployeesServices.prototype.getAll = function () {
        return this.api.get(this.path);
    };
    EmployeesServices.prototype.getById = function (ut, inizio, fine) {
        return this.api.getById(this.pathFilter + inizio + "..." + fine, ut);
    };
    EmployeesServices.prototype.getAllPage = function (primo, secondo) {
        return this.api.getByPage(this.path, primo, secondo);
    };
    EmployeesServices.ctorParameters = function () { return [
        { type: _api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"] }
    ]; };
    EmployeesServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root",
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]])
    ], EmployeesServices);
    return EmployeesServices;
}());



/***/ }),

/***/ "./src/app/views/employees/employees-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/views/employees/employees-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: EmployeesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeesRoutingModule", function() { return EmployeesRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _employees_employees_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./employees/employees.component */ "./src/app/views/employees/employees/employees.component.ts");




var routes = [{
        path: '',
        component: _employees_employees_component__WEBPACK_IMPORTED_MODULE_3__["EmployeesComponent"],
        data: {
            title: 'Employees'
        }
    },];
var EmployeesRoutingModule = /** @class */ (function () {
    function EmployeesRoutingModule() {
    }
    EmployeesRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], EmployeesRoutingModule);
    return EmployeesRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/employees/employees.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/views/employees/employees.module.ts ***!
  \*****************************************************/
/*! exports provided: EmployeesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeesModule", function() { return EmployeesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _employees_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./employees-routing.module */ "./src/app/views/employees/employees-routing.module.ts");
/* harmony import */ var _employees_employees_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./employees/employees.component */ "./src/app/views/employees/employees/employees.component.ts");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/table */ "./node_modules/primeng/fesm5/primeng-table.js");
/* harmony import */ var primeng_sidebar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/sidebar */ "./node_modules/primeng/fesm5/primeng-sidebar.js");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/fesm5/primeng-dropdown.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");









var EmployeesModule = /** @class */ (function () {
    function EmployeesModule() {
    }
    EmployeesModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_employees_employees_component__WEBPACK_IMPORTED_MODULE_4__["EmployeesComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _employees_routing_module__WEBPACK_IMPORTED_MODULE_3__["EmployeesRoutingModule"],
                primeng_table__WEBPACK_IMPORTED_MODULE_5__["TableModule"],
                primeng_sidebar__WEBPACK_IMPORTED_MODULE_6__["SidebarModule"],
                primeng_dropdown__WEBPACK_IMPORTED_MODULE_7__["DropdownModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
            ],
        })
    ], EmployeesModule);
    return EmployeesModule;
}());



/***/ }),

/***/ "./src/app/views/employees/employees/employees.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/views/employees/employees/employees.component.css ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#input{\r\n    border-radius: 30%;\r\n}\r\n\r\n.form-control{\r\n    height: 40px;\r\n    box-shadow: none;\r\n    color: #969fa4;\r\n}\r\n\r\n.form-control:focus{\r\n    border-color: #5cb85c;\r\n}\r\n\r\n.form-control, .btn{        \r\n    border-radius: 3px;\r\n}\r\n\r\n.advanced-form{\r\n    width: 50%;\r\n    margin: 0 auto;\r\n    padding: 30px 0;\r\n}\r\n\r\n.advanced-form h2{\r\n    color: #636363;\r\n    margin: 0 0 15px;\r\n    position: relative;\r\n    text-align: center;\r\n}\r\n\r\n.advanced-form h2:before, .advanced-form h2:after{\r\n    content: \"\";\r\n    height: 2px;\r\n    width: 25%;\r\n    background: #d4d4d4;\r\n    position: absolute;\r\n    top: 50%;\r\n    z-index: 2;\r\n}\r\n\r\n.advanced-form h2:before{\r\n    left: 0;\r\n}\r\n\r\n.advanced-form h2:after{\r\n    right: 0;\r\n}\r\n\r\n.advanced-form .hint-text{\r\n    color: #999;\r\n    margin-bottom: 30px;\r\n    text-align: center;\r\n}\r\n\r\n.advanced-form form{\r\n    color: #999;\r\n    border-radius: 3px;\r\n    margin-bottom: 15px;\r\n    background: #f2f3f7;\r\n    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);\r\n    padding: 30px;\r\n}\r\n\r\n.advanced-form .form-group{\r\n    margin-bottom: 20px;\r\n}\r\n\r\n.advanced-form input[type=\"checkbox\"]{\r\n    margin-top: 3px;\r\n}\r\n\r\n.advanced-form .btn{        \r\n    font-size: 16px;\r\n    font-weight: bold;\t\t\r\n    min-width: 140px;\r\n    outline: none !important;\r\n}\r\n\r\n.advanced-form .row div:first-child{\r\n    padding-right: 10px;\r\n}\r\n\r\n.advanced-form .row div:last-child{\r\n    padding-left: 10px;\r\n}\r\n\r\n.advanced-form a{\r\n    color: #fff;\r\n    text-decoration: underline;\r\n}\r\n\r\n.advanced-form a:hover{\r\n    text-decoration: none;\r\n}\r\n\r\n.advanced-form form a{\r\n    color: #5cb85c;\r\n    text-decoration: none;\r\n}\r\n\r\n.advanced-form form a:hover{\r\n    text-decoration: underline;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvZW1wbG95ZWVzL2VtcGxveWVlcy9lbXBsb3llZXMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLFlBQVk7SUFDWixnQkFBZ0I7SUFDaEIsY0FBYztBQUNsQjs7QUFDQTtJQUNJLHFCQUFxQjtBQUN6Qjs7QUFDQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFDQTtJQUNJLFVBQVU7SUFDVixjQUFjO0lBQ2QsZUFBZTtBQUNuQjs7QUFDQTtJQUNJLGNBQWM7SUFDZCxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtBQUN0Qjs7QUFDQTtJQUNJLFdBQVc7SUFDWCxXQUFXO0lBQ1gsVUFBVTtJQUNWLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLFVBQVU7QUFDZDs7QUFDQTtJQUNJLE9BQU87QUFDWDs7QUFDQTtJQUNJLFFBQVE7QUFDWjs7QUFDQTtJQUNJLFdBQVc7SUFDWCxtQkFBbUI7SUFDbkIsa0JBQWtCO0FBQ3RCOztBQUNBO0lBQ0ksV0FBVztJQUNYLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLDBDQUEwQztJQUMxQyxhQUFhO0FBQ2pCOztBQUNBO0lBQ0ksbUJBQW1CO0FBQ3ZCOztBQUNBO0lBQ0ksZUFBZTtBQUNuQjs7QUFDQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsZ0JBQWdCO0lBQ2hCLHdCQUF3QjtBQUM1Qjs7QUFDQTtJQUNJLG1CQUFtQjtBQUN2Qjs7QUFDQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFDQTtJQUNJLFdBQVc7SUFDWCwwQkFBMEI7QUFDOUI7O0FBQ0E7SUFDSSxxQkFBcUI7QUFDekI7O0FBQ0E7SUFDSSxjQUFjO0lBQ2QscUJBQXFCO0FBQ3pCOztBQUNBO0lBQ0ksMEJBQTBCO0FBQzlCIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvZW1wbG95ZWVzL2VtcGxveWVlcy9lbXBsb3llZXMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiNpbnB1dHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDMwJTtcclxufVxyXG5cclxuLmZvcm0tY29udHJvbHtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICBjb2xvcjogIzk2OWZhNDtcclxufVxyXG4uZm9ybS1jb250cm9sOmZvY3Vze1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjNWNiODVjO1xyXG59XHJcbi5mb3JtLWNvbnRyb2wsIC5idG57ICAgICAgICBcclxuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcclxufVxyXG4uYWR2YW5jZWQtZm9ybXtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIHBhZGRpbmc6IDMwcHggMDtcclxufVxyXG4uYWR2YW5jZWQtZm9ybSBoMntcclxuICAgIGNvbG9yOiAjNjM2MzYzO1xyXG4gICAgbWFyZ2luOiAwIDAgMTVweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uYWR2YW5jZWQtZm9ybSBoMjpiZWZvcmUsIC5hZHZhbmNlZC1mb3JtIGgyOmFmdGVye1xyXG4gICAgY29udGVudDogXCJcIjtcclxuICAgIGhlaWdodDogMnB4O1xyXG4gICAgd2lkdGg6IDI1JTtcclxuICAgIGJhY2tncm91bmQ6ICNkNGQ0ZDQ7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDUwJTtcclxuICAgIHotaW5kZXg6IDI7XHJcbn1cdFxyXG4uYWR2YW5jZWQtZm9ybSBoMjpiZWZvcmV7XHJcbiAgICBsZWZ0OiAwO1xyXG59XHJcbi5hZHZhbmNlZC1mb3JtIGgyOmFmdGVye1xyXG4gICAgcmlnaHQ6IDA7XHJcbn1cclxuLmFkdmFuY2VkLWZvcm0gLmhpbnQtdGV4dHtcclxuICAgIGNvbG9yOiAjOTk5O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uYWR2YW5jZWQtZm9ybSBmb3Jte1xyXG4gICAgY29sb3I6ICM5OTk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgYmFja2dyb3VuZDogI2YyZjNmNztcclxuICAgIGJveC1zaGFkb3c6IDBweCAycHggMnB4IHJnYmEoMCwgMCwgMCwgMC4zKTtcclxuICAgIHBhZGRpbmc6IDMwcHg7XHJcbn1cclxuLmFkdmFuY2VkLWZvcm0gLmZvcm0tZ3JvdXB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcbi5hZHZhbmNlZC1mb3JtIGlucHV0W3R5cGU9XCJjaGVja2JveFwiXXtcclxuICAgIG1hcmdpbi10b3A6IDNweDtcclxufVxyXG4uYWR2YW5jZWQtZm9ybSAuYnRueyAgICAgICAgXHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcdFx0XHJcbiAgICBtaW4td2lkdGg6IDE0MHB4O1xyXG4gICAgb3V0bGluZTogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcbi5hZHZhbmNlZC1mb3JtIC5yb3cgZGl2OmZpcnN0LWNoaWxke1xyXG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcclxufVxyXG4uYWR2YW5jZWQtZm9ybSAucm93IGRpdjpsYXN0LWNoaWxke1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG59ICAgIFx0XHJcbi5hZHZhbmNlZC1mb3JtIGF7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG59XHJcbi5hZHZhbmNlZC1mb3JtIGE6aG92ZXJ7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbn1cclxuLmFkdmFuY2VkLWZvcm0gZm9ybSBhe1xyXG4gICAgY29sb3I6ICM1Y2I4NWM7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbn1cdFxyXG4uYWR2YW5jZWQtZm9ybSBmb3JtIGE6aG92ZXJ7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/views/employees/employees/employees.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/views/employees/employees/employees.component.ts ***!
  \******************************************************************/
/*! exports provided: EmployeesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeesComponent", function() { return EmployeesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../core/services/api.service */ "./src/app/core/services/api.service.ts");
/* harmony import */ var _core_services_employees_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../core/services/employees.services */ "./src/app/core/services/employees.services.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var paix__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! paix */ "./node_modules/paix/build/index.js");
/* harmony import */ var paix__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(paix__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _core_services_course_users_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../core/services/course_users.service */ "./src/app/core/services/course_users.service.ts");







var EmployeesComponent = /** @class */ (function () {
    function EmployeesComponent(api, employeesService, fb, courseUserServices) {
        this.api = api;
        this.employeesService = employeesService;
        this.fb = fb;
        this.courseUserServices = courseUserServices;
        this.pagina = 1;
        this.tipo2 = [];
        this.dati = 4;
        this.nPagine = 0;
        this.lista = [];
        this.lista2 = [];
        this.filt = [];
        this.listaAllCourses = [];
        this.tipo2 = [
            { name: "employees Id", code: "dipendenteId" },
            { name: "freshman", code: "matricola" },
            { name: "surname", code: "cognome" },
            { name: "first name", code: "nome" },
            { name: "gender", code: "sesso" },
            { name: "date of birth", code: "dataNascita" },
            { name: "birth place", code: "luogoNascita" },
            { name: "marital status", code: "statoCivile" },
            { name: "educational qualification", code: "titoloStudio" },
            { name: "achieved at", code: "conseguitoPresso" },
            { name: "Year Graduated", code: "annoConseguimento" },
            { name: "Employee type", code: "tipoDipendente" },
            { name: "qualification", code: "qualifica" },
            { name: "level", code: "livello" },
            { name: "assumption date", code: "dataAssunzione" },
            { name: "area manager", code: "responsabileRisorsa" },
            { name: "Resource manager", code: "responsabileArea" },
            { name: "End of relationship date", code: "dataFineRapporto" },
            { name: "Contract Expiration date", code: "dataScadenzaContratto" },
        ];
    }
    EmployeesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.formGroup = this.fb.group({
            soggFiltrato: [""],
        });
        this.formGroup2 = this.fb.group({
            dipendenteId: [""],
            matricola: [""],
            cognome: [""],
            nome: [""],
            sesso: [""],
            dataNascita: [""],
            luogoNascita: [""],
            statoCivile: [""],
            titoloStudio: [""],
            conseguitoPresso: [""],
            annoConseguimento: [""],
            tipoDipendente: [""],
            qualifica: [""],
            livello: [""],
            dataAssunzione: [""],
            responsabileRisorsa: [""],
            responsabileArea: [""],
            dataFineRapporto: [""],
            dataScadenzaContratto: [""],
        });
        this.employeesService.getAll().subscribe(function (resp) {
            _this.lista2 = resp.response;
        });
        this.loadAll();
    };
    EmployeesComponent.prototype.loadAll = function () {
        var _this = this;
        this.employeesService
            .getAllPage(this.pagina, this.dati)
            .subscribe(function (resp) {
            _this.lista = resp.response;
        });
    };
    EmployeesComponent.prototype.show = function (valore) {
        this.visibleSidebar5 = true;
        this.showCorses(valore);
        this.utCorr = valore;
    };
    EmployeesComponent.prototype.filtra = function () {
        var _this = this;
        var res = { nome: this.formGroup.value.soggFiltrato };
        var replacement = { nome: this.filtroSelezionato.code };
        var modificato = Object(paix__WEBPACK_IMPORTED_MODULE_5__["paix"])(res, replacement);
        this.employeesService
            .getById(modificato, this.pagina, this.dati)
            .subscribe(function (resp) {
            _this.lista = resp.response;
            _this.lista2 = resp.response;
        });
    };
    EmployeesComponent.prototype.avanti = function () {
        this.nPagine = this.lista2.length / this.dati;
        if (this.pagina < this.nPagine)
            this.pagina++;
        if (this.filtroSelezionato == null) {
            this.loadAll();
        }
        else {
            this.filtra();
        }
    };
    EmployeesComponent.prototype.dietro = function () {
        if (this.pagina - 1 > 0) {
            this.pagina--;
            if (this.filtroSelezionato == null) {
                this.loadAll();
            }
            else {
                this.filtra();
            }
        }
    };
    EmployeesComponent.prototype.removeFilter = function () {
        this.loadAll();
        this.filtroSelezionato = null;
        this.formGroup = this.fb.group({
            soggFiltrato: [""],
        });
        this.formGroup2 = this.fb.group({
            dipendenteId: [""],
            matricola: [""],
            cognome: [""],
            nome: [""],
            sesso: [""],
            dataNascita: [""],
            luogoNascita: [""],
            statoCivile: [""],
            titoloStudio: [""],
            conseguitoPresso: [""],
            annoConseguimento: [""],
            tipoDipendente: [""],
            qualifica: [""],
            livello: [""],
            dataAssunzione: [""],
            responsabileRisorsa: [""],
            responsabileArea: [""],
            dataFineRapporto: [""],
            dataScadenzaContratto: [""],
        });
    };
    EmployeesComponent.prototype.filtraAvanzato = function () {
        this.visibleSidebartop = true;
    };
    EmployeesComponent.prototype.filtraA = function () {
        var _this = this;
        var primo = [];
        var secondo = [];
        for (var field in this.formGroup2.controls) {
            if (this.formGroup2.controls[field].value != "") {
                primo.push(field);
                secondo.push(this.formGroup2.controls[field].value);
            }
        }
        var finale = Object(paix__WEBPACK_IMPORTED_MODULE_5__["paix"])(secondo, primo);
        var newFinale = Object.assign({}, finale);
        this.employeesService
            .getById(newFinale, this.pagina, this.dati)
            .subscribe(function (resp) {
            _this.lista = resp.response;
            _this.lista2 = resp.response;
        });
        this.visibleSidebartop = false;
    };
    EmployeesComponent.prototype.showCorses = function (valore) {
        var _this = this;
        this.listaAllCourses = [];
        this.courseUserServices.getAllCourseUsers().subscribe(function (element) {
            console.log(element.response);
            for (var i = 0; i < element.response.length; i++)
                if (element.response[i].dipendente.dipendenteId == valore.dipendenteId)
                    _this.listaAllCourses.push(element.response[i].corso);
        });
        console.log(this.listaAllCourses);
    };
    EmployeesComponent.ctorParameters = function () { return [
        { type: _core_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"] },
        { type: _core_services_employees_services__WEBPACK_IMPORTED_MODULE_3__["EmployeesServices"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
        { type: _core_services_course_users_service__WEBPACK_IMPORTED_MODULE_6__["CourseUsersService"] }
    ]; };
    EmployeesComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-employees",
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./employees.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/employees/employees/employees.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./employees.component.css */ "./src/app/views/employees/employees/employees.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_core_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            _core_services_employees_services__WEBPACK_IMPORTED_MODULE_3__["EmployeesServices"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _core_services_course_users_service__WEBPACK_IMPORTED_MODULE_6__["CourseUsersService"]])
    ], EmployeesComponent);
    return EmployeesComponent;
}());



/***/ })

}]);
//# sourceMappingURL=views-employees-employees-module.js.map