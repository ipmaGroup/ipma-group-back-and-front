(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-teachers-teachers-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/teachers/teachers/teachers.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/teachers/teachers/teachers.component.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"\">\r\n  <p-dropdown\r\n    [options]=\"tipo\"\r\n    [(ngModel)]=\"filtroSelezionato\"\r\n    placeholder=\"filter by type\"\r\n    class=\"mt-2\"\r\n    optionLabel=\"name\"\r\n    [showClear]=\"true\"\r\n    \r\n\r\n  ></p-dropdown>\r\n  </div>\r\n\r\n  <div class=\"pull-right mb-2\">\r\n      <button class=\"btn btn-success \" (click)=\"new()\">\r\n        <i class=\"fa fa-plus\"></i>\r\n      </button>\r\n   \r\n      <button (click)=\"filtraAvanzato()\" class=\"btn btn-light \">\r\n        <i class=\"fa fa-gear\"></i>\r\n      </button>\r\n      <button class=\"btn btn-warning\" (click)=\"removeFilter()\">\r\n        <i class=\"fa fa-trash\"></i>\r\n      </button>\r\n    </div>\r\n \r\n  <form [formGroup]=\"formGroup\" style=\"display: flex;\">\r\n<!--       <input\r\n      type=\"text\"\r\n      formControlName=\"soggFiltrato\"\r\n      class=\"mt-2 mr-1\"\r\n      required\r\n      style=\"border-radius:3px;\r\n      border: 1px\t#c0c0c0 solid ;\r\n      padding: 6.006px ;\r\n      display: inline;\"\r\n    />\r\n    <button (click)=\"filtra()\" class=\"btn btn-light\">filter</button>\r\n    \r\n\r\n      <button class=\"btn btn-warning mt-3\" (click)=\"removeFilter()\">\r\n          <i class=\"icon-trash icona\"></i><b>Remove</b>\r\n      </button>\r\n    \r\n      <button (click)=\"filtraAvanzato()\" class=\"btn btn-info mt-3\">\r\n       <b>Advanced</b>\r\n       \r\n         </button>\r\n    \r\n      <button class=\"btn btn-success mt-3\" (click)=\"new()\">\r\n          <i class=\"icon-plus icona\"></i>\r\n      </button> -->\r\n\r\n      <div class=\"input-group mt-1 mb-1\" style=\"width: 20%;\">\r\n          <input type=\"text\" formControlName=\"soggFiltrato\" style=\"border: 1px solid #cacaca;\">\r\n          <div class=\"input-group-append\">\r\n            <button (click)=\"filtra()\"class=\"btn btn-dark\" type=\"button\" id=\"button-addon2\" >\r\n            <i class=\"fa fa-filter\"></i>\r\n            </button>\r\n          </div>\r\n        </div>\r\n    \r\n  </form>\r\n  \r\n\r\n\r\n\r\n<table class=\"table table-striped mt-3\">\r\n\r\n  <thead class=\"thead-dark\">\r\n \r\n  <th scope=\"col\">title</th>\r\n  <th scope=\"col\">business name</th>\r\n  <th scope=\"col\">province</th>\r\n  <th scope=\"col\">Actions</th>\r\n</thead>\r\n  <tr *ngFor=\"let valore of lista\">\r\n   \r\n    <td>{{ valore.titolo }}</td>\r\n    <td>{{ valore.ragioneSociale }}</td>\r\n    <td>{{ valore.provincia }}</td>\r\n    <td>\r\n      <button class=\"btn btn-info mr-2\" type=\"button\" (click)=\"show(valore)\">\r\n        <i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i>\r\n      </button>\r\n\r\n      <button\r\n        class=\"btn btn-danger mr-2\"\r\n        (click)=\"confirm1(valore.docenteId)\"\r\n        type=\"button\"\r\n      >\r\n        <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\r\n        <p-confirmDialog\r\n          [style]=\"{ width: '50vw' }\"\r\n          [baseZIndex]=\"10\"\r\n          header=\"Conferma\"\r\n          key=\"conferma\"\r\n          closable=\"true\"\r\n          [transitionOptions]=\"'0ms'\"\r\n        ></p-confirmDialog>\r\n      </button>\r\n\r\n      <button class=\"btn btn-info mr-2\" (click)=\"edit(valore.docenteId)\">\r\n        <i class=\"fa fa-edit\" aria-hidden=\"true\"></i>\r\n      </button>\r\n    </td>\r\n  </tr>\r\n</table>\r\n<button type=\"button\" class=\"btn btn-primary\" (click)=\"dietro()\">\r\n  <i class=\"pi pi-backward\"></i>\r\n</button>\r\n<font class=\"ml-2 mr-2\" style=\"font-weight: bold;\">{{ pagina }}</font>\r\n<button type=\"button\" class=\"btn btn-primary\" (click)=\"avanti()\">\r\n  <i class=\"pi pi-forward\"></i>\r\n</button>\r\n\r\n<p-sidebar\r\n  [(visible)]=\"visibleSidebar5\"\r\n  position=\"right\"\r\n  [baseZIndex]=\"100000\"\r\n  [fullScreen]=\"true\"\r\n>\r\n\r\n<div>\r\n\r\n    <div class=\"text-center\">\r\n        <h2>Teachers details</h2>\r\n    </div>\r\n    \r\n    <div class=\"row justify-content-center\">\r\n     \r\n    <table class=\"table table-striped mt-3 table-hover\" style=\"width: 50%;\">\r\n      <tbody>\r\n        <tr>\r\n          <td>Id:</td>\r\n          <td>{{ utCorr?.docenteId }}</td>\r\n        </tr>\r\n        <tr>\r\n          <td>Title:</td>\r\n          <td>{{ utCorr?.titolo }}</td>\r\n        </tr>\r\n        <tr>\r\n          <td>Business:</td>\r\n          <td>{{ utCorr?.ragioneSociale }}</td>\r\n        </tr>\r\n        <tr>\r\n          <td>Address:</td>\r\n          <td>{{ utCorr?.indirizzo }}</td>\r\n        </tr>\r\n        <tr>\r\n          <td>Place:</td>\r\n          <td>{{ utCorr?.localita }}</td>\r\n        </tr>\r\n        <tr>\r\n          <td>Province:</td>\r\n          <td>{{ utCorr?.provincia }}</td>\r\n        </tr>\r\n        <tr>\r\n          <td>Nation:</td>\r\n          <td>{{ utCorr?.nazione }}</td>\r\n        </tr>\r\n        <tr>\r\n          <td>Phone:</td>\r\n          <td>{{ utCorr?.telefono }}</td>\r\n        </tr>\r\n        <tr>\r\n          <td>Fax:</td>\r\n          <td>{{ utCorr?.fax }}</td>\r\n        </tr>\r\n        <tr>\r\n          <td>Partita Iva :</td>\r\n          <td>{{ utCorr?.partitaIva }}</td>\r\n        </tr>\r\n        <tr>\r\n          <td>Referent:</td>\r\n          <td>{{ utCorr?.referente }}</td>\r\n        </tr>\r\n        <tr>\r\n        </tr>  \r\n      </tbody>\r\n    </table>\r\n  </div>\r\n\r\n  <div class=\"row justify-content-center\">\r\n      <table class=\"table table-light mt-3\" style=\"width: 50%; \">\r\n        <thead class=\"bg-info\">\r\n        <th scope=\"col\">description course</th>\r\n        <th scope=\"col\">tipology</th>\r\n        <th scope=\"col\">type</th>\r\n      </thead>\r\n        <tr *ngFor=\"let valore of listaAllCourses\">\r\n          <!--  <td>{{ valore.docenteId }}</td> -->\r\n          <td>{{ valore?.descrizione }}</td>\r\n          <td>{{ valore?.tipologia?.descrizione }}</td>\r\n          <td>{{ valore?.tipo?.descrizione }}</td>\r\n        </tr>\r\n      </table>\r\n    </div>\r\n\r\n</div>\r\n \r\n</p-sidebar>\r\n\r\n<div class=\"container-mt-3\">\r\n  <p-messages [value]=\"msgs\" [style]=\"{ width: '100%' }\"></p-messages>\r\n</div>\r\n\r\n<!-- <p-sidebar\r\n  [(visible)]=\"visibleSidebartop\"\r\n  position=\"top\"\r\n  [baseZIndex]=\"100000\"\r\n  [style]=\"{ height: '100%' }\"\r\n>\r\n  <div>\r\n    <form [formGroup]=\"formGroup2\">\r\n      teachers id:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"docenteId\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      title:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"titolo\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      business name:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"ragioneSociale\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      address:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"indirizzo\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      resort:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"localita\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      province:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"provincia\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      nation:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"nazione\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      phone number:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"telefono\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      fax:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"fax\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      partita iva:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"partitaIva\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      referent:\r\n      <input\r\n        type=\"text\"\r\n        formControlName=\"referente\"\r\n        class=\"mt-2 mr-1\"\r\n        required\r\n      /><br />\r\n      <button (click)=\"filtraA()\" class=\"btn btn-light\">filter</button>\r\n    </form>\r\n  </div>\r\n</p-sidebar> -->\r\n\r\n<p-sidebar [(visible)]=\"visibleSidebartop\" position=\"top\" [baseZIndex]=\"100000\" [fullScreen]=\"true\"\r\n   [style]=\"{'overflow-y': 'scroll'}\">\r\n   <div class=\"row justify-content-center\">\r\n     <h2>Advanced Filter</h2>\r\n   </div>\r\n\r\n   <div class=\"advanced-form\">\r\n     <form [formGroup]=\"formGroup2\">\r\n       <div class=\"form-group\">\r\n         <input type=\"text\" class=\"form-control\" formControlName=\"docenteId\" required placeholder=\"docenteId\" />\r\n       </div>\r\n       <div class=\"form-group\">\r\n         <input type=\"text\" class=\"form-control\" formControlName=\"titolo\" required placeholder=\"titolo\" />\r\n       </div>\r\n       <div class=\"form-group\">\r\n         <input type=\"text\" class=\"form-control\" formControlName=\"ragioneSociale\"  required placeholder=\"ragioneSociale\" />\r\n       </div>\r\n       <div class=\"form-group\">\r\n         <input type=\"text\" class=\"form-control\" formControlName=\"indirizzo\" required placeholder=\"indirizzo\" />\r\n       </div>\r\n       <div class=\"form-group\">\r\n         <input type=\"text\" class=\"form-control\" formControlName=\"localita\" required placeholder=\"localita\" />\r\n       </div>\r\n       <div class=\"form-group\">\r\n         <input type=\"text\" class=\"form-control\" formControlName=\"provincia\" required placeholder=\"provincia\" />\r\n       </div>\r\n       <div class=\"form-group\">\r\n         <input type=\"text\" class=\"form-control\" formControlName=\"nazione\" required placeholder=\"nazione\" />\r\n       </div>\r\n\r\n       <div class=\"form-group\">\r\n         <input type=\"text\" class=\"form-control\" formControlName=\"telefono\" required placeholder=\"telefono\" />\r\n       </div>\r\n       <div class=\"form-group\">\r\n         <input type=\"text\" class=\"form-control\" formControlName=\"fax\" required placeholder=\"fax\" />\r\n       </div>\r\n       <div class=\"form-group\">\r\n         <input type=\"text\" class=\"form-control\" formControlName=\"partitaIva\" required\r\n           placeholder=\"partitaIva\" />\r\n       </div>\r\n       <div class=\"form-group\">\r\n         <input type=\"text\" class=\"form-control\" formControlName=\"referente\" required placeholder=\"referente\" />\r\n       </div>\r\n       <div class=\"form-group\">\r\n         <button (click)=\"filtraA()\" class=\"btn btn-info btn-lg btn-block\">filter</button>\r\n       </div>\r\n     </form>\r\n   </div>\r\n </p-sidebar>\r\n\r\n");

/***/ }),

/***/ "./src/app/views/teachers/teachers-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/views/teachers/teachers-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: TeachersRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeachersRoutingModule", function() { return TeachersRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _teachers_teachers_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./teachers/teachers.component */ "./src/app/views/teachers/teachers/teachers.component.ts");




var routes = [{
        path: '',
        component: _teachers_teachers_component__WEBPACK_IMPORTED_MODULE_3__["TeachersComponent"],
        data: {
            title: 'teachers'
        }
    },];
var TeachersRoutingModule = /** @class */ (function () {
    function TeachersRoutingModule() {
    }
    TeachersRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], TeachersRoutingModule);
    return TeachersRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/teachers/teachers.module.ts":
/*!***************************************************!*\
  !*** ./src/app/views/teachers/teachers.module.ts ***!
  \***************************************************/
/*! exports provided: TeachersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeachersModule", function() { return TeachersModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _teachers_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./teachers-routing.module */ "./src/app/views/teachers/teachers-routing.module.ts");
/* harmony import */ var _teachers_teachers_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./teachers/teachers.component */ "./src/app/views/teachers/teachers/teachers.component.ts");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/table */ "./node_modules/primeng/fesm5/primeng-table.js");
/* harmony import */ var primeng_sidebar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/sidebar */ "./node_modules/primeng/fesm5/primeng-sidebar.js");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/fesm5/primeng-dropdown.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/confirmdialog */ "./node_modules/primeng/fesm5/primeng-confirmdialog.js");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/button */ "./node_modules/primeng/fesm5/primeng-button.js");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/messages */ "./node_modules/primeng/fesm5/primeng-messages.js");














var TeachersModule = /** @class */ (function () {
    function TeachersModule() {
    }
    TeachersModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_teachers_teachers_component__WEBPACK_IMPORTED_MODULE_4__["TeachersComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _teachers_routing_module__WEBPACK_IMPORTED_MODULE_3__["TeachersRoutingModule"],
                primeng_table__WEBPACK_IMPORTED_MODULE_5__["TableModule"],
                primeng_dropdown__WEBPACK_IMPORTED_MODULE_7__["DropdownModule"],
                primeng_sidebar__WEBPACK_IMPORTED_MODULE_6__["SidebarModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_10__["HttpModule"],
                primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_11__["ConfirmDialogModule"],
                primeng_button__WEBPACK_IMPORTED_MODULE_12__["ButtonModule"],
                primeng_messages__WEBPACK_IMPORTED_MODULE_13__["MessagesModule"],
            ],
        })
    ], TeachersModule);
    return TeachersModule;
}());



/***/ }),

/***/ "./src/app/views/teachers/teachers/teachers.component.css":
/*!****************************************************************!*\
  !*** ./src/app/views/teachers/teachers/teachers.component.css ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#sidebar{\r\n font-weight: 300;\r\n white-space: pre;\r\n}\r\n.form-control{\r\n    height: 40px;\r\n    box-shadow: none;\r\n    color: #969fa4;\r\n}\r\n.form-control:focus{\r\n    border-color: #5cb85c;\r\n}\r\n.form-control, .btn{        \r\n    border-radius: 3px;\r\n}\r\n.advanced-form{\r\n    width: 50%;\r\n    margin: 0 auto;\r\n    padding: 30px 0;\r\n}\r\n.advanced-form h2{\r\n    color: #636363;\r\n    margin: 0 0 15px;\r\n    position: relative;\r\n    text-align: center;\r\n}\r\n.advanced-form h2:before, .advanced-form h2:after{\r\n    content: \"\";\r\n    height: 2px;\r\n    width: 25%;\r\n    background: #d4d4d4;\r\n    position: absolute;\r\n    top: 50%;\r\n    z-index: 2;\r\n}\r\n.advanced-form h2:before{\r\n    left: 0;\r\n}\r\n.advanced-form h2:after{\r\n    right: 0;\r\n}\r\n.advanced-form .hint-text{\r\n    color: #999;\r\n    margin-bottom: 30px;\r\n    text-align: center;\r\n}\r\n.advanced-form form{\r\n    color: #999;\r\n    border-radius: 3px;\r\n    margin-bottom: 15px;\r\n    background: #f2f3f7;\r\n    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);\r\n    padding: 30px;\r\n}\r\n.advanced-form .form-group{\r\n    margin-bottom: 20px;\r\n}\r\n.advanced-form input[type=\"checkbox\"]{\r\n    margin-top: 3px;\r\n}\r\n.advanced-form .btn{        \r\n    font-size: 16px;\r\n    font-weight: bold;\t\t\r\n    min-width: 140px;\r\n    outline: none !important;\r\n}\r\n.advanced-form .row div:first-child{\r\n    padding-right: 10px;\r\n}\r\n.advanced-form .row div:last-child{\r\n    padding-left: 10px;\r\n}\r\n.advanced-form a{\r\n    color: #fff;\r\n    text-decoration: underline;\r\n}\r\n.advanced-form a:hover{\r\n    text-decoration: none;\r\n}\r\n.advanced-form form a{\r\n    color: #5cb85c;\r\n    text-decoration: none;\r\n}\r\n.advanced-form form a:hover{\r\n    text-decoration: underline;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvdGVhY2hlcnMvdGVhY2hlcnMvdGVhY2hlcnMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtDQUNDLGdCQUFnQjtDQUNoQixnQkFBZ0I7QUFDakI7QUFDQTtJQUNJLFlBQVk7SUFDWixnQkFBZ0I7SUFDaEIsY0FBYztBQUNsQjtBQUNBO0lBQ0kscUJBQXFCO0FBQ3pCO0FBQ0E7SUFDSSxrQkFBa0I7QUFDdEI7QUFDQTtJQUNJLFVBQVU7SUFDVixjQUFjO0lBQ2QsZUFBZTtBQUNuQjtBQUNBO0lBQ0ksY0FBYztJQUNkLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsV0FBVztJQUNYLFVBQVU7SUFDVixtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixVQUFVO0FBQ2Q7QUFDQTtJQUNJLE9BQU87QUFDWDtBQUNBO0lBQ0ksUUFBUTtBQUNaO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsbUJBQW1CO0lBQ25CLGtCQUFrQjtBQUN0QjtBQUNBO0lBQ0ksV0FBVztJQUNYLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLDBDQUEwQztJQUMxQyxhQUFhO0FBQ2pCO0FBQ0E7SUFDSSxtQkFBbUI7QUFDdkI7QUFDQTtJQUNJLGVBQWU7QUFDbkI7QUFDQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsZ0JBQWdCO0lBQ2hCLHdCQUF3QjtBQUM1QjtBQUNBO0lBQ0ksbUJBQW1CO0FBQ3ZCO0FBQ0E7SUFDSSxrQkFBa0I7QUFDdEI7QUFDQTtJQUNJLFdBQVc7SUFDWCwwQkFBMEI7QUFDOUI7QUFDQTtJQUNJLHFCQUFxQjtBQUN6QjtBQUNBO0lBQ0ksY0FBYztJQUNkLHFCQUFxQjtBQUN6QjtBQUNBO0lBQ0ksMEJBQTBCO0FBQzlCIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvdGVhY2hlcnMvdGVhY2hlcnMvdGVhY2hlcnMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiNzaWRlYmFye1xyXG4gZm9udC13ZWlnaHQ6IDMwMDtcclxuIHdoaXRlLXNwYWNlOiBwcmU7XHJcbn1cclxuLmZvcm0tY29udHJvbHtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICBjb2xvcjogIzk2OWZhNDtcclxufVxyXG4uZm9ybS1jb250cm9sOmZvY3Vze1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjNWNiODVjO1xyXG59XHJcbi5mb3JtLWNvbnRyb2wsIC5idG57ICAgICAgICBcclxuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcclxufVxyXG4uYWR2YW5jZWQtZm9ybXtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIHBhZGRpbmc6IDMwcHggMDtcclxufVxyXG4uYWR2YW5jZWQtZm9ybSBoMntcclxuICAgIGNvbG9yOiAjNjM2MzYzO1xyXG4gICAgbWFyZ2luOiAwIDAgMTVweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uYWR2YW5jZWQtZm9ybSBoMjpiZWZvcmUsIC5hZHZhbmNlZC1mb3JtIGgyOmFmdGVye1xyXG4gICAgY29udGVudDogXCJcIjtcclxuICAgIGhlaWdodDogMnB4O1xyXG4gICAgd2lkdGg6IDI1JTtcclxuICAgIGJhY2tncm91bmQ6ICNkNGQ0ZDQ7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDUwJTtcclxuICAgIHotaW5kZXg6IDI7XHJcbn1cdFxyXG4uYWR2YW5jZWQtZm9ybSBoMjpiZWZvcmV7XHJcbiAgICBsZWZ0OiAwO1xyXG59XHJcbi5hZHZhbmNlZC1mb3JtIGgyOmFmdGVye1xyXG4gICAgcmlnaHQ6IDA7XHJcbn1cclxuLmFkdmFuY2VkLWZvcm0gLmhpbnQtdGV4dHtcclxuICAgIGNvbG9yOiAjOTk5O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uYWR2YW5jZWQtZm9ybSBmb3Jte1xyXG4gICAgY29sb3I6ICM5OTk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgYmFja2dyb3VuZDogI2YyZjNmNztcclxuICAgIGJveC1zaGFkb3c6IDBweCAycHggMnB4IHJnYmEoMCwgMCwgMCwgMC4zKTtcclxuICAgIHBhZGRpbmc6IDMwcHg7XHJcbn1cclxuLmFkdmFuY2VkLWZvcm0gLmZvcm0tZ3JvdXB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcbi5hZHZhbmNlZC1mb3JtIGlucHV0W3R5cGU9XCJjaGVja2JveFwiXXtcclxuICAgIG1hcmdpbi10b3A6IDNweDtcclxufVxyXG4uYWR2YW5jZWQtZm9ybSAuYnRueyAgICAgICAgXHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcdFx0XHJcbiAgICBtaW4td2lkdGg6IDE0MHB4O1xyXG4gICAgb3V0bGluZTogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcbi5hZHZhbmNlZC1mb3JtIC5yb3cgZGl2OmZpcnN0LWNoaWxke1xyXG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcclxufVxyXG4uYWR2YW5jZWQtZm9ybSAucm93IGRpdjpsYXN0LWNoaWxke1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG59ICAgIFx0XHJcbi5hZHZhbmNlZC1mb3JtIGF7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG59XHJcbi5hZHZhbmNlZC1mb3JtIGE6aG92ZXJ7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbn1cclxuLmFkdmFuY2VkLWZvcm0gZm9ybSBhe1xyXG4gICAgY29sb3I6ICM1Y2I4NWM7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbn1cdFxyXG4uYWR2YW5jZWQtZm9ybSBmb3JtIGE6aG92ZXJ7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/views/teachers/teachers/teachers.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/views/teachers/teachers/teachers.component.ts ***!
  \***************************************************************/
/*! exports provided: TeachersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeachersComponent", function() { return TeachersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_services_teachers_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../core/services/teachers.service */ "./src/app/core/services/teachers.service.ts");
/* harmony import */ var _core_services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../core/services/api.service */ "./src/app/core/services/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var paix__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! paix */ "./node_modules/paix/build/index.js");
/* harmony import */ var paix__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(paix__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/fesm5/primeng-api.js");
/* harmony import */ var _core_services_course_users_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../core/services/course_users.service */ "./src/app/core/services/course_users.service.ts");









var TeachersComponent = /** @class */ (function () {
    function TeachersComponent(api, teachersService, router, confirmationService, fb, courseUserServices) {
        this.api = api;
        this.teachersService = teachersService;
        this.router = router;
        this.confirmationService = confirmationService;
        this.fb = fb;
        this.courseUserServices = courseUserServices;
        this.tipo = [];
        this.pagina = 1;
        this.dati = 4;
        this.nPagine = 0;
        this.visibleSidebartop = false;
        this.filt = [];
        this.msgs = [];
        this.lista = [];
        this.lista2 = [];
        this.listaAllCourses = [];
        this.tipo = [
            { name: "id", code: "docenteId" },
            { name: "title", code: "titolo" },
            { name: "business name", code: "ragioneSociale" },
            { name: "address", code: "indirizzo" },
            { name: "resort", code: "localita" },
            { name: "province", code: "provincia" },
            { name: "nation", code: "nazione" },
            { name: "phone number", code: "telefono" },
            { name: "fax", code: "fax" },
            { name: "partita iva", code: "partitaIva" },
            { name: "referent", code: "referente" },
        ];
    }
    TeachersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.formGroup2 = this.fb.group({
            docenteId: [""],
            titolo: [""],
            ragioneSociale: [""],
            indirizzo: [""],
            localita: [""],
            provincia: [""],
            nazione: [""],
            telefono: [""],
            fax: [""],
            partitaIva: [""],
            referente: [""],
        });
        this.formGroup = this.fb.group({
            soggFiltrato: [""],
        });
        this.teachersService.getAll().subscribe(function (resp) {
            _this.lista2 = resp.response;
        });
        this.loadAll();
    };
    TeachersComponent.prototype.confirm1 = function (val) {
        var _this = this;
        this.confirmationService.confirm({
            message: "Sei sicuro di voler eliminare il docente?",
            header: "Conferma",
            icon: "pi pi-exclamation-triangle",
            key: "conferma",
            accept: function () {
                _this.msgs = [
                    {
                        severity: "success",
                        summary: "Confermato",
                        detail: "Eliminazione Completata",
                    },
                ];
                _this.delete(val);
                _this.confirmationService.close();
            },
            reject: function () {
                _this.msgs = [
                    {
                        severity: "info",
                        summary: "Annullato",
                        detail: "Operazione Annullata",
                    },
                ];
                _this.confirmationService.close();
            },
        });
    };
    TeachersComponent.prototype.loadAll = function () {
        var _this = this;
        this.teachersService
            .getAllPage(this.pagina, this.dati)
            .subscribe(function (resp) {
            _this.lista = resp.response;
        });
    };
    TeachersComponent.prototype.show = function (valore) {
        this.visibleSidebar5 = true;
        this.utCorr = valore;
        this.showCorses(valore);
    };
    TeachersComponent.prototype.new = function () {
        this.router.navigate(["/new"]);
    };
    TeachersComponent.prototype.delete = function (val) {
        var _this = this;
        this.teachersService.deleteById(val).subscribe(function () {
            if (_this.filtroSelezionato == null) {
                _this.loadAll();
            }
            else {
                _this.filtra();
            }
        });
    };
    TeachersComponent.prototype.edit = function (id) {
        this.router.navigate(["/edit/" + id]);
    };
    TeachersComponent.prototype.filtra = function () {
        var _this = this;
        var res = { nome: this.formGroup.value.soggFiltrato };
        var replacement = { nome: this.filtroSelezionato.code };
        var modificato = Object(paix__WEBPACK_IMPORTED_MODULE_6__["paix"])(res, replacement);
        this.teachersService
            .getById(modificato, this.pagina, this.dati)
            .subscribe(function (resp) {
            _this.lista = resp.response;
            _this.lista2 = resp.response;
        });
    };
    TeachersComponent.prototype.avanti = function () {
        this.nPagine = this.lista2.length / this.dati;
        if (this.pagina < this.nPagine)
            this.pagina++;
        if (this.filtroSelezionato == null) {
            this.loadAll();
        }
        else {
            this.filtra();
        }
    };
    TeachersComponent.prototype.dietro = function () {
        if (this.pagina - 1 > 0) {
            this.pagina--;
            if (this.filtroSelezionato == null) {
                this.loadAll();
            }
            else {
                this.filtra();
            }
        }
    };
    TeachersComponent.prototype.removeFilter = function () {
        this.loadAll();
        this.filtroSelezionato = null;
        this.formGroup = this.fb.group({
            soggFiltrato: [""],
        });
        this.formGroup2 = this.fb.group({
            docenteId: [""],
            titolo: [""],
            ragioneSociale: [""],
            indirizzo: [""],
            localita: [""],
            provincia: [""],
            nazione: [""],
            telefono: [""],
            fax: [""],
            partitaIva: [""],
            referente: [""],
        });
    };
    TeachersComponent.prototype.filtraAvanzato = function () {
        this.visibleSidebartop = true;
    };
    TeachersComponent.prototype.filtraA = function () {
        var _this = this;
        var primo = [];
        var secondo = [];
        for (var field in this.formGroup2.controls) {
            if (this.formGroup2.controls[field].value != "") {
                primo.push(field);
                secondo.push(this.formGroup2.controls[field].value);
            }
        }
        var finale = Object(paix__WEBPACK_IMPORTED_MODULE_6__["paix"])(secondo, primo);
        var newFinale = Object.assign({}, finale);
        this.teachersService
            .getById(newFinale, this.pagina, this.dati)
            .subscribe(function (resp) {
            _this.lista = resp.response;
            _this.lista2 = resp.response;
        });
        this.visibleSidebartop = false;
    };
    TeachersComponent.prototype.showCorses = function (valore) {
        var _this = this;
        this.listaAllCourses = [];
        this.courseUserServices.getAllCourseTeachers().subscribe(function (element) {
            for (var i = 0; i < element.response.length; i++)
                if (element.response[i].docente == valore.docenteId)
                    _this.listaAllCourses.push(element.response[i].corso);
        });
    };
    TeachersComponent.ctorParameters = function () { return [
        { type: _core_services_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"] },
        { type: _core_services_teachers_service__WEBPACK_IMPORTED_MODULE_2__["TeachersService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: primeng_api__WEBPACK_IMPORTED_MODULE_7__["ConfirmationService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] },
        { type: _core_services_course_users_service__WEBPACK_IMPORTED_MODULE_8__["CourseUsersService"] }
    ]; };
    TeachersComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-teachers",
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./teachers.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/teachers/teachers/teachers.component.html")).default,
            providers: [primeng_api__WEBPACK_IMPORTED_MODULE_7__["ConfirmationService"]],
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./teachers.component.css */ "./src/app/views/teachers/teachers/teachers.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_core_services_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"],
            _core_services_teachers_service__WEBPACK_IMPORTED_MODULE_2__["TeachersService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            primeng_api__WEBPACK_IMPORTED_MODULE_7__["ConfirmationService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            _core_services_course_users_service__WEBPACK_IMPORTED_MODULE_8__["CourseUsersService"]])
    ], TeachersComponent);
    return TeachersComponent;
}());



/***/ })

}]);
//# sourceMappingURL=views-teachers-teachers-module.js.map