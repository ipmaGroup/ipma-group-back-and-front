(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-new-course-new-course-users-new-course-users-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/new-course/new-course-users/new-course-users/new-course-users.component.html":
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/new-course/new-course-users/new-course-users/new-course-users.component.html ***!
  \******************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"row\" style=\"display:flex; margin-left: 18%; width: 130%;\">\r\n    <div class=\"col-sm-6\">\r\n        <div class=\"card\" style=\"width: 100%;\">\r\n            <form [formGroup]=\"formGroupDocenti\" class=\"text-center border border-light p-5\" action=\"#!\">\r\n\r\n                <p class=\"h2 mb-4\">\r\n                    <font size=\"6\">Aggiungi Personale</font>\r\n                </p>\r\n\r\n                <p class=\"h2 mb-4\" style=\"text-align: left;\">\r\n                    <font size=\"5\">Aggiunta Docente</font>\r\n                </p>\r\n\r\n                <div class=\"form-group ui-g-12 ui-md-6\">\r\n                    <label for=\"nome\" style=\"display: block; text-align: center;\">\r\n                        <font size=\"3\">{{scritta}}</font>\r\n                    </label>\r\n                    <p-inputSwitch [(ngModel)]=\"internoControl\" formControlName=\"interno\" required\r\n                        (onChange)=\"getDocenti()\">\r\n                    </p-inputSwitch>\r\n                </div>\r\n\r\n                <div class=\"form-group ui-g-12 ui-md-6\">\r\n                    <label for=\"nome\">\r\n                        <font size=\"3\">Docente</font>\r\n                    </label><br>\r\n                    <p-dropdown *ngIf=(listaDipendenti) [options]=\"descrizioneDipendente\" optionLabel=\"label\"\r\n                        formControlName=\"docente\"></p-dropdown>\r\n                    <p-dropdown *ngIf=(listaDocenti) [options]=\"descrizioneDocente\" optionLabel=\"label\"\r\n                        formControlName=\"docente\"></p-dropdown>\r\n                </div>\r\n            </form>\r\n\r\n\r\n            <form [formGroup]=\"formGroupDipendenti\" class=\"border border-light p-5\" action=\"#!\">\r\n                <p class=\"h2 mb-4\" style=\"text-align: left;\">\r\n                    <font size=\"5\">Aggiunta Alunni</font>\r\n                </p>\r\n\r\n                <div class=\"col-6 ui-g-12 ui-md-6\">\r\n                    <div class=\"form-group\">\r\n                        <p-listbox *ngIf=(listaDipendenti1) [options]=\"descrizioneDipendente1\" [multiple]=\"true\"\r\n                            [checkbox]=\"true\" [filter]=\"true\" optionLabel=\"label\" [style]=\"{width: '90%'}\"\r\n                            [listStyle]=\"{ 'max-height': '210px' }\" formControlName=\"dipendente\">\r\n                            <p-header style=\"text-align: center;\">\r\n                                Alunni\r\n                            </p-header>\r\n                        </p-listbox>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group ui-g-12 ui-md-6\">\r\n                    <label for=\"nome\">\r\n                        <font size=\"3\">Durata</font>\r\n                    </label><br>\r\n                    <p-spinner [(ngModel)]=\"durataN\" size=\"30\" formControlName=\"durata\"></p-spinner>\r\n                </div>\r\n            </form>\r\n            <div><br>\r\n                <button class=\"btn btn-info btn-block\" (click)=\"conferma()\" type=\"button\" style=\"width: 25%; margin-left: 37.5%;\">\r\n                    <i class=\"fa fa-check mr-2\"\r\n                    aria-hidden=\"true\"></i>Conferma\r\n                </button><br></div>\r\n        </div>\r\n    </div>\r\n</div>");

/***/ }),

/***/ "./src/app/core/services/course_users.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/core/services/course_users.service.ts ***!
  \*******************************************************/
/*! exports provided: CourseUsersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CourseUsersService", function() { return CourseUsersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api.service */ "./src/app/core/services/api.service.ts");
/* harmony import */ var _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/enviroment */ "./src/app/shared/enviroment.ts");




var CourseUsersService = /** @class */ (function () {
    function CourseUsersService(api) {
        this.api = api;
        this.teachersPath = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.courseTeachersController;
        this.usersPath = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.courseUsersController;
        this.teachersPathFilter = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.courseTeachersControllerFilter;
        this.usersPathFilter = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.courseUsersControllerFilter;
        this.listSoggetti = [];
    }
    CourseUsersService.prototype.getAllCourseTeachers = function () {
        return this.api.get(this.teachersPath);
    };
    CourseUsersService.prototype.getAllCourseUsers = function () {
        return this.api.get(this.usersPath);
    };
    CourseUsersService.prototype.getAllPageTeachers = function (primo, secondo) {
        return this.api.getByPage(this.teachersPath, primo, secondo);
    };
    CourseUsersService.prototype.getAllPageUsers = function (primo, secondo) {
        return this.api.getByPage(this.usersPath, primo, secondo);
    };
    CourseUsersService.prototype.getByIdTeachers = function (ut, inizio, fine) {
        return this.api.getById(this.teachersPathFilter + inizio + "..." + fine, ut);
    };
    CourseUsersService.prototype.getByIdUsers = function (ut, inizio, fine) {
        return this.api.getById(this.usersPathFilter + inizio + "..." + fine, ut);
    };
    CourseUsersService.prototype.addTeachers = function (item) {
        return this.api.post(this.teachersPath, item);
    };
    CourseUsersService.prototype.addUsers = function (item) {
        return this.api.post(this.usersPath, item);
    };
    CourseUsersService.prototype.deleteByIdTeachers = function (body) {
        return this.api.delete(this.teachersPath, body);
    };
    CourseUsersService.prototype.deleteByIdUsers = function (body) {
        return this.api.delete(this.usersPath, body);
    };
    CourseUsersService.prototype.updateTeachers = function (soggetto) {
        return this.api.patch(this.teachersPath, soggetto);
    };
    CourseUsersService.prototype.updateUsers = function (soggetto) {
        return this.api.patch(this.usersPath, soggetto);
    };
    CourseUsersService.ctorParameters = function () { return [
        { type: _api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"] }
    ]; };
    CourseUsersService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root",
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]])
    ], CourseUsersService);
    return CourseUsersService;
}());



/***/ }),

/***/ "./src/app/core/services/teachers.service.ts":
/*!***************************************************!*\
  !*** ./src/app/core/services/teachers.service.ts ***!
  \***************************************************/
/*! exports provided: TeachersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeachersService", function() { return TeachersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api.service */ "./src/app/core/services/api.service.ts");
/* harmony import */ var _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/enviroment */ "./src/app/shared/enviroment.ts");




var TeachersService = /** @class */ (function () {
    function TeachersService(api) {
        this.api = api;
        this.path = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.teachersController;
        this.pathFilter = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.teachersControllerFilter;
        this.listSoggetti = [];
    }
    TeachersService.prototype.getAll = function () {
        return this.api.get(this.path);
    };
    TeachersService.prototype.getAllPage = function (primo, secondo) {
        return this.api.getByPage(this.path, primo, secondo);
    };
    TeachersService.prototype.getById = function (ut, inizio, fine) {
        return this.api.getById(this.pathFilter + inizio + "..." + fine, ut);
    };
    TeachersService.prototype.add = function (item) {
        return this.api.post(this.path, item);
    };
    TeachersService.prototype.deleteById = function (body) {
        return this.api.delete(this.path, body);
    };
    TeachersService.prototype.update = function (soggetto) {
        return this.api.patch(this.path, soggetto);
    };
    TeachersService.ctorParameters = function () { return [
        { type: _api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"] }
    ]; };
    TeachersService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root",
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]])
    ], TeachersService);
    return TeachersService;
}());



/***/ }),

/***/ "./src/app/views/new-course/new-course-users/new-course-users-routing.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/views/new-course/new-course-users/new-course-users-routing.module.ts ***!
  \**************************************************************************************/
/*! exports provided: NewCourseUsersRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCourseUsersRoutingModule", function() { return NewCourseUsersRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _new_course_users_new_course_users_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./new-course-users/new-course-users.component */ "./src/app/views/new-course/new-course-users/new-course-users/new-course-users.component.ts");




var routes = [{
        path: '',
        component: _new_course_users_new_course_users_component__WEBPACK_IMPORTED_MODULE_3__["NewCourseUsersComponent"],
        data: {
            title: 'new-course-users'
        }
    },
];
var NewCourseUsersRoutingModule = /** @class */ (function () {
    function NewCourseUsersRoutingModule() {
    }
    NewCourseUsersRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], NewCourseUsersRoutingModule);
    return NewCourseUsersRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/new-course/new-course-users/new-course-users.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/views/new-course/new-course-users/new-course-users.module.ts ***!
  \******************************************************************************/
/*! exports provided: NewCourseUsersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCourseUsersModule", function() { return NewCourseUsersModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _new_course_users_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./new-course-users-routing.module */ "./src/app/views/new-course/new-course-users/new-course-users-routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/fesm5/primeng-dropdown.js");
/* harmony import */ var primeng_calendar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/calendar */ "./node_modules/primeng/fesm5/primeng-calendar.js");
/* harmony import */ var primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/inputtextarea */ "./node_modules/primeng/fesm5/primeng-inputtextarea.js");
/* harmony import */ var primeng_tabview__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/tabview */ "./node_modules/primeng/fesm5/primeng-tabview.js");
/* harmony import */ var primeng_inputswitch__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/inputswitch */ "./node_modules/primeng/fesm5/primeng-inputswitch.js");
/* harmony import */ var primeng_listbox__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/listbox */ "./node_modules/primeng/fesm5/primeng-listbox.js");
/* harmony import */ var _new_course_users_new_course_users_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./new-course-users/new-course-users.component */ "./src/app/views/new-course/new-course-users/new-course-users/new-course-users.component.ts");
/* harmony import */ var primeng_spinner__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/spinner */ "./node_modules/primeng/fesm5/primeng-spinner.js");













var NewCourseUsersModule = /** @class */ (function () {
    function NewCourseUsersModule() {
    }
    NewCourseUsersModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_new_course_users_new_course_users_component__WEBPACK_IMPORTED_MODULE_11__["NewCourseUsersComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _new_course_users_routing_module__WEBPACK_IMPORTED_MODULE_3__["NewCourseUsersRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                primeng_dropdown__WEBPACK_IMPORTED_MODULE_5__["DropdownModule"],
                primeng_calendar__WEBPACK_IMPORTED_MODULE_6__["CalendarModule"],
                primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_7__["InputTextareaModule"],
                primeng_tabview__WEBPACK_IMPORTED_MODULE_8__["TabViewModule"],
                primeng_inputswitch__WEBPACK_IMPORTED_MODULE_9__["InputSwitchModule"],
                primeng_listbox__WEBPACK_IMPORTED_MODULE_10__["ListboxModule"],
                primeng_spinner__WEBPACK_IMPORTED_MODULE_12__["SpinnerModule"]
            ]
        })
    ], NewCourseUsersModule);
    return NewCourseUsersModule;
}());



/***/ }),

/***/ "./src/app/views/new-course/new-course-users/new-course-users/new-course-users.component.css":
/*!***************************************************************************************************!*\
  !*** ./src/app/views/new-course/new-course-users/new-course-users/new-course-users.component.css ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL25ldy1jb3Vyc2UvbmV3LWNvdXJzZS11c2Vycy9uZXctY291cnNlLXVzZXJzL25ldy1jb3Vyc2UtdXNlcnMuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/views/new-course/new-course-users/new-course-users/new-course-users.component.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/views/new-course/new-course-users/new-course-users/new-course-users.component.ts ***!
  \**************************************************************************************************/
/*! exports provided: NewCourseUsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCourseUsersComponent", function() { return NewCourseUsersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _core_services_course_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../core/services/course.service */ "./src/app/core/services/course.service.ts");
/* harmony import */ var _core_services_employees_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/services/employees.services */ "./src/app/core/services/employees.services.ts");
/* harmony import */ var _core_services_teachers_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../core/services/teachers.service */ "./src/app/core/services/teachers.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_services_course_users_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../core/services/course_users.service */ "./src/app/core/services/course_users.service.ts");








var NewCourseUsersComponent = /** @class */ (function () {
    function NewCourseUsersComponent(fb, courseUsersService, courseService, employeeService, teacherService, router, route) {
        this.fb = fb;
        this.courseUsersService = courseUsersService;
        this.courseService = courseService;
        this.employeeService = employeeService;
        this.teacherService = teacherService;
        this.router = router;
        this.route = route;
        this.scritta = "Esterno";
        this.i = 0;
    }
    NewCourseUsersComponent.prototype.ngOnInit = function () {
        this.getDipendenti();
        this.getDocenti();
        this.getCorso();
        this.formGroupDocenti = this.fb.group({
            corso: [''],
            docente: [''],
            interno: [''],
        });
        this.formGroupDipendenti = this.fb.group({
            corso: [''],
            dipendente: [''],
            durata: [''],
        });
    };
    NewCourseUsersComponent.prototype.conferma = function () {
        var _this = this;
        this.docentiAgg = this.formGroupDocenti.value;
        console.log(this.docentiAgg);
        if (this.docentiAgg) {
            this.docentiAgg.corso = this.ultimoCorso;
            if (this.internoControl) {
                this.internoInt = 1;
                this.docentiAgg.docente = this.docentiAgg.docente.value.element.dipendenteId;
            }
            else {
                this.internoInt = 0;
                this.docentiAgg.docente = this.docentiAgg.docente.value.element.docenteId;
            }
            this.docentiAgg.interno = this.internoInt;
            var idOBJ = Object.assign({}, this.docentiAgg);
            this.courseUsersService.addTeachers(this.docentiAgg).subscribe(function () {
            });
        }
        this.dipendentiAgg = this.formGroupDipendenti.value;
        if (this.dipendentiAgg) {
            this.dipendentiAgg.dipendente.forEach(function (element) {
                _this.dipendentiAgg.corso = _this.ultimoCorso;
                _this.dipendentiAgg.dipendente = element.value.element;
                _this.dipendentiAgg.durata = _this.durataN;
                _this.courseUsersService.addUsers(_this.dipendentiAgg).subscribe(function () {
                    _this.verifica = true;
                    _this.router.navigate(['/corsi']);
                });
            });
        }
    };
    NewCourseUsersComponent.prototype.getDocenti = function () {
        var _this = this;
        if (this.internoControl) {
            this.scritta = "Interno";
            this.internoInt = 1;
            this.listaDocenti = null;
            this.descrizioneDocente = null;
            this.descrizioneDipendente = [{ label: "Seleziona Docente", value: null }];
            this.employeeService.getAll().subscribe(function (resp) {
                _this.listaDipendenti = resp.response;
                if (_this.listaDipendenti) {
                    _this.listaDipendenti.forEach(function (element) {
                        _this.descrizioneDipendente.push({
                            label: element.cognome + " " + element.nome,
                            value: { element: element },
                        });
                    });
                }
            });
        }
        else {
            this.scritta = "Esterno";
            this.internoInt = 0;
            this.listaDipendenti = null;
            this.descrizioneDipendente = null;
            this.descrizioneDocente = [{ label: "Seleziona Docente", value: null }];
            this.teacherService.getAll().subscribe(function (resp) {
                _this.listaDocenti = resp.response;
                if (_this.listaDocenti) {
                    _this.listaDocenti.forEach(function (element) {
                        _this.descrizioneDocente.push({
                            label: element.titolo,
                            value: { element: element },
                        });
                    });
                }
            });
        }
    };
    NewCourseUsersComponent.prototype.getDipendenti = function () {
        var _this = this;
        this.descrizioneDipendente1 = [{ label: "Seleziona Alunni", value: null }];
        this.employeeService.getAll().subscribe(function (resp) {
            _this.listaDipendenti1 = resp.response;
            if (_this.listaDipendenti1) {
                _this.listaDipendenti1.forEach(function (element) {
                    _this.descrizioneDipendente1.push({
                        label: element.cognome + " " + element.nome,
                        value: { element: element },
                    });
                });
            }
        });
    };
    NewCourseUsersComponent.prototype.getCorso = function () {
        var _this = this;
        this.courseService.getAll().subscribe(function (resp) {
            _this.listaCorsi = resp.response;
            if (_this.listaCorsi) {
                _this.ultimoCorso = _this.listaCorsi[_this.listaCorsi.length - 1];
            }
        });
    };
    NewCourseUsersComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _core_services_course_users_service__WEBPACK_IMPORTED_MODULE_7__["CourseUsersService"] },
        { type: _core_services_course_service__WEBPACK_IMPORTED_MODULE_3__["CourseService"] },
        { type: _core_services_employees_services__WEBPACK_IMPORTED_MODULE_4__["EmployeesServices"] },
        { type: _core_services_teachers_service__WEBPACK_IMPORTED_MODULE_5__["TeachersService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] }
    ]; };
    NewCourseUsersComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-new-course-users',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./new-course-users.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/new-course/new-course-users/new-course-users/new-course-users.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./new-course-users.component.css */ "./src/app/views/new-course/new-course-users/new-course-users/new-course-users.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _core_services_course_users_service__WEBPACK_IMPORTED_MODULE_7__["CourseUsersService"], _core_services_course_service__WEBPACK_IMPORTED_MODULE_3__["CourseService"], _core_services_employees_services__WEBPACK_IMPORTED_MODULE_4__["EmployeesServices"], _core_services_teachers_service__WEBPACK_IMPORTED_MODULE_5__["TeachersService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]])
    ], NewCourseUsersComponent);
    return NewCourseUsersComponent;
}());



/***/ })

}]);
//# sourceMappingURL=views-new-course-new-course-users-new-course-users-module.js.map