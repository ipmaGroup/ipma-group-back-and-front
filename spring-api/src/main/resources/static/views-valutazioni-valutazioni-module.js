(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-valutazioni-valutazioni-module"],{

/***/ "./node_modules/primeng/fesm5/primeng-sidebar.js":
/*!*******************************************************!*\
  !*** ./node_modules/primeng/fesm5/primeng-sidebar.js ***!
  \*******************************************************/
/*! exports provided: Sidebar, SidebarModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Sidebar", function() { return Sidebar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarModule", function() { return SidebarModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var primeng_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/dom */ "./node_modules/primeng/fesm5/primeng-dom.js");





var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var Sidebar = /** @class */ (function () {
    function Sidebar(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        this.position = 'left';
        this.blockScroll = false;
        this.autoZIndex = true;
        this.baseZIndex = 0;
        this.modal = true;
        this.dismissible = true;
        this.showCloseIcon = true;
        this.closeOnEscape = true;
        this.onShow = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onHide = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.visibleChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    Sidebar.prototype.ngAfterViewInit = function () {
        this.initialized = true;
        if (this.appendTo) {
            if (this.appendTo === 'body')
                document.body.appendChild(this.containerViewChild.nativeElement);
            else
                primeng_dom__WEBPACK_IMPORTED_MODULE_3__["DomHandler"].appendChild(this.containerViewChild.nativeElement, this.appendTo);
        }
        if (this.visible) {
            this.show();
        }
    };
    Object.defineProperty(Sidebar.prototype, "visible", {
        get: function () {
            return this._visible;
        },
        set: function (val) {
            this._visible = val;
            if (this.initialized && this.containerViewChild && this.containerViewChild.nativeElement) {
                if (this._visible)
                    this.show();
                else {
                    if (this.preventVisibleChangePropagation)
                        this.preventVisibleChangePropagation = false;
                    else
                        this.hide();
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Sidebar.prototype.ngAfterViewChecked = function () {
        if (this.executePostDisplayActions) {
            this.onShow.emit({});
            this.executePostDisplayActions = false;
        }
    };
    Sidebar.prototype.show = function () {
        this.executePostDisplayActions = true;
        if (this.autoZIndex) {
            this.containerViewChild.nativeElement.style.zIndex = String(this.baseZIndex + (++primeng_dom__WEBPACK_IMPORTED_MODULE_3__["DomHandler"].zindex));
        }
        if (this.modal) {
            this.enableModality();
        }
    };
    Sidebar.prototype.hide = function () {
        this.onHide.emit({});
        if (this.modal) {
            this.disableModality();
        }
    };
    Sidebar.prototype.close = function (event) {
        this.preventVisibleChangePropagation = true;
        this.hide();
        this.visibleChange.emit(false);
        event.preventDefault();
    };
    Sidebar.prototype.enableModality = function () {
        var _this = this;
        if (!this.mask) {
            this.mask = document.createElement('div');
            this.mask.style.zIndex = String(parseInt(this.containerViewChild.nativeElement.style.zIndex) - 1);
            primeng_dom__WEBPACK_IMPORTED_MODULE_3__["DomHandler"].addMultipleClasses(this.mask, 'ui-widget-overlay ui-sidebar-mask');
            if (this.dismissible) {
                this.maskClickListener = this.renderer.listen(this.mask, 'click', function (event) {
                    if (_this.dismissible) {
                        _this.close(event);
                    }
                });
            }
            document.body.appendChild(this.mask);
            if (this.blockScroll) {
                primeng_dom__WEBPACK_IMPORTED_MODULE_3__["DomHandler"].addClass(document.body, 'ui-overflow-hidden');
            }
        }
    };
    Sidebar.prototype.disableModality = function () {
        if (this.mask) {
            this.unbindMaskClickListener();
            document.body.removeChild(this.mask);
            if (this.blockScroll) {
                primeng_dom__WEBPACK_IMPORTED_MODULE_3__["DomHandler"].removeClass(document.body, 'ui-overflow-hidden');
            }
            this.mask = null;
        }
    };
    Sidebar.prototype.onAnimationStart = function (event) {
        switch (event.toState) {
            case 'visible':
                if (this.closeOnEscape) {
                    this.bindDocumentEscapeListener();
                }
                break;
            case 'hidden':
                this.unbindGlobalListeners();
                break;
        }
    };
    Sidebar.prototype.bindDocumentEscapeListener = function () {
        var _this = this;
        this.documentEscapeListener = this.renderer.listen('document', 'keydown', function (event) {
            if (event.which == 27) {
                if (parseInt(_this.containerViewChild.nativeElement.style.zIndex) === (primeng_dom__WEBPACK_IMPORTED_MODULE_3__["DomHandler"].zindex + _this.baseZIndex)) {
                    _this.close(event);
                }
            }
        });
    };
    Sidebar.prototype.unbindDocumentEscapeListener = function () {
        if (this.documentEscapeListener) {
            this.documentEscapeListener();
            this.documentEscapeListener = null;
        }
    };
    Sidebar.prototype.unbindMaskClickListener = function () {
        if (this.maskClickListener) {
            this.maskClickListener();
            this.maskClickListener = null;
        }
    };
    Sidebar.prototype.unbindGlobalListeners = function () {
        this.unbindMaskClickListener();
        this.unbindDocumentEscapeListener();
    };
    Sidebar.prototype.ngOnDestroy = function () {
        this.initialized = false;
        if (this.visible) {
            this.hide();
        }
        if (this.appendTo) {
            this.el.nativeElement.appendChild(this.containerViewChild.nativeElement);
        }
        this.unbindGlobalListeners();
    };
    Sidebar.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()
    ], Sidebar.prototype, "position", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()
    ], Sidebar.prototype, "fullScreen", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()
    ], Sidebar.prototype, "appendTo", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()
    ], Sidebar.prototype, "blockScroll", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()
    ], Sidebar.prototype, "style", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()
    ], Sidebar.prototype, "styleClass", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()
    ], Sidebar.prototype, "ariaCloseLabel", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()
    ], Sidebar.prototype, "autoZIndex", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()
    ], Sidebar.prototype, "baseZIndex", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()
    ], Sidebar.prototype, "modal", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()
    ], Sidebar.prototype, "dismissible", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()
    ], Sidebar.prototype, "showCloseIcon", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()
    ], Sidebar.prototype, "closeOnEscape", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('container')
    ], Sidebar.prototype, "containerViewChild", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])()
    ], Sidebar.prototype, "onShow", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])()
    ], Sidebar.prototype, "onHide", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])()
    ], Sidebar.prototype, "visibleChange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()
    ], Sidebar.prototype, "visible", null);
    Sidebar = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'p-sidebar',
            template: "\n        <div #container [ngClass]=\"{'ui-sidebar ui-widget ui-widget-content ui-shadow':true, 'ui-sidebar-active': visible, \n            'ui-sidebar-left': (position === 'left'), 'ui-sidebar-right': (position === 'right'),\n            'ui-sidebar-top': (position === 'top'), 'ui-sidebar-bottom': (position === 'bottom'), \n            'ui-sidebar-full': fullScreen}\"\n            [@panelState]=\"visible ? 'visible' : 'hidden'\" (@panelState.start)=\"onAnimationStart($event)\" [ngStyle]=\"style\" [class]=\"styleClass\"  role=\"complementary\" [attr.aria-modal]=\"modal\">\n            <a [ngClass]=\"{'ui-sidebar-close ui-corner-all':true}\" *ngIf=\"showCloseIcon\" tabindex=\"0\" role=\"button\" (click)=\"close($event)\" (keydown.enter)=\"close($event)\" [attr.aria-label]=\"ariaCloseLabel\">\n                <span class=\"pi pi-times\"></span>\n            </a>\n            <ng-content></ng-content>\n        </div>\n    ",
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["trigger"])('panelState', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["state"])('hidden', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                        opacity: 0
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["state"])('visible', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                        opacity: 1
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('visible => hidden', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('300ms ease-in')),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('hidden => visible', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('300ms ease-out'))
                ])
            ],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].Default
        })
    ], Sidebar);
    return Sidebar;
}());
var SidebarModule = /** @class */ (function () {
    function SidebarModule() {
    }
    SidebarModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
            exports: [Sidebar],
            declarations: [Sidebar]
        })
    ], SidebarModule);
    return SidebarModule;
}());

/**
 * Generated bundle index. Do not edit.
 */


//# sourceMappingURL=primeng-sidebar.js.map


/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/valutazioni/valutazioni/valutazioni.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/valutazioni/valutazioni/valutazioni.component.html ***!
  \****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div>\r\n  <h3>{{ desc }}</h3>\r\n  <table class=\"table\">\r\n    <thead>\r\n      <tr>\r\n        <th scope=\"col\">criterio</th>\r\n        <th scope=\"col\">aggiungi/modifica</th>\r\n        <th scope=\"col\">valore</th>\r\n      </tr>\r\n    </thead>\r\n    <tbody>\r\n      <tr>\r\n        <td>utilità del corso per la crescita professionale</td>\r\n        <td>\r\n          <form [formGroup]=\"formGroupCorsi\" class=\"mb-3\">\r\n            <div class=\"form-group ui-g-12 ui-md-6\">\r\n              <input\r\n                type=\"text\"\r\n                class=\"form-control\"\r\n                formControlName=\"valore\"\r\n                required\r\n              />\r\n            </div>\r\n\r\n            <div>\r\n              <button\r\n                class=\"btn btn-success\"\r\n                (click)=\"creaCorsoVoto('utilita')\"\r\n                type=\"button\"\r\n              >\r\n                <i class=\"fa fa-check mr-2\" aria-hidden=\"true\"></i>salva\r\n              </button>\r\n            </div>\r\n          </form>\r\n        </td>\r\n        <td>{{ listaVotiCorso2.utilita }}</td>\r\n      </tr>\r\n      <tr>\r\n        <td>utilità del corso nell'ambito lavorativo attuale</td>\r\n        <td>\r\n          <form [formGroup]=\"formGroupCorsi\" class=\"mb-3\">\r\n            <div class=\"form-group ui-g-12 ui-md-6\">\r\n              <input\r\n                type=\"text\"\r\n                class=\"form-control\"\r\n                formControlName=\"valore\"\r\n                required\r\n              />\r\n            </div>\r\n\r\n            <div>\r\n              <button\r\n                class=\"btn btn-success\"\r\n                (click)=\"creaCorsoVoto('lavorativo')\"\r\n                type=\"button\"\r\n              >\r\n                <i class=\"fa fa-check mr-2\" aria-hidden=\"true\"></i>salva\r\n              </button>\r\n            </div>\r\n          </form>\r\n        </td>\r\n        <td>{{ listaVotiCorso2.lavorativo }}</td>\r\n      </tr>\r\n      <tr>\r\n        <td>scelta argomenti specifici</td>\r\n        <td>\r\n          <form [formGroup]=\"formGroupCorsi\" class=\"mb-3\">\r\n            <div class=\"form-group ui-g-12 ui-md-6\">\r\n              <input\r\n                type=\"text\"\r\n                class=\"form-control\"\r\n                formControlName=\"valore\"\r\n                required\r\n              />\r\n            </div>\r\n\r\n            <div>\r\n              <button\r\n                class=\"btn btn-success\"\r\n                (click)=\"creaCorsoVoto('specifici')\"\r\n                type=\"button\"\r\n              >\r\n                <i class=\"fa fa-check mr-2\" aria-hidden=\"true\"></i>salva\r\n              </button>\r\n            </div>\r\n          </form>\r\n        </td>\r\n        <td>{{ listaVotiCorso2.specifici }}</td>\r\n      </tr>\r\n      <tr>\r\n        <td>livello approfondimento degli argomenti specifici</td>\r\n        <td>\r\n          <form [formGroup]=\"formGroupCorsi\" class=\"mb-3\">\r\n            <div class=\"form-group ui-g-12 ui-md-6\">\r\n              <input\r\n                type=\"text\"\r\n                class=\"form-control\"\r\n                formControlName=\"valore\"\r\n                required\r\n              />\r\n            </div>\r\n\r\n            <div>\r\n              <button\r\n                class=\"btn btn-success\"\r\n                (click)=\"creaCorsoVoto('approfondimento')\"\r\n                type=\"button\"\r\n              >\r\n                <i class=\"fa fa-check mr-2\" aria-hidden=\"true\"></i>salva\r\n              </button>\r\n            </div>\r\n          </form>\r\n        </td>\r\n        <td>{{ listaVotiCorso2.approfondimento }}</td>\r\n      </tr>\r\n      <tr>\r\n        <td>durata del corso</td>\r\n        <td>\r\n          <form [formGroup]=\"formGroupCorsi\" class=\"mb-3\">\r\n            <div class=\"form-group ui-g-12 ui-md-6\">\r\n              <input\r\n                type=\"text\"\r\n                class=\"form-control\"\r\n                formControlName=\"valore\"\r\n                required\r\n              />\r\n            </div>\r\n\r\n            <div>\r\n              <button\r\n                class=\"btn btn-success\"\r\n                (click)=\"creaCorsoVoto('durata')\"\r\n                type=\"button\"\r\n              >\r\n                <i class=\"fa fa-check mr-2\" aria-hidden=\"true\"></i>salva\r\n              </button>\r\n            </div>\r\n          </form>\r\n        </td>\r\n        <td>{{ listaVotiCorso2.durata }}</td>\r\n      </tr>\r\n      <tr>\r\n        <td>corretta suddivisione fra teoria e pratica</td>\r\n        <td>\r\n          <form [formGroup]=\"formGroupCorsi\" class=\"mb-3\">\r\n            <div class=\"form-group ui-g-12 ui-md-6\">\r\n              <input\r\n                type=\"text\"\r\n                class=\"form-control\"\r\n                formControlName=\"valore\"\r\n                required\r\n              />\r\n            </div>\r\n\r\n            <div>\r\n              <button\r\n                class=\"btn btn-success\"\r\n                (click)=\"creaCorsoVoto('suddivisione')\"\r\n                type=\"button\"\r\n              >\r\n                <i class=\"fa fa-check mr-2\" aria-hidden=\"true\"></i>salva\r\n              </button>\r\n            </div>\r\n          </form>\r\n        </td>\r\n        <td>{{ listaVotiCorso2.suddivisione }}</td>\r\n      </tr>\r\n      <tr>\r\n        <td>efficacia delle esercitazioni</td>\r\n        <td>\r\n          <form [formGroup]=\"formGroupCorsi\" class=\"mb-3\">\r\n            <div class=\"form-group ui-g-12 ui-md-6\">\r\n              <input\r\n                type=\"text\"\r\n                class=\"form-control\"\r\n                formControlName=\"valore\"\r\n                required\r\n              />\r\n            </div>\r\n\r\n            <div>\r\n              <button\r\n                class=\"btn btn-success\"\r\n                (click)=\"creaCorsoVoto('esercitazioni')\"\r\n                type=\"button\"\r\n              >\r\n                <i class=\"fa fa-check mr-2\" aria-hidden=\"true\"></i>salva\r\n              </button>\r\n            </div>\r\n          </form>\r\n        </td>\r\n        <td>{{ listaVotiCorso2.esercitazioni }}</td>\r\n      </tr>\r\n      <tr>\r\n        <td>qualità materiale didattico</td>\r\n        <td>\r\n          <form [formGroup]=\"formGroupCorsi\" class=\"mb-3\">\r\n            <div class=\"form-group ui-g-12 ui-md-6\">\r\n              <input\r\n                type=\"text\"\r\n                class=\"form-control\"\r\n                formControlName=\"valore\"\r\n                required\r\n              />\r\n            </div>\r\n\r\n            <div>\r\n              <button\r\n                class=\"btn btn-success\"\r\n                (click)=\"creaCorsoVoto('materiale')\"\r\n                type=\"button\"\r\n              >\r\n                <i class=\"fa fa-check mr-2\" aria-hidden=\"true\"></i>salva\r\n              </button>\r\n            </div>\r\n          </form>\r\n        </td>\r\n        <td>{{ listaVotiCorso2.materiale }}</td>\r\n      </tr>\r\n    </tbody>\r\n  </table>\r\n\r\n  <div>\r\n    <h3>\r\n      {{ nome }}\r\n    </h3>\r\n    <table class=\"table\">\r\n      <thead>\r\n        <tr>\r\n          <th scope=\"col\">criterio</th>\r\n          <th scope=\"col\">aggiungi/modifica</th>\r\n          <th scope=\"col\">valore</th>\r\n        </tr>\r\n      </thead>\r\n      <tbody>\r\n        <tr>\r\n          <td>completezza</td>\r\n          <td>\r\n            <form [formGroup]=\"formGroupDocente\" class=\"mb-3\">\r\n              <div class=\"form-group ui-g-12 ui-md-6\">\r\n                <input\r\n                  type=\"text\"\r\n                  class=\"form-control\"\r\n                  formControlName=\"valore\"\r\n                  required\r\n                />\r\n              </div>\r\n\r\n              <div>\r\n                <button\r\n                  class=\"btn btn-success\"\r\n                  (click)=\"creaDocenteVoto('completezza')\"\r\n                  type=\"button\"\r\n                >\r\n                  <i class=\"fa fa-check mr-2\" aria-hidden=\"true\"></i>salva\r\n                </button>\r\n              </div>\r\n            </form>\r\n          </td>\r\n          <td>{{ listavotiDocente2.completezza }}</td>\r\n        </tr>\r\n        <tr>\r\n          <td>chiarezza nelle esposizioni</td>\r\n          <td>\r\n            <form [formGroup]=\"formGroupDocente\" class=\"mb-3\">\r\n              <div class=\"form-group ui-g-12 ui-md-6\">\r\n                <input\r\n                  type=\"text\"\r\n                  class=\"form-control\"\r\n                  formControlName=\"valore\"\r\n                  required\r\n                />\r\n              </div>\r\n\r\n              <div>\r\n                <button\r\n                  class=\"btn btn-success\"\r\n                  (click)=\"creaDocenteVoto('chiarezza')\"\r\n                  type=\"button\"\r\n                >\r\n                  <i class=\"fa fa-check mr-2\" aria-hidden=\"true\"></i>salva\r\n                </button>\r\n              </div>\r\n            </form>\r\n          </td>\r\n          <td>{{ listavotiDocente2.chiarezza }}</td>\r\n        </tr>\r\n        <tr>\r\n          <td>disponibilità</td>\r\n          <td>\r\n            <form [formGroup]=\"formGroupDocente\" class=\"mb-3\">\r\n              <div class=\"form-group ui-g-12 ui-md-6\">\r\n                <input\r\n                  type=\"text\"\r\n                  class=\"form-control\"\r\n                  formControlName=\"valore\"\r\n                  required\r\n                />\r\n              </div>\r\n\r\n              <div>\r\n                <button\r\n                  class=\"btn btn-success\"\r\n                  (click)=\"creaDocenteVoto('disponibilità')\"\r\n                  type=\"button\"\r\n                >\r\n                  <i class=\"fa fa-check mr-2\" aria-hidden=\"true\"></i>salva\r\n                </button>\r\n              </div>\r\n            </form>\r\n          </td>\r\n          <td>{{ listavotiDocente2.disponibilita }}</td>\r\n        </tr>\r\n      </tbody>\r\n    </table>\r\n\r\n    <table class=\"table\">\r\n      <thead>\r\n        <tr>\r\n          <th scope=\"col\">nome</th>\r\n          <th scope=\"col\">cognome</th>\r\n          <th></th>\r\n        </tr>\r\n      </thead>\r\n      <tbody>\r\n        <tr *ngFor=\"let valore of listaDipendenti\">\r\n          <td>{{ valore.dipendente.nome }}</td>\r\n          <td>{{ valore.dipendente.cognome }}</td>\r\n          <td>\r\n            <button\r\n              class=\"btn btn-info mr-2\"\r\n              (click)=\"vota(valore.dipendente.dipendenteId)\"\r\n            >\r\n              <i class=\"fa fa-edit\" aria-hidden=\"true\"></i>\r\n            </button>\r\n          </td>\r\n        </tr>\r\n      </tbody>\r\n    </table>\r\n\r\n    <p-sidebar\r\n      [(visible)]=\"mostraSidebar\"\r\n      position=\"right\"\r\n      [baseZIndex]=\"10000\"\r\n      [style]=\"{ width: '50%' }\"\r\n    >\r\n      <h1>{{ utSel }}</h1>\r\n      <table class=\"table\">\r\n        <thead>\r\n          <tr>\r\n            <th scope=\"col\">criterio</th>\r\n            <th scope=\"col\">aggiungi/modifica</th>\r\n            <th scope=\"col\">valore</th>\r\n          </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr>\r\n            <td>impegno</td>\r\n            <td>\r\n              <form [formGroup]=\"formGroupDip\" class=\"mb-3\">\r\n                <div>\r\n                  <div class=\"form-group ui-g-12 ui-md-6\">\r\n                    <input\r\n                      type=\"text\"\r\n                      class=\"form-control\"\r\n                      formControlName=\"valore\"\r\n                      required\r\n                    />\r\n                  </div>\r\n                  <button\r\n                    class=\"btn btn-success\"\r\n                    (click)=\"creaDipVoto('impegno')\"\r\n                    type=\"button\"\r\n                  >\r\n                    <i class=\"fa fa-check mr-2\" aria-hidden=\"true\"></i>salva\r\n                  </button>\r\n                </div>\r\n              </form>\r\n            </td>\r\n            <td>{{ listaDipendenteVoto2.impegno }}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>logica</td>\r\n            <td>\r\n              <form [formGroup]=\"formGroupDip\" class=\"mb-3\">\r\n                <div>\r\n                  <div class=\"form-group ui-g-12 ui-md-6\">\r\n                    <input\r\n                      type=\"text\"\r\n                      class=\"form-control\"\r\n                      formControlName=\"valore\"\r\n                      required\r\n                    />\r\n                  </div>\r\n                  <button\r\n                    class=\"btn btn-success\"\r\n                    (click)=\"creaDipVoto('logica')\"\r\n                    type=\"button\"\r\n                  >\r\n                    <i class=\"fa fa-check mr-2\" aria-hidden=\"true\"></i>salva\r\n                  </button>\r\n                </div>\r\n              </form>\r\n            </td>\r\n            <td>{{ listaDipendenteVoto2.logica }}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>apprendimento</td>\r\n            <td>\r\n              <form [formGroup]=\"formGroupDip\" class=\"mb-3\">\r\n                <div>\r\n                  <div class=\"form-group ui-g-12 ui-md-6\">\r\n                    <input\r\n                      type=\"text\"\r\n                      class=\"form-control\"\r\n                      formControlName=\"valore\"\r\n                      required\r\n                    />\r\n                  </div>\r\n                  <button\r\n                    class=\"btn btn-success\"\r\n                    (click)=\"creaDipVoto('apprendimento')\"\r\n                    type=\"button\"\r\n                  >\r\n                    <i class=\"fa fa-check mr-2\" aria-hidden=\"true\"></i>salva\r\n                  </button>\r\n                </div>\r\n              </form>\r\n            </td>\r\n            <td>{{ listaDipendenteVoto2.apprendimento }}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>velocità</td>\r\n            <td>\r\n              <form [formGroup]=\"formGroupDip\" class=\"mb-3\">\r\n                <div>\r\n                  <div class=\"form-group ui-g-12 ui-md-6\">\r\n                    <input\r\n                      type=\"text\"\r\n                      class=\"form-control\"\r\n                      formControlName=\"valore\"\r\n                      required\r\n                    />\r\n                  </div>\r\n                  <button\r\n                    class=\"btn btn-success\"\r\n                    (click)=\"creaDipVoto('velocita')\"\r\n                    type=\"button\"\r\n                  >\r\n                    <i class=\"fa fa-check mr-2\" aria-hidden=\"true\"></i>salva\r\n                  </button>\r\n                </div>\r\n              </form>\r\n            </td>\r\n            <td>{{ listaDipendenteVoto2.velocita }}</td>\r\n          </tr>\r\n          <tr>\r\n            <td>ordine e precisione</td>\r\n            <td>\r\n              <form [formGroup]=\"formGroupDip\" class=\"mb-3\">\r\n                <div>\r\n                  <div class=\"form-group ui-g-12 ui-md-6\">\r\n                    <input\r\n                      type=\"text\"\r\n                      class=\"form-control\"\r\n                      formControlName=\"valore\"\r\n                      required\r\n                    />\r\n                  </div>\r\n                  <button\r\n                    class=\"btn btn-success\"\r\n                    (click)=\"creaDipVoto('ordine')\"\r\n                    type=\"button\"\r\n                  >\r\n                    <i class=\"fa fa-check mr-2\" aria-hidden=\"true\"></i>salva\r\n                  </button>\r\n                </div>\r\n              </form>\r\n            </td>\r\n            <td>{{ listaDipendenteVoto2.ordine }}</td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n    </p-sidebar>\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ "./src/app/core/services/course.service.ts":
/*!*************************************************!*\
  !*** ./src/app/core/services/course.service.ts ***!
  \*************************************************/
/*! exports provided: CourseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CourseService", function() { return CourseService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api.service */ "./src/app/core/services/api.service.ts");
/* harmony import */ var _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/enviroment */ "./src/app/shared/enviroment.ts");




var CourseService = /** @class */ (function () {
    function CourseService(api) {
        this.api = api;
        this.path = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.courseController;
        this.misure = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.misureController;
        this.tipi = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.tipiController;
        this.tipologie = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.tipologieController;
        this.pathFilter = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.courseControllerFilter;
        this.listSoggetti = [];
    }
    CourseService.prototype.getAll = function () {
        return this.api.get(this.path);
    };
    CourseService.prototype.getAllPage = function (primo, secondo) {
        return this.api.getByPage(this.path, primo, secondo);
    };
    CourseService.prototype.getById = function (ut, inizio, fine) {
        return this.api.getById(this.pathFilter + inizio + "..." + fine, ut);
    };
    CourseService.prototype.add = function (item) {
        return this.api.post(this.path, item);
    };
    CourseService.prototype.deleteById = function (body) {
        return this.api.delete(this.path, body);
    };
    CourseService.prototype.update = function (soggetto) {
        return this.api.patch(this.path, soggetto);
    };
    CourseService.prototype.getMisura = function () {
        return this.api.get(this.misure);
    };
    CourseService.prototype.getTipo = function () {
        return this.api.get(this.tipi);
    };
    CourseService.prototype.getTipologia = function () {
        return this.api.get(this.tipologie);
    };
    CourseService.ctorParameters = function () { return [
        { type: _api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"] }
    ]; };
    CourseService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root",
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]])
    ], CourseService);
    return CourseService;
}());



/***/ }),

/***/ "./src/app/core/services/course_users.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/core/services/course_users.service.ts ***!
  \*******************************************************/
/*! exports provided: CourseUsersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CourseUsersService", function() { return CourseUsersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api.service */ "./src/app/core/services/api.service.ts");
/* harmony import */ var _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/enviroment */ "./src/app/shared/enviroment.ts");




var CourseUsersService = /** @class */ (function () {
    function CourseUsersService(api) {
        this.api = api;
        this.teachersPath = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.courseTeachersController;
        this.usersPath = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.courseUsersController;
        this.teachersPathFilter = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.courseTeachersControllerFilter;
        this.usersPathFilter = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.courseUsersControllerFilter;
        this.listSoggetti = [];
    }
    CourseUsersService.prototype.getAllCourseTeachers = function () {
        return this.api.get(this.teachersPath);
    };
    CourseUsersService.prototype.getAllCourseUsers = function () {
        return this.api.get(this.usersPath);
    };
    CourseUsersService.prototype.getAllPageTeachers = function (primo, secondo) {
        return this.api.getByPage(this.teachersPath, primo, secondo);
    };
    CourseUsersService.prototype.getAllPageUsers = function (primo, secondo) {
        return this.api.getByPage(this.usersPath, primo, secondo);
    };
    CourseUsersService.prototype.getByIdTeachers = function (ut, inizio, fine) {
        return this.api.getById(this.teachersPathFilter + inizio + "..." + fine, ut);
    };
    CourseUsersService.prototype.getByIdUsers = function (ut, inizio, fine) {
        return this.api.getById(this.usersPathFilter + inizio + "..." + fine, ut);
    };
    CourseUsersService.prototype.addTeachers = function (item) {
        return this.api.post(this.teachersPath, item);
    };
    CourseUsersService.prototype.addUsers = function (item) {
        return this.api.post(this.usersPath, item);
    };
    CourseUsersService.prototype.deleteByIdTeachers = function (body) {
        return this.api.delete(this.teachersPath, body);
    };
    CourseUsersService.prototype.deleteByIdUsers = function (body) {
        return this.api.delete(this.usersPath, body);
    };
    CourseUsersService.prototype.updateTeachers = function (soggetto) {
        return this.api.patch(this.teachersPath, soggetto);
    };
    CourseUsersService.prototype.updateUsers = function (soggetto) {
        return this.api.patch(this.usersPath, soggetto);
    };
    CourseUsersService.ctorParameters = function () { return [
        { type: _api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"] }
    ]; };
    CourseUsersService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root",
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]])
    ], CourseUsersService);
    return CourseUsersService;
}());



/***/ }),

/***/ "./src/app/core/services/employees.services.ts":
/*!*****************************************************!*\
  !*** ./src/app/core/services/employees.services.ts ***!
  \*****************************************************/
/*! exports provided: EmployeesServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeesServices", function() { return EmployeesServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api.service */ "./src/app/core/services/api.service.ts");
/* harmony import */ var _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/enviroment */ "./src/app/shared/enviroment.ts");




var EmployeesServices = /** @class */ (function () {
    function EmployeesServices(api) {
        this.api = api;
        this.pathFilter = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.employeesControllerFilter;
        this.path = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.employeesController;
        this.listSoggetti = [];
    }
    EmployeesServices.prototype.getAll = function () {
        return this.api.get(this.path);
    };
    EmployeesServices.prototype.getById = function (ut, inizio, fine) {
        return this.api.getById(this.pathFilter + inizio + "..." + fine, ut);
    };
    EmployeesServices.prototype.getAllPage = function (primo, secondo) {
        return this.api.getByPage(this.path, primo, secondo);
    };
    EmployeesServices.ctorParameters = function () { return [
        { type: _api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"] }
    ]; };
    EmployeesServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root",
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]])
    ], EmployeesServices);
    return EmployeesServices;
}());



/***/ }),

/***/ "./src/app/core/services/teachers.service.ts":
/*!***************************************************!*\
  !*** ./src/app/core/services/teachers.service.ts ***!
  \***************************************************/
/*! exports provided: TeachersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeachersService", function() { return TeachersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api.service */ "./src/app/core/services/api.service.ts");
/* harmony import */ var _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/enviroment */ "./src/app/shared/enviroment.ts");




var TeachersService = /** @class */ (function () {
    function TeachersService(api) {
        this.api = api;
        this.path = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.teachersController;
        this.pathFilter = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.teachersControllerFilter;
        this.listSoggetti = [];
    }
    TeachersService.prototype.getAll = function () {
        return this.api.get(this.path);
    };
    TeachersService.prototype.getAllPage = function (primo, secondo) {
        return this.api.getByPage(this.path, primo, secondo);
    };
    TeachersService.prototype.getById = function (ut, inizio, fine) {
        return this.api.getById(this.pathFilter + inizio + "..." + fine, ut);
    };
    TeachersService.prototype.add = function (item) {
        return this.api.post(this.path, item);
    };
    TeachersService.prototype.deleteById = function (body) {
        return this.api.delete(this.path, body);
    };
    TeachersService.prototype.update = function (soggetto) {
        return this.api.patch(this.path, soggetto);
    };
    TeachersService.ctorParameters = function () { return [
        { type: _api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"] }
    ]; };
    TeachersService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root",
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]])
    ], TeachersService);
    return TeachersService;
}());



/***/ }),

/***/ "./src/app/core/services/vote.service.ts":
/*!***********************************************!*\
  !*** ./src/app/core/services/vote.service.ts ***!
  \***********************************************/
/*! exports provided: VoteServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VoteServices", function() { return VoteServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api.service */ "./src/app/core/services/api.service.ts");
/* harmony import */ var _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/enviroment */ "./src/app/shared/enviroment.ts");




var VoteServices = /** @class */ (function () {
    function VoteServices(api) {
        this.api = api;
        this.path = _shared_enviroment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint.voteControl;
        this.listSoggetti = [];
    }
    VoteServices.prototype.getAllCourses = function (id, primo, secondo) {
        return this.api.getById(this.path + "courses/" + primo + "..." + secondo, id);
    };
    VoteServices.prototype.addCourses = function (body) {
        return this.api.post(this.path + "courses/", body);
    };
    VoteServices.prototype.getAllEmployees = function (id, primo, secondo) {
        return this.api.getById(this.path + "employees/" + primo + "..." + secondo, id);
    };
    VoteServices.prototype.addEmployees = function (body) {
        return this.api.post(this.path + "employees/", body);
    };
    VoteServices.prototype.getAllTeachers = function (id, primo, secondo) {
        return this.api.getById(this.path + "teachers/" + primo + "..." + secondo, id);
    };
    VoteServices.prototype.addTeachers = function (body) {
        return this.api.post(this.path + "teachers/", body);
    };
    VoteServices.ctorParameters = function () { return [
        { type: _api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"] }
    ]; };
    VoteServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root",
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]])
    ], VoteServices);
    return VoteServices;
}());



/***/ }),

/***/ "./src/app/views/valutazioni/valutazioni-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/views/valutazioni/valutazioni-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: ValutazioniRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValutazioniRoutingModule", function() { return ValutazioniRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _valutazioni_valutazioni_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./valutazioni/valutazioni.component */ "./src/app/views/valutazioni/valutazioni/valutazioni.component.ts");




var routes = [{
        path: '',
        component: _valutazioni_valutazioni_component__WEBPACK_IMPORTED_MODULE_3__["ValutazioniComponent"],
        data: {
            title: 'Valutazioni'
        }
    },];
var ValutazioniRoutingModule = /** @class */ (function () {
    function ValutazioniRoutingModule() {
    }
    ValutazioniRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], ValutazioniRoutingModule);
    return ValutazioniRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/valutazioni/valutazioni.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/views/valutazioni/valutazioni.module.ts ***!
  \*********************************************************/
/*! exports provided: ValutazioniModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValutazioniModule", function() { return ValutazioniModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _valutazioni_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./valutazioni-routing.module */ "./src/app/views/valutazioni/valutazioni-routing.module.ts");
/* harmony import */ var _valutazioni_valutazioni_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./valutazioni/valutazioni.component */ "./src/app/views/valutazioni/valutazioni/valutazioni.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var primeng_sidebar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/sidebar */ "./node_modules/primeng/fesm5/primeng-sidebar.js");







var ValutazioniModule = /** @class */ (function () {
    function ValutazioniModule() {
    }
    ValutazioniModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_valutazioni_valutazioni_component__WEBPACK_IMPORTED_MODULE_4__["ValutazioniComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _valutazioni_routing_module__WEBPACK_IMPORTED_MODULE_3__["ValutazioniRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                primeng_sidebar__WEBPACK_IMPORTED_MODULE_6__["SidebarModule"]
            ]
        })
    ], ValutazioniModule);
    return ValutazioniModule;
}());



/***/ }),

/***/ "./src/app/views/valutazioni/valutazioni/valutazioni.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/views/valutazioni/valutazioni/valutazioni.component.css ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3ZhbHV0YXppb25pL3ZhbHV0YXppb25pL3ZhbHV0YXppb25pLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/views/valutazioni/valutazioni/valutazioni.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/views/valutazioni/valutazioni/valutazioni.component.ts ***!
  \************************************************************************/
/*! exports provided: ValutazioniComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValutazioniComponent", function() { return ValutazioniComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_services_vote_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../core/services/vote.service */ "./src/app/core/services/vote.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _core_services_course_users_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../core/services/course_users.service */ "./src/app/core/services/course_users.service.ts");
/* harmony import */ var _core_services_course_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../core/services/course.service */ "./src/app/core/services/course.service.ts");
/* harmony import */ var _core_services_teachers_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../core/services/teachers.service */ "./src/app/core/services/teachers.service.ts");
/* harmony import */ var _core_services_employees_services__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../core/services/employees.services */ "./src/app/core/services/employees.services.ts");









var ValutazioniComponent = /** @class */ (function () {
    function ValutazioniComponent(courseUserServices, router, route, voteServices, fb, courseServices, teachersServices, employeesServices) {
        this.courseUserServices = courseUserServices;
        this.router = router;
        this.route = route;
        this.voteServices = voteServices;
        this.fb = fb;
        this.courseServices = courseServices;
        this.teachersServices = teachersServices;
        this.employeesServices = employeesServices;
        this.listaVotiCorso = [];
        this.listaDipendenti = [];
        this.listaVotiDocente = [];
        this.votiDocCorr = [];
        this.utSelLista = [];
    }
    ValutazioniComponent.prototype.ngOnInit = function () {
        this.listavotiDocente2 = {
            completezza: null,
            chiarezza: null,
            disponibilita: null,
        };
        this.listaVotiCorso2 = {
            utilita: null,
            lavorativo: null,
            specifici: null,
            approfondimento: null,
            durata: null,
            suddivisione: null,
            esercitazioni: null,
            materiale: null,
        };
        this.listaDipendenteVoto2 = {
            impegno: null,
            logica: null,
            apprendimento: null,
            velocita: null,
            ordine: null,
        };
        this.idDocente = this.route.snapshot.params["idDocente"];
        this.idCorso = this.route.snapshot.params["id"];
        this.interno = this.route.snapshot.params["interno"];
        this.loadAllCorsi();
        this.loadAllTeachers();
        this.getAllDateCorso();
        this.getAllteachersData();
        this.showDipendenti();
        this.formGroupCorsi = this.fb.group({
            corso: {
                corsoId: this.idCorso,
            },
            criterio: [""],
            valore: [""],
        });
        this.formGroupDocente = this.fb.group({
            corso: {
                corsoId: this.idCorso,
            },
            docente: {
                docenteId: this.idDocente,
            },
            criterio: [""],
            valore: [""],
            interno: this.interno,
        });
        this.formGroupDip = this.fb.group({
            corso: {
                corsoId: this.idCorso,
            },
            dipendente: {
                dipendenteId: [""],
            },
            criterio: [""],
            valore: [""],
        });
    };
    ValutazioniComponent.prototype.getAllteachersData = function () {
        var _this = this;
        var idO1 = { corsoId: this.idDocente };
        var idOBJ = Object.assign({}, idO1);
        if (this.interno == 0) {
            this.teachersServices.getById(idOBJ, 1, 1).subscribe(function (element) {
                _this.allTeachersData = element.response;
                _this.nome = _this.allTeachersData[0].titolo;
            });
        }
        else {
            this.employeesServices.getById(idOBJ, 1, 1).subscribe(function (element) {
                _this.allTeachersData = element.response;
                _this.nome =
                    _this.allTeachersData[0].nome + _this.allTeachersData[0].cognome;
            });
        }
    };
    ValutazioniComponent.prototype.getAllDateCorso = function () {
        var _this = this;
        var idO1 = { corsoId: this.idCorso };
        var idOBJ = Object.assign({}, idO1);
        this.courseServices.getById(idOBJ, 1, 1).subscribe(function (element) {
            _this.allDatacorso = element.response[0];
            _this.desc = _this.allDatacorso.descrizione;
        });
    };
    ValutazioniComponent.prototype.loadAllCorsi = function () {
        var _this = this;
        this.listaVotiCorso = [];
        var idO1 = { corsoId: this.idCorso };
        var idOBJ = Object.assign({}, idO1);
        this.voteServices.getAllCourses(idOBJ, 1, 200).subscribe(function (element) {
            element.response.forEach(function (element2) {
                if (element2.corso.corsoId == _this.idCorso) {
                    if (element2.criterio == "utilita")
                        _this.listaVotiCorso2.utilita = element2.valore;
                    if (element2.criterio == "lavorativo")
                        _this.listaVotiCorso2.lavorativo = element2.valore;
                    if (element2.criterio == "specifici")
                        _this.listaVotiCorso2.specifici = element2.valore;
                    if (element2.criterio == "approfondimento")
                        _this.listaVotiCorso2.approfondimento = element2.valore;
                    if (element2.criterio == "durata")
                        _this.listaVotiCorso2.durata = element2.valore;
                    if (element2.criterio == "suddivisione")
                        _this.listaVotiCorso2.suddivisione = element2.valore;
                    if (element2.criterio == "esercitazioni")
                        _this.listaVotiCorso2.esercitazioni = element2.valore;
                    if (element2.criterio == "materiale")
                        _this.listaVotiCorso2.materiale = element2.valore;
                }
                _this.listaVotiCorso.push(element2);
            });
        });
    };
    ValutazioniComponent.prototype.loadAllTeachers = function () {
        var _this = this;
        this.listaVotiDocente = [];
        var idO1 = { corsoId: this.idCorso };
        var idOBJ = Object.assign({}, idO1);
        this.voteServices.getAllTeachers(idOBJ, 1, 200).subscribe(function (element) {
            element.response.forEach(function (element2) {
                if (element2.corso.corsoId == _this.idCorso) {
                    console.log(element2.criterio);
                    if (element2.criterio == "chiarezza")
                        _this.listavotiDocente2.chiarezza = element2.valore;
                    if (element2.criterio == "disponibilità")
                        _this.listavotiDocente2.disponibilita = element2.valore;
                    if (element2.criterio == "completezza")
                        _this.listavotiDocente2.completezza = element2.valore;
                    _this.listaVotiDocente.push(element2);
                }
            });
        });
        console.log(this.listavotiDocente2);
    };
    ValutazioniComponent.prototype.loadallDipVote = function (id) {
        var _this = this;
        this.votiDocCorr = [];
        this.listaDipendenteVoto2 = {
            impegno: null,
            logica: null,
            apprendimento: null,
            velocita: null,
            ordine: null,
        };
        var idO1 = { corsoId: this.idCorso };
        var idOBJ = Object.assign({}, idO1);
        this.voteServices.getAllEmployees(idOBJ, 1, 200).subscribe(function (element) {
            element.response.forEach(function (element2) {
                if (element2.corso.corsoId == _this.idCorso &&
                    element2.dipendente.dipendenteId == id) {
                    if (element2.criterio == "impegno")
                        _this.listaDipendenteVoto2.impegno = element2.valore;
                    if (element2.criterio == "logica")
                        _this.listaDipendenteVoto2.logica = element2.valore;
                    if (element2.criterio == "apprendimento")
                        _this.listaDipendenteVoto2.apprendimento = element2.valore;
                    if (element2.criterio == "velocita")
                        _this.listaDipendenteVoto2.velocita = element2.valore;
                    if (element2.criterio == "ordine")
                        _this.listaDipendenteVoto2.ordine = element2.valore;
                    _this.votiDocCorr.push(element2);
                }
            });
            console.log(_this.listaDipendenteVoto2);
        });
        this.formGroupDip = this.fb.group({
            corso: {
                corsoId: this.idCorso,
            },
            dipendente: {
                dipendenteId: id,
            },
            criterio: [""],
            valore: [""],
        });
    };
    ValutazioniComponent.prototype.creaCorsoVoto = function (crite) {
        var _this = this;
        this.formGroupCorsi.value["criterio"] = crite;
        var obj = Object.assign({}, this.formGroupCorsi);
        this.voteServices.addCourses(obj.value).subscribe(function () {
            _this.loadAllCorsi();
        });
        this.formGroupCorsi = this.fb.group({
            corso: {
                corsoId: this.idCorso,
            },
            criterio: [""],
            valore: [""],
        });
    };
    ValutazioniComponent.prototype.creaDocenteVoto = function (crite) {
        var _this = this;
        this.formGroupDocente.value["criterio"] = crite;
        var obj = Object.assign({}, this.formGroupDocente);
        this.voteServices.addTeachers(obj.value).subscribe(function () {
            _this.loadAllTeachers();
        });
        this.formGroupDocente = this.fb.group({
            corso: {
                corsoId: this.idCorso,
            },
            docente: {
                docenteId: this.idDocente,
            },
            criterio: [""],
            valore: [""],
            interno: this.interno,
        });
    };
    ValutazioniComponent.prototype.showDipendenti = function () {
        var _this = this;
        this.courseUserServices.getAllCourseUsers().subscribe(function (element) {
            for (var i = 0; i < element.response.length; i++) {
                if (element.response[i].corso.corsoId == _this.idCorso) {
                    _this.listaDipendenti.push(element.response[i]);
                }
            }
        });
    };
    ValutazioniComponent.prototype.vota = function (id) {
        var _this = this;
        var idO1 = {
            corso: { dipendenteId: id },
        };
        var idOBJ = Object.assign({}, idO1);
        this.employeesServices.getById(idOBJ, 1, 100).subscribe(function (element) {
            for (var i = 0; i < element.response.length; i++) {
                if (element.response[i].dipendenteId == id) {
                    _this.utSel =
                        element.response[i].nome + " " + element.response[i].cognome;
                    _this.utSelLista = element.response[i];
                }
            }
        });
        this.mostraSidebar = true;
        this.loadallDipVote(id);
    };
    ValutazioniComponent.prototype.creaDipVoto = function (crite) {
        var _this = this;
        this.formGroupDip.value["criterio"] = crite;
        var id = this.utSelLista["dipendenteId"];
        var obj = Object.assign({}, this.formGroupDip);
        console.log(obj.value);
        this.voteServices.addEmployees(obj.value).subscribe(function () {
            _this.loadallDipVote(id);
        });
        this.formGroupDip = this.fb.group({
            corso: {
                corsoId: this.idCorso,
            },
            dipendente: {
                dipendenteId: id,
            },
            criterio: [""],
            valore: [""],
        });
    };
    ValutazioniComponent.ctorParameters = function () { return [
        { type: _core_services_course_users_service__WEBPACK_IMPORTED_MODULE_5__["CourseUsersService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _core_services_vote_service__WEBPACK_IMPORTED_MODULE_3__["VoteServices"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
        { type: _core_services_course_service__WEBPACK_IMPORTED_MODULE_6__["CourseService"] },
        { type: _core_services_teachers_service__WEBPACK_IMPORTED_MODULE_7__["TeachersService"] },
        { type: _core_services_employees_services__WEBPACK_IMPORTED_MODULE_8__["EmployeesServices"] }
    ]; };
    ValutazioniComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-valutazioni",
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./valutazioni.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/valutazioni/valutazioni/valutazioni.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./valutazioni.component.css */ "./src/app/views/valutazioni/valutazioni/valutazioni.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_core_services_course_users_service__WEBPACK_IMPORTED_MODULE_5__["CourseUsersService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _core_services_vote_service__WEBPACK_IMPORTED_MODULE_3__["VoteServices"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _core_services_course_service__WEBPACK_IMPORTED_MODULE_6__["CourseService"],
            _core_services_teachers_service__WEBPACK_IMPORTED_MODULE_7__["TeachersService"],
            _core_services_employees_services__WEBPACK_IMPORTED_MODULE_8__["EmployeesServices"]])
    ], ValutazioniComponent);
    return ValutazioniComponent;
}());



/***/ })

}]);
//# sourceMappingURL=views-valutazioni-valutazioni-module.js.map