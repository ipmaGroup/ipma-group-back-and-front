package com.its.ipma.corsi.corsi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.its.ipma.corsi.corsi.dao.CorsiDao;

public interface CorsiRepository extends JpaRepository<CorsiDao, Integer> {
}
