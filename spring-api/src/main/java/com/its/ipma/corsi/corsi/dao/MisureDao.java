package com.its.ipma.corsi.corsi.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.its.ipma.corsi.corsi.dto.MisureDto;

import lombok.Data;

@Entity
@Table(name = "misure")
@Data
public class MisureDao {

	@Id
	@Column(name = "misura_id")
	private Integer misuraId;

	@Column(name = "descrizione")
	private String descrizione;

	
	public MisureDto convertToDto() {

		MisureDto dto = new MisureDto();

		dto.setMisuraId(this.getMisuraId());
		dto.setDescrizione(this.getDescrizione());

		return dto;
	}
}
