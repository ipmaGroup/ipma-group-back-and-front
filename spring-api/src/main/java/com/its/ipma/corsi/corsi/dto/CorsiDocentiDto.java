package com.its.ipma.corsi.corsi.dto;

import com.its.ipma.corsi.corsi.dao.CorsiDao;
import com.its.ipma.corsi.corsi.dao.CorsiDocentiDao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CorsiDocentiDto {

	private Integer corsiDocentiId;
	private CorsiDao corso;
	private Integer docente;
	private Integer interno;

	public CorsiDocentiDao convertToDao() {

		CorsiDocentiDao dao = new CorsiDocentiDao();

		dao.setCorsiDocentiId(this.getCorsiDocentiId());
		dao.setCorso(this.getCorso());
		dao.setDocente(this.getDocente());
		dao.setInterno(this.getInterno());

		return dao;

	}

}
