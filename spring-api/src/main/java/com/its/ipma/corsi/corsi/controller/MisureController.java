package com.its.ipma.corsi.corsi.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.its.ipma.corsi.corsi.dto.BaseResponseDto;
import com.its.ipma.corsi.corsi.dto.MisureDto;
import com.its.ipma.corsi.corsi.services.interfaces.MisureInterface;

@RestController
@RequestMapping(value = "api/misure")
public class MisureController {
	private static final Logger logger = LoggerFactory.getLogger(MisureController.class);

	@Autowired
	MisureInterface misureService;

	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<MisureDto>> fetchAll() {

		logger.info("****** FETCH *******");

		BaseResponseDto<List<MisureDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<MisureDto> misure = misureService.fetchAll();

		if (misure.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(misure);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	
}
