package com.its.ipma.corsi.corsi.services.interfaces;

import java.util.List;

import com.its.ipma.corsi.corsi.dao.CorsiUtentiDao;
import com.its.ipma.corsi.corsi.dto.CorsiUtentiDto;

public interface CorsiUtentiInterface {
	CorsiUtentiDto readById(int id);

	List<CorsiUtentiDto> filter(CorsiUtentiDao filters);

	List<CorsiUtentiDto> fetchAll();
	
	List<CorsiUtentiDto> pageLayout(int pagina, int quantita);

	boolean update(CorsiUtentiDao dao);

	boolean deleteById(int id);

	boolean create(CorsiUtentiDao dao);
}
