package com.its.ipma.corsi.corsi.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.its.ipma.corsi.corsi.dto.BaseResponseDto;
import com.its.ipma.corsi.corsi.dto.TipiDto;
import com.its.ipma.corsi.corsi.services.interfaces.TipiInterface;

@RestController
@RequestMapping(value = "api/tipi")
public class TipiController {
	private static final Logger logger = LoggerFactory.getLogger(TipiController.class);

	@Autowired
	TipiInterface tipiService;

	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<TipiDto>> fetchAll() {

		logger.info("****** FETCH *******");

		BaseResponseDto<List<TipiDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<TipiDto> tipi = tipiService.fetchAll();

		if (tipi.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(tipi);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	
}
