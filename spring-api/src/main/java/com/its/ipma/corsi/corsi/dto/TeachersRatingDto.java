package com.its.ipma.corsi.corsi.dto;

import com.its.ipma.corsi.corsi.dao.CorsiDao;
import com.its.ipma.corsi.corsi.dao.TeachersDao;
import com.its.ipma.corsi.corsi.dao.TeachersRatingDao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeachersRatingDto {

	private Integer valutazioniDocentiId;
	private CorsiDao corso;
	private TeachersDao docente;
	private String criterio;
	private Integer insufficente;
	private Integer sufficente;
	private Integer buono;
	private Integer moltoBuono;
	private Integer nonApplicabile;
	private Double media;
	private Integer interno;

	public TeachersRatingDao convertToDao() {

		TeachersRatingDao dao = new TeachersRatingDao();

		dao.setValutazioniDocentiId(this.getValutazioniDocentiId());
		dao.setCorso(this.getCorso());
		dao.setDocente(this.getDocente());
		dao.setCriterio(this.getCriterio());
		dao.setInsufficente(this.getInsufficente());
		dao.setSufficente(this.getSufficente());
		dao.setBuono(this.getBuono());
		dao.setMoltoBuono(this.getMoltoBuono());
		dao.setNonApplicabile(this.getNonApplicabile());
		dao.setMedia(this.getMedia());
		dao.setInterno(this.getInterno());

		return dao;

	}

}
