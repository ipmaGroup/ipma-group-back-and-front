package com.its.ipma.corsi.corsi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.its.ipma.corsi.corsi.dao.CorsiUtentiDao;

public interface CorsiUtentiRepository extends JpaRepository<CorsiUtentiDao, Integer> {
}
