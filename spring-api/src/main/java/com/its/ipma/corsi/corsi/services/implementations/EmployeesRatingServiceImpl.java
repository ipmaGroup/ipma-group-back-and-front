package com.its.ipma.corsi.corsi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.its.ipma.corsi.corsi.dao.EmployeesRatingDao;
import com.its.ipma.corsi.corsi.dto.EmployeesRatingDto;
import com.its.ipma.corsi.corsi.repository.EmployeesRatingRepository;
import com.its.ipma.corsi.corsi.services.interfaces.EmployeesRatingInterface;

@Service
@Transactional
public class EmployeesRatingServiceImpl implements EmployeesRatingInterface {

	@Autowired
	EmployeesRatingRepository employeesRatingRepository;

	@Override
	public boolean create(EmployeesRatingDao dao) {

		try {
			employeesRatingRepository.save(dao);
		} catch (Exception e) {
			return false;
		}
		return true;

	}

	@Override
	public List<EmployeesRatingDto> fetchAll() {

		List<EmployeesRatingDto> list = new ArrayList<>();
		try {
			for (EmployeesRatingDao rating : employeesRatingRepository.findAll()) {
				list.add(rating.convertToDto());
			}
		} catch (Exception e) {
			return null;
		}
		return list;
	}

	@Override
	public boolean update(EmployeesRatingDao dao) {
		if (employeesRatingRepository.existsById(dao.getValutazioniUtentiId())) {
			try {
				employeesRatingRepository.save(dao);
			} catch (Exception e) {
				return false;
			}
			return true;
		}
		return false;

	}

	@Override
	public boolean deleteById(int id) {

		if (employeesRatingRepository.existsById(id)) {
			employeesRatingRepository.deleteById(id);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public EmployeesRatingDto readById(int id) {

		try {
			return employeesRatingRepository.getOne(id).convertToDto();
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public List<EmployeesRatingDto> filter(EmployeesRatingDao filters, int pagina, int quantita) {

		Pageable p = PageRequest.of(pagina - 1, quantita);

		ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher.matchingAll().withIgnoreCase()
				.withMatcher("valutazioniUtentiId", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("corso.tipo.descrizione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("corso.tipologia.descrizione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("corso.descrizione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("corso.luogo", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("dipendente.matricola", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("dipendente.societa.ragioneSociale", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("dipendente.cognome", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("dipendente.nome", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("dipendente.sesso", ExampleMatcher.GenericPropertyMatchers.contains());
		Example<EmployeesRatingDao> example = Example.of(filters, caseInsensitiveExampleMatcher);

		Page<EmployeesRatingDao> ratingDao = employeesRatingRepository.findAll(example, p);
		ArrayList<EmployeesRatingDto> ratingDto = new ArrayList<EmployeesRatingDto>();
		for (EmployeesRatingDao rating : ratingDao) {
			ratingDto.add(rating.convertToDto());
		}
		return ratingDto;
	}

	@Override
	public List<EmployeesRatingDto> pageLayout(int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina - 1, quantita);
		Page<EmployeesRatingDao> listaPagina = employeesRatingRepository.findAll(p);
		ArrayList<EmployeesRatingDto> employeesDto = new ArrayList<EmployeesRatingDto>();
		for (EmployeesRatingDao e : listaPagina) {
			employeesDto.add(e.convertToDto());
		}
		return employeesDto;
	}

}
