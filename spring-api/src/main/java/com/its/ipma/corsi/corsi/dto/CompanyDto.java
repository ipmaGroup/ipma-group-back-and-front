package com.its.ipma.corsi.corsi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompanyDto {

	private int societaId;
	private String ragioneSociale;
	private String indirizzo;
	private String localita;
	private String provincia;
	private String nazione;
	private String telefono;
	private String fax;
	private String partitaIva;

}
