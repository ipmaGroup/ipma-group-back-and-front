package com.its.ipma.corsi.corsi.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.its.ipma.corsi.corsi.dto.TeachersDto;

import lombok.Data;

@Entity
@Table(name = "docenti")
@Data
public class TeachersDao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "docente_id")
	private Integer docenteId;

	@Column(name = "titolo")
	private String titolo;

	@Column(name = "ragione_sociale")
	private String ragioneSociale;

	@Column(name = "indirizzo")
	private String indirizzo;

	@Column(name = "localita")
	private String localita;

	@Column(name = "provincia")
	private String provincia;

	@Column(name = "nazione")
	private String nazione;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "fax")
	private String fax;

	@Column(name = "partita_iva")
	private String partitaIva;

	@Column(name = "referente")
	private String referente;

	public TeachersDto convertToDto() {

		TeachersDto dto = new TeachersDto();

		dto.setDocenteId(this.getDocenteId());
		dto.setFax(this.getFax());
		dto.setIndirizzo(this.getIndirizzo());
		dto.setLocalita(this.getLocalita());
		dto.setNazione(this.getNazione());
		dto.setPartitaIva(this.getPartitaIva());
		dto.setProvincia(this.getProvincia());
		dto.setRagioneSociale(this.getRagioneSociale());
		dto.setReferente(this.getReferente());
		dto.setTelefono(this.getTelefono());
		dto.setTitolo(this.getTitolo());

		return dto;
	}

}