package com.its.ipma.corsi.corsi.dto;

import java.sql.Date;

import com.its.ipma.corsi.corsi.dao.CompanyDao;
import com.its.ipma.corsi.corsi.dao.EmployeesDao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeesDto {

	private Integer dipendenteId;
	private Integer matricola;
	private CompanyDao societa;
	private String cognome;
	private String nome;
	private String sesso;
	private Date dataNascita;
	private String luogoNascita;
	private String statoCivile;
	private String titoloStudio;
	private String conseguitoPresso;
	private Integer annoConseguimento;
	private String tipoDipendente;
	private String qualifica;
	private String livello;
	private Date dataAssunzione;
	private Integer responsabileRisorsa;
	private Integer responsabileArea;
	private Date dataFineRapporto;
	private Date dataScadenzaContratto;

	public EmployeesDao convertToDao() {

		EmployeesDao dao = new EmployeesDao();

		dao.setDipendenteId(this.getDipendenteId());
		dao.setMatricola(this.getMatricola());
		dao.setSocieta(this.getSocieta());
		dao.setCognome(this.getCognome());
		dao.setNome(this.getNome());
		dao.setSesso(this.getSesso());
		dao.setDataNascita(this.getDataNascita());
		dao.setLuogoNascita(this.getLuogoNascita());
		dao.setStatoCivile(this.getStatoCivile());
		dao.setTitoloStudio(this.getTitoloStudio());
		dao.setConseguitoPresso(this.getConseguitoPresso());
		dao.setAnnoConseguimento(this.getAnnoConseguimento());
		dao.setTipoDipendente(this.getTipoDipendente());
		dao.setQualifica(this.getQualifica());
		dao.setLivello(this.getLivello());
		dao.setDataAssunzione(this.getDataAssunzione());
		dao.setResponsabileRisorsa(this.getResponsabileRisorsa());
		dao.setResponsabileArea(this.getResponsabileArea());
		dao.setDataFineRapporto(this.getDataFineRapporto());
		dao.setDataScadenzaContratto(this.getDataScadenzaContratto());

		return dao;

	}

}
