package com.its.ipma.corsi.corsi.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.its.ipma.corsi.corsi.dto.EmployeesRatingDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "valutazioni_utenti")
@Data
public class EmployeesRatingDao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "valutazioni_utenti_id")
	private Integer valutazioniUtentiId;

	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "corso", referencedColumnName = "corso_id")
	private CorsiDao corso;

	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "dipendente", referencedColumnName = "dipendente_id")
	private EmployeesDao dipendente;

	@Column(name = "criterio")
	private String criterio;

	@Column(name = "totalmente_insufficente")
	private Integer totalmenteInsufficente;

	@Column(name = "insufficente")
	private Integer insufficente;

	@Column(name = "sufficente")
	private Integer sufficente;

	@Column(name = "discreto")
	private Integer discreto;

	@Column(name = "buono")
	private Integer buono;

	@Column(name = "ottimo")
	private Integer ottimo;

	@Column(name = "media")
	private Double media;

	public EmployeesRatingDto convertToDto() {

		EmployeesRatingDto dto = new EmployeesRatingDto();

		dto.setValutazioniUtentiId(this.getValutazioniUtentiId());
		dto.setCorso(this.getCorso());
		dto.setDipendente(this.getDipendente());
		dto.setCriterio(this.getCriterio());
		dto.setTotalmenteInsufficente(this.getTotalmenteInsufficente());
		dto.setInsufficente(this.getInsufficente());
		dto.setSufficente(this.getSufficente());
		dto.setDiscreto(this.getDiscreto());
		dto.setBuono(this.getBuono());
		dto.setOttimo(this.getOttimo());
		dto.setMedia(this.getMedia());

		return dto;
	}

}