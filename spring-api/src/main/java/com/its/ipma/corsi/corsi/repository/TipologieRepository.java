package com.its.ipma.corsi.corsi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.its.ipma.corsi.corsi.dao.TipologieDao;

public interface TipologieRepository extends JpaRepository<TipologieDao, Integer> {
}
