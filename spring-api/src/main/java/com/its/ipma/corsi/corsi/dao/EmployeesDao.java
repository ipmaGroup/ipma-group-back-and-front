package com.its.ipma.corsi.corsi.dao;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.its.ipma.corsi.corsi.dto.EmployeesDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "dipendenti")
@Data
public class EmployeesDao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "dipendente_id")
	private Integer dipendenteId;

	@Column(name = "matricola")
	private Integer matricola;

	// Chiave esterna di società
	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "societa", referencedColumnName = "societa_id")
	@JsonBackReference
	private CompanyDao societa;

	@Column(name = "cognome")
	private String cognome;

	@Column(name = "nome")
	private String nome;

	@Column(name = "sesso")
	private String sesso;

	@Column(name = "data_nascita")
	private Date dataNascita;

	@Column(name = "luogo_nascita")
	private String luogoNascita;

	@Column(name = "stato_civile")
	private String statoCivile;

	@Column(name = "titolo_studio")
	private String titoloStudio;

	@Column(name = "conseguito_presso")
	private String conseguitoPresso;

	@Column(name = "anno_conseguimento")
	private Integer annoConseguimento;

	@Column(name = "tipo_dipendente")
	private String tipoDipendente;

	@Column(name = "qualifica")
	private String qualifica;

	@Column(name = "livello")
	private String livello;

	@Column(name = "data_assunzione")
	private Date dataAssunzione;

	@Column(name = "responsabile_risorsa")
	private Integer responsabileRisorsa;

	@Column(name = "responsabile_area")
	private Integer responsabileArea;

	@Column(name = "data_fine_rapporto")
	private Date dataFineRapporto;

	@Column(name = "data_scadenza_contratto")
	private Date dataScadenzaContratto;

	public EmployeesDto convertToDto() {

		EmployeesDto dto = new EmployeesDto();

		dto.setDipendenteId(this.getDipendenteId());
		dto.setMatricola(this.getMatricola());
		dto.setSocieta(this.getSocieta());
		dto.setCognome(this.getCognome());
		dto.setNome(this.getNome());
		dto.setSesso(this.getSesso());
		dto.setDataNascita(this.getDataNascita());
		dto.setLuogoNascita(this.getLuogoNascita());
		dto.setStatoCivile(this.getStatoCivile());
		dto.setTitoloStudio(this.getTitoloStudio());
		dto.setConseguitoPresso(this.getConseguitoPresso());
		dto.setAnnoConseguimento(this.getAnnoConseguimento());
		dto.setTipoDipendente(this.getTipoDipendente());
		dto.setQualifica(this.getQualifica());
		dto.setLivello(this.getLivello());
		dto.setDataAssunzione(this.getDataAssunzione());
		dto.setResponsabileRisorsa(this.getResponsabileRisorsa());
		dto.setResponsabileArea(this.getResponsabileArea());
		dto.setDataFineRapporto(this.getDataFineRapporto());
		dto.setDataScadenzaContratto(this.getDataScadenzaContratto());

		return dto;
	}

}