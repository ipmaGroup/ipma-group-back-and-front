package com.its.ipma.corsi.corsi.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.its.ipma.corsi.corsi.dao.CorsiDocentiDao;
import com.its.ipma.corsi.corsi.dto.BaseResponseDto;
import com.its.ipma.corsi.corsi.dto.CorsiDocentiDto;
import com.its.ipma.corsi.corsi.services.interfaces.CorsiDocentiInterface;

@RestController
@RequestMapping(value = "api/corsi-docenti")
public class CorsiDocentiController {
	private static final Logger logger = LoggerFactory.getLogger(CorsiDocentiController.class);

	@Autowired
	CorsiDocentiInterface corsidocentiService;

	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<CorsiDocentiDto>> fetchAll() {

		logger.info("****** FETCH *******");

		BaseResponseDto<List<CorsiDocentiDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<CorsiDocentiDto> corsidocenti = corsidocentiService.fetchAll();

		if (corsidocenti.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(corsidocenti);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@PostMapping(produces = "application/json", value = "filter")
	public BaseResponseDto<List<CorsiDocentiDto>> filter(@RequestBody CorsiDocentiDao filters) {

		logger.info("****** FILTER *******");

		BaseResponseDto<List<CorsiDocentiDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<CorsiDocentiDto> corsidocenti = corsidocentiService.filter(filters);

		if (corsidocenti.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(corsidocenti);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@PostMapping(produces = "application/json")
	public BaseResponseDto<CorsiDocentiDto> createCorsiDocenti(@RequestBody CorsiDocentiDao corsidocenti) {

		logger.info("****** CREATE *******");
		BaseResponseDto<CorsiDocentiDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		response.setResponse(corsidocenti.convertToDto());
		
			if(corsidocenti.getCorso()!=null && corsidocenti.getDocente()!=null) {
				corsidocentiService.create(corsidocenti);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Corsi docenti added correctly");
			}
			else if(corsidocenti.getCorso()==null && corsidocenti.getDocente()!=null) {
				response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
				response.setMessage("Corso can not be null");
			}
			else if(corsidocenti.getCorso()!=null && corsidocenti.getDocente()==null) {
				response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
				response.setMessage("Docente can not be null");
			}
		   else {
			    response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			    response.setMessage("Corsi Docenti not added");
		}

		return response;
	}

	@PatchMapping(produces = "application/json")
	public BaseResponseDto<CorsiDocentiDto> updateCorsiDocenti(@RequestBody CorsiDocentiDao corsidocenti) {

		logger.info("****** UPDATE *******");

		BaseResponseDto<CorsiDocentiDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		response.setResponse(corsidocenti.convertToDto());

		if(corsidocenti.getCorso()==null || corsidocenti.getDocente()==null) {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setMessage("Corsi e dipendenti can not be null");
		}	
		else if(corsidocenti.getCorso()!=null && corsidocenti.getDocente()!=null) {
			corsidocentiService.update(corsidocenti);
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("Corsi Utenti changed correctly");
		}			
	   else {
		    response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		    response.setMessage("Teacher not changed");
	  }

		return response;
	}

	@DeleteMapping(produces = "application/json", path = "/{id}")
	public BaseResponseDto<CorsiDocentiDto> deleteById(@PathVariable int id) {

		logger.info("****** DELETE *******");

		BaseResponseDto<CorsiDocentiDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		corsidocentiService.deleteById(id);
		response.setResponse(id);
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("Corsi docenti deleted correctly");

		return response;
	}
	
	@GetMapping(value = "/{pagina}...{quantita}", produces = "application/json")
	public BaseResponseDto<List<CorsiDocentiDto>> fetchPagina(@PathVariable("pagina") int pagina, @PathVariable("quantita") int quantita) {

		logger.info("****** FETCH IMPAGINAZIONE*******");

		BaseResponseDto<List<CorsiDocentiDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<CorsiDocentiDto> corsidocenti;

		corsidocenti = corsidocentiService.pageLayout(pagina, quantita);
		if (corsidocenti.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(corsidocenti);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

}
