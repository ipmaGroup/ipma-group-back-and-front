package com.its.ipma.corsi.corsi.services.interfaces;

import java.util.List;
import com.its.ipma.corsi.corsi.dto.MisureDto;

public interface MisureInterface {
	MisureDto readById(int id);

	List<MisureDto> fetchAll();

}
