package com.its.ipma.corsi.corsi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.its.ipma.corsi.corsi.dao.TipologieDao;
import com.its.ipma.corsi.corsi.dto.TipologieDto;
import com.its.ipma.corsi.corsi.repository.TipologieRepository;
import com.its.ipma.corsi.corsi.services.interfaces.TipologieInterface;

@Service
@Transactional
public class TipologieServiceImpl implements TipologieInterface {

	@Autowired
	TipologieRepository tipologieRepository;

	@Override
	public List<TipologieDto> fetchAll() {

		List<TipologieDto> list = new ArrayList<>();
		try {
			for (TipologieDao tipologie : tipologieRepository.findAll()) {
				list.add(tipologie.convertToDto());
			}
		} catch (Exception e) {
			return null;
		}
		return list;
	}

	@Override
	public TipologieDto readById(int id) {

		try {
			return tipologieRepository.getOne(id).convertToDto();
		} catch (Exception e) {
			return null;
		}

	}

}
