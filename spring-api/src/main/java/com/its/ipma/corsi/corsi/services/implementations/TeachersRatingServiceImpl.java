package com.its.ipma.corsi.corsi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.its.ipma.corsi.corsi.dao.TeachersRatingDao;
import com.its.ipma.corsi.corsi.dto.TeachersRatingDto;
import com.its.ipma.corsi.corsi.repository.TeachersRatingRepository;
import com.its.ipma.corsi.corsi.services.interfaces.TeachersRatingInterface;

@Service
@Transactional
public class TeachersRatingServiceImpl implements TeachersRatingInterface {

	@Autowired
	TeachersRatingRepository teachersRatingRepository;

	@Override
	public boolean create(TeachersRatingDao dao) {

		try {
			teachersRatingRepository.save(dao);
		} catch (Exception e) {
			return false;
		}
		return true;

	}

	@Override
	public List<TeachersRatingDto> fetchAll() {

		List<TeachersRatingDto> list = new ArrayList<>();
		try {
			for (TeachersRatingDao rating : teachersRatingRepository.findAll()) {
				list.add(rating.convertToDto());
			}
		} catch (Exception e) {
			return null;
		}
		return list;
	}

	@Override
	public boolean update(TeachersRatingDao dao) {
		if (teachersRatingRepository.existsById(dao.getValutazioniDocentiId())) {
			try {
				teachersRatingRepository.save(dao);
			} catch (Exception e) {
				return false;
			}
			return true;
		}
		return false;

	}

	@Override
	public boolean deleteById(int id) {

		if (teachersRatingRepository.existsById(id)) {
			teachersRatingRepository.deleteById(id);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public TeachersRatingDto readById(int id) {

		try {
			return teachersRatingRepository.getOne(id).convertToDto();
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public List<TeachersRatingDto> filter(TeachersRatingDao filters, int pagina, int quantita) {

		Pageable p = PageRequest.of(pagina - 1, quantita);

		ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher.matchingAll().withIgnoreCase()
				.withMatcher("valutazioniUtentiId", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("corso.tipo.descrizione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("corso.tipologia.descrizione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("corso.descrizione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("corso.luogo", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("docente.titolo", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("docente.ragioneSociale", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("interno", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("docente.partitaIva", ExampleMatcher.GenericPropertyMatchers.contains());
		Example<TeachersRatingDao> example = Example.of(filters, caseInsensitiveExampleMatcher);

		Page<TeachersRatingDao> corsiDao = teachersRatingRepository.findAll(example, p);
		ArrayList<TeachersRatingDto> corsiDto = new ArrayList<TeachersRatingDto>();
		for (TeachersRatingDao rating : corsiDao) {
			corsiDto.add(rating.convertToDto());
		}
		return corsiDto;
	}

	@Override
	public List<TeachersRatingDto> pageLayout(int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina - 1, quantita);
		Page<TeachersRatingDao> listaPagina = teachersRatingRepository.findAll(p);
		ArrayList<TeachersRatingDto> employeesDto = new ArrayList<TeachersRatingDto>();
		for (TeachersRatingDao e : listaPagina) {
			employeesDto.add(e.convertToDto());
		}
		return employeesDto;
	}

}
