package com.its.ipma.corsi.corsi.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.its.ipma.corsi.corsi.dto.TipiDto;

import lombok.Data;

@Entity
@Table(name = "tipi")
@Data
public class TipiDao {

	@Id
	@Column(name = "tipo_id")
	private Integer tipoId;

	@Column(name = "descrizione")
	private String descrizione;

	public TipiDto convertToDto() {

		TipiDto dto = new TipiDto();

		dto.setTipoId(this.getTipoId());
		dto.setDescrizione(this.getDescrizione());

		return dto;
	}
}
