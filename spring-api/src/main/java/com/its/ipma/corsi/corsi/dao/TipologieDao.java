package com.its.ipma.corsi.corsi.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.its.ipma.corsi.corsi.dto.TipologieDto;

import lombok.Data;

@Entity
@Table(name = "tipologie")
@Data
public class TipologieDao {

	@Id
	@Column(name = "tipologia_id")
	private Integer tipologiaId;

	@Column(name = "descrizione")
	private String descrizione;

	public TipologieDto convertToDto() {

		TipologieDto dto = new TipologieDto();

		dto.setTipologiaId(this.getTipologiaId());
		dto.setDescrizione(this.getDescrizione());

		return dto;
	}
}
