package com.its.ipma.corsi.corsi.services.interfaces;

import java.util.List;

import com.its.ipma.corsi.corsi.dao.TeachersRatingDao;
import com.its.ipma.corsi.corsi.dto.TeachersRatingDto;

public interface TeachersRatingInterface {
	TeachersRatingDto readById(int id);

	List<TeachersRatingDto> filter(TeachersRatingDao filters, int pagina, int quantita);

	List<TeachersRatingDto> fetchAll();

	boolean update(TeachersRatingDao dao);

	boolean deleteById(int id);

	boolean create(TeachersRatingDao dao);

	List<TeachersRatingDto> pageLayout(int pagina, int quantita);
}
