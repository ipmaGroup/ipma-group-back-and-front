package com.its.ipma.corsi.corsi.dto;

import java.util.Date;

import com.its.ipma.corsi.corsi.dao.CorsiDao;
import com.its.ipma.corsi.corsi.dao.MisureDao;
import com.its.ipma.corsi.corsi.dao.TipiDao;
import com.its.ipma.corsi.corsi.dao.TipologieDao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CorsiDto {

	private Integer corsoId;
	private TipiDao tipo;
	private TipologieDao tipologia;
	private String descrizione;
	private String edizione;
	private Integer previsto;
	private Integer erogato;
	private Integer durata;
	private MisureDao misura;
	private String note;
	private String luogo;
	private String ente;
	private Date dataErogazione;
	private Date dataChiusura;
	private Date dataCensimento;
	private Integer interno;
	private Integer tipoId;
	private Integer tipologiaId;
	private Integer misuraId;

	public CorsiDao convertToDao() {

		CorsiDao dao = new CorsiDao();

		dao.setCorsoId(this.getCorsoId());
		dao.setTipo(this.getTipo());
		dao.setTipologia(this.getTipologia());
		dao.setDescrizione(this.getDescrizione());
		dao.setEdizione(this.getEdizione());
		dao.setPrevisto(this.getPrevisto());
		dao.setErogato(this.getErogato());
		dao.setDurata(this.getDurata());
		dao.setMisura(this.getMisura());
		dao.setNote(this.getNote());
		dao.setLuogo(this.getLuogo());
		dao.setEnte(this.getEnte());
		dao.setDataErogazione(this.getDataErogazione());
		dao.setDataChiusura(this.getDataChiusura());
		dao.setDataCensimento(this.getDataCensimento());
		dao.setInterno(this.getInterno());
		dao.setTipoId(this.getTipoId());
		dao.setTipologiaId(this.getTipologiaId());
		dao.setMisuraId(this.getMisuraId());

		return dao;

	}

}
