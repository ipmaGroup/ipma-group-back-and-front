package com.its.ipma.corsi.corsi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.its.ipma.corsi.corsi.dao.CorsiUtentiDao;
import com.its.ipma.corsi.corsi.dto.CorsiUtentiDto;
import com.its.ipma.corsi.corsi.repository.CorsiUtentiRepository;
import com.its.ipma.corsi.corsi.services.interfaces.CorsiUtentiInterface;

@Service
@Transactional
public class CorsiUtentiServiceImpl implements CorsiUtentiInterface {

	@Autowired
	CorsiUtentiRepository corsiutentiRepository;

	@Override
	public boolean create(CorsiUtentiDao dao) {

		try {
			corsiutentiRepository.save(dao);
		} catch (Exception e) {
			return false;
		}
		return true;

	}

	@Override
	public List<CorsiUtentiDto> fetchAll() {

		List<CorsiUtentiDto> list = new ArrayList<>();
		try {
			for (CorsiUtentiDao corsiutenti : corsiutentiRepository.findAll()) {
				list.add(corsiutenti.convertToDto());
			}
		} catch (Exception e) {
			return null;
		}
		return list;
	}

	@Override
	public boolean update(CorsiUtentiDao dao) {
		if (corsiutentiRepository.existsById(dao.getCorsiUtentiId())) {
			try {
				corsiutentiRepository.save(dao);
			} catch (Exception e) {
				return false;
			}
			return true;
		}
		return false;

	}

	@Override
	public boolean deleteById(int id) {

		if (corsiutentiRepository.existsById(id)) {
			corsiutentiRepository.deleteById(id);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public CorsiUtentiDto readById(int id) {

		try {
			return corsiutentiRepository.getOne(id).convertToDto();
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public List<CorsiUtentiDto> filter(CorsiUtentiDao filters) {

		ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher.matchingAll().withIgnoreCase()
				.withMatcher("corsiUtentiId", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("corso", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("dipendente", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("durata", ExampleMatcher.GenericPropertyMatchers.contains());

		Example<CorsiUtentiDao> example = Example.of(filters, caseInsensitiveExampleMatcher);

		List<CorsiUtentiDao> corsiutentiDao = corsiutentiRepository.findAll(example);
		ArrayList<CorsiUtentiDto> corsiutentiDto = new ArrayList<CorsiUtentiDto>();
		for (CorsiUtentiDao corsiutenti : corsiutentiDao) {
			corsiutentiDto.add(corsiutenti.convertToDto());
		}
		return corsiutentiDto;
	}

	@Override
	public List<CorsiUtentiDto> pageLayout(int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina-1, quantita);
		Page<CorsiUtentiDao> listaPagina = corsiutentiRepository.findAll(p);
		ArrayList<CorsiUtentiDto> corsiutentiDto = new ArrayList<CorsiUtentiDto>();
		for(CorsiUtentiDao t : listaPagina) {
			corsiutentiDto.add(t.convertToDto());
		}
		return corsiutentiDto;
	}

}
