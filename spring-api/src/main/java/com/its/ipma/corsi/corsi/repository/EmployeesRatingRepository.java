package com.its.ipma.corsi.corsi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.its.ipma.corsi.corsi.dao.EmployeesRatingDao;

public interface EmployeesRatingRepository extends JpaRepository<EmployeesRatingDao, Integer> {
}
