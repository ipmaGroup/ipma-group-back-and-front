package com.its.ipma.corsi.corsi.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.its.ipma.corsi.corsi.dao.TeachersRatingDao;
import com.its.ipma.corsi.corsi.dto.BaseResponseDto;
import com.its.ipma.corsi.corsi.dto.TeachersRatingDto;
import com.its.ipma.corsi.corsi.services.interfaces.TeachersRatingInterface;

@RestController
@RequestMapping(value = "api/rating/teachers")
public class TeachersRatingController {
	private static final Logger logger = LoggerFactory.getLogger(TeachersRatingController.class);

	@Autowired
	TeachersRatingInterface teachersRatingService;

	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<TeachersRatingDto>> fetchAll() {

		logger.info("****** FETCH *******");

		BaseResponseDto<List<TeachersRatingDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<TeachersRatingDto> valutazioni = teachersRatingService.fetchAll();

		if (valutazioni.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(valutazioni);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@PostMapping(value = "{pagina}...{quantita}", produces = "application/json")
	public BaseResponseDto<List<TeachersRatingDto>> filter(@PathVariable("pagina") int pagina,
			@PathVariable("quantita") int quantita, @RequestBody TeachersRatingDao filters) {

		logger.info("****** FILTER *******");

		BaseResponseDto<List<TeachersRatingDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<TeachersRatingDto> valutazioni = teachersRatingService.filter(filters, pagina, quantita);

		if (valutazioni.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(valutazioni);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@GetMapping(value = "{pagina}...{quantita}", produces = "application/json")
	public BaseResponseDto<List<TeachersRatingDto>> fetchPagina(@PathVariable("pagina") int pagina,
			@PathVariable("quantita") int quantita) {

		logger.info("****** FETCH IMPAGINAZIONE*******");

		BaseResponseDto<List<TeachersRatingDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<TeachersRatingDto> valutazioni = teachersRatingService.pageLayout(pagina, quantita);
		if (valutazioni.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(valutazioni);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@PostMapping(produces = "application/json")
	public BaseResponseDto<TeachersRatingDto> createRating(@RequestBody TeachersRatingDao valutazione) {

		logger.info("****** CREATE *******");
		BaseResponseDto<TeachersRatingDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		response.setResponse(valutazione.convertToDto());

		if (teachersRatingService.create(valutazione)) {
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Rating added correctly");
		} else {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setMessage("Rating not added");
		}

		return response;
	}

	@PatchMapping(produces = "application/json")
	public BaseResponseDto<TeachersRatingDto> updateRating(@RequestBody TeachersRatingDao valutazione) {

		logger.info("****** UPDATE *******");

		BaseResponseDto<TeachersRatingDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		response.setResponse(valutazione.convertToDto());

		if (teachersRatingService.update(valutazione)) {
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Rating changed correctly");
		} else {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setMessage("Rating not changed");
		}

		return response;
	}

	@DeleteMapping(produces = "application/json", path = "/{id}")
	public BaseResponseDto<TeachersRatingDto> deleteById(@PathVariable int id) {

		logger.info("****** DELETE *******");

		BaseResponseDto<TeachersRatingDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		teachersRatingService.deleteById(id);
		response.setResponse(id);
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("Rating deleted correctly");

		return response;
	}

}
