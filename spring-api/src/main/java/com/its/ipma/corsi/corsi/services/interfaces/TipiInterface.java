package com.its.ipma.corsi.corsi.services.interfaces;

import java.util.List;
import com.its.ipma.corsi.corsi.dto.TipiDto;

public interface TipiInterface {
	TipiDto readById(int id);

	List<TipiDto> fetchAll();

}
