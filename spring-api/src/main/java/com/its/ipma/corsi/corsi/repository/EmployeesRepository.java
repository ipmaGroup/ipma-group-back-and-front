package com.its.ipma.corsi.corsi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.its.ipma.corsi.corsi.dao.EmployeesDao;

public interface EmployeesRepository extends JpaRepository<EmployeesDao, Integer> {
}
