package com.its.ipma.corsi.corsi.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.its.ipma.corsi.corsi.dao.CorsiUtentiDao;
import com.its.ipma.corsi.corsi.dto.BaseResponseDto;
import com.its.ipma.corsi.corsi.dto.CorsiUtentiDto;
import com.its.ipma.corsi.corsi.services.interfaces.CorsiUtentiInterface;

@RestController
@RequestMapping(value = "api/corsi-utenti")
public class CorsiUtentiController {
	private static final Logger logger = LoggerFactory.getLogger(CorsiUtentiController.class);

	@Autowired
	CorsiUtentiInterface corsiutentiService;

	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<CorsiUtentiDto>> fetchAll() {

		logger.info("****** FETCH *******");

		BaseResponseDto<List<CorsiUtentiDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<CorsiUtentiDto> corsiutenti = corsiutentiService.fetchAll();

		if (corsiutenti.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(corsiutenti);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@PostMapping(produces = "application/json", value = "filter")
	public BaseResponseDto<List<CorsiUtentiDto>> filter(@RequestBody CorsiUtentiDao filters) {

		logger.info("****** FILTER *******");

		BaseResponseDto<List<CorsiUtentiDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<CorsiUtentiDto> corsiutenti = corsiutentiService.filter(filters);

		if (corsiutenti.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(corsiutenti);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@PostMapping(produces = "application/json")
	public BaseResponseDto<CorsiUtentiDto> createCorsiUtenti(@RequestBody CorsiUtentiDao corsiutenti) {

		logger.info("****** CREATE *******");
		BaseResponseDto<CorsiUtentiDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		response.setResponse(corsiutenti.convertToDto());

			if(corsiutenti.getCorso()!=null && corsiutenti.getDipendente()!=null) {
				corsiutentiService.create(corsiutenti);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Corsi utenti added correctly");
			}
			else if(corsiutenti.getCorso()==null && corsiutenti.getDipendente()!=null) {
				response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
				response.setMessage("Corso can not be null");
			}
			else if(corsiutenti.getCorso()!=null && corsiutenti.getDipendente()==null) {
				response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
				response.setMessage("Dipendente can not be null");
			}
		   else {
			    response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			    response.setMessage("Corsi Utenti not added");
		}

		return response;
	}

	@PatchMapping(produces = "application/json")
	public BaseResponseDto<CorsiUtentiDto> updateCorsiUtenti(@RequestBody CorsiUtentiDao corsiutenti) {

		logger.info("****** UPDATE *******");

		BaseResponseDto<CorsiUtentiDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		response.setResponse(corsiutenti.convertToDto());

		if(corsiutenti.getCorso()==null || corsiutenti.getDipendente()==null) {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setMessage("Corsi e dipendenti can not be null");
		}	
		else if(corsiutenti.getCorso()!=null && corsiutenti.getDipendente()!=null) {
			corsiutentiService.update(corsiutenti);
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("Corsi Utenti changed correctly");
		}			
	   else {
		    response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		    response.setMessage("Teacher not changed");
	  }

		return response;
	}

	@DeleteMapping(produces = "application/json", path = "/{id}")
	public BaseResponseDto<CorsiUtentiDto> deleteById(@PathVariable int id) {

		logger.info("****** DELETE *******");

		BaseResponseDto<CorsiUtentiDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		corsiutentiService.deleteById(id);
		response.setResponse(id);
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("Corsi utenti deleted correctly");

		return response;
	}
	
	@GetMapping(value = "/{pagina}...{quantita}", produces = "application/json")
	public BaseResponseDto<List<CorsiUtentiDto>> fetchPagina(@PathVariable("pagina") int pagina, @PathVariable("quantita") int quantita) {

		logger.info("****** FETCH IMPAGINAZIONE*******");

		BaseResponseDto<List<CorsiUtentiDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<CorsiUtentiDto> corsiutenti;

		corsiutenti = corsiutentiService.pageLayout(pagina, quantita);
		if (corsiutenti.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(corsiutenti);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

}
