package com.its.ipma.corsi.corsi.dto;

import com.its.ipma.corsi.corsi.dao.MisureDao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MisureDto {

	private Integer misuraId;
	private String descrizione;

	public MisureDao convertToDao() {

		MisureDao dao = new MisureDao();

		dao.setMisuraId(this.getMisuraId());
		dao.setDescrizione(this.getDescrizione());

		return dao;

	}

}
