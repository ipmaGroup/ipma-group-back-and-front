package com.its.ipma.corsi.corsi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.its.ipma.corsi.corsi.dao.CorsiDocentiDao;
import com.its.ipma.corsi.corsi.dto.CorsiDocentiDto;
import com.its.ipma.corsi.corsi.repository.CorsiDocentiRepository;
import com.its.ipma.corsi.corsi.services.interfaces.CorsiDocentiInterface;

@Service
@Transactional
public class CorsiDocentiServiceImpl implements CorsiDocentiInterface {

	@Autowired
	CorsiDocentiRepository corsidocentiRepository;

	@Override
	public boolean create(CorsiDocentiDao dao) {

		try {
			corsidocentiRepository.save(dao);
		} catch (Exception e) {
			return false;
		}
		return true;

	}

	@Override
	public List<CorsiDocentiDto> fetchAll() {

		List<CorsiDocentiDto> list = new ArrayList<>();
		try {
			for (CorsiDocentiDao corsidocenti : corsidocentiRepository.findAll()) {
				list.add(corsidocenti.convertToDto());
			}
		} catch (Exception e) {
			return null;
		}
		return list;
	}

	@Override
	public boolean update(CorsiDocentiDao dao) {
		if (corsidocentiRepository.existsById(dao.getCorsiDocentiId())) {
			try {
				corsidocentiRepository.save(dao);
			} catch (Exception e) {
				return false;
			}
			return true;
		}
		return false;

	}

	@Override
	public boolean deleteById(int id) {

		if (corsidocentiRepository.existsById(id)) {
			corsidocentiRepository.deleteById(id);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public CorsiDocentiDto readById(int id) {

		try {
			return corsidocentiRepository.getOne(id).convertToDto();
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public List<CorsiDocentiDto> filter(CorsiDocentiDao filters) {

		ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher.matchingAll().withIgnoreCase()
				.withMatcher("corsiUtentiId", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("corso", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("docente", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("interno", ExampleMatcher.GenericPropertyMatchers.contains());

		Example<CorsiDocentiDao> example = Example.of(filters, caseInsensitiveExampleMatcher);

		List<CorsiDocentiDao> corsidocentiDao = corsidocentiRepository.findAll(example);
		ArrayList<CorsiDocentiDto> corsidocentiDto = new ArrayList<CorsiDocentiDto>();
		for (CorsiDocentiDao corsidocenti : corsidocentiDao) {
			corsidocentiDto.add(corsidocenti.convertToDto());
		}
		return corsidocentiDto;
	}

	@Override
	public List<CorsiDocentiDto> pageLayout(int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina-1, quantita);
		Page<CorsiDocentiDao> listaPagina = corsidocentiRepository.findAll(p);
		ArrayList<CorsiDocentiDto> corsidocentiDto = new ArrayList<CorsiDocentiDto>();
		for(CorsiDocentiDao t : listaPagina) {
			corsidocentiDto.add(t.convertToDto());
		}
		return corsidocentiDto;
	}

}
