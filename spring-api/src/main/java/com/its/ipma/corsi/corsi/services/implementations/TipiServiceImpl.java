package com.its.ipma.corsi.corsi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.its.ipma.corsi.corsi.dao.TipiDao;
import com.its.ipma.corsi.corsi.dto.TipiDto;
import com.its.ipma.corsi.corsi.repository.TipiRepository;
import com.its.ipma.corsi.corsi.services.interfaces.TipiInterface;

@Service
@Transactional
public class TipiServiceImpl implements TipiInterface {

	@Autowired
	TipiRepository tipiRepository;

	@Override
	public List<TipiDto> fetchAll() {

		List<TipiDto> list = new ArrayList<>();
		try {
			for (TipiDao tipi : tipiRepository.findAll()) {
				list.add(tipi.convertToDto());
			}
		} catch (Exception e) {
			return null;
		}
		return list;
	}

	@Override
	public TipiDto readById(int id) {

		try {
			return tipiRepository.getOne(id).convertToDto();
		} catch (Exception e) {
			return null;
		}

	}

}
