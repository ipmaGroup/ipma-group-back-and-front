package com.its.ipma.corsi.corsi.dto;

import com.its.ipma.corsi.corsi.dao.CorsiDao;
import com.its.ipma.corsi.corsi.dao.EmployeesDao;
import com.its.ipma.corsi.corsi.dao.EmployeesRatingDao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeesRatingDto {

	private Integer valutazioniUtentiId;
	private CorsiDao corso;
	private EmployeesDao dipendente;
	private String criterio;
	private Integer totalmenteInsufficente;
	private Integer insufficente;
	private Integer sufficente;
	private Integer discreto;
	private Integer buono;
	private Integer ottimo;
	private Double media;

	public EmployeesRatingDao convertToDao() {

		EmployeesRatingDao dao = new EmployeesRatingDao();

		dao.setValutazioniUtentiId(this.getValutazioniUtentiId());
		dao.setCorso(this.getCorso());
		dao.setDipendente(this.getDipendente());
		dao.setCriterio(this.getCriterio());
		dao.setTotalmenteInsufficente(this.getTotalmenteInsufficente());
		dao.setInsufficente(this.getInsufficente());
		dao.setSufficente(this.getSufficente());
		dao.setDiscreto(this.getDiscreto());
		dao.setBuono(this.getBuono());
		dao.setOttimo(this.getOttimo());
		dao.setMedia(this.getMedia());

		return dao;

	}

}
