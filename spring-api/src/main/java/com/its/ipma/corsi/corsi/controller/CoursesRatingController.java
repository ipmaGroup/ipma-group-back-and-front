package com.its.ipma.corsi.corsi.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.its.ipma.corsi.corsi.dao.CoursesRatingDao;
import com.its.ipma.corsi.corsi.dto.BaseResponseDto;
import com.its.ipma.corsi.corsi.dto.CoursesRatingDto;
import com.its.ipma.corsi.corsi.services.interfaces.CoursesRatingInterface;

@RestController
@RequestMapping(value = "api/rating/courses")
public class CoursesRatingController {
	private static final Logger logger = LoggerFactory.getLogger(CoursesRatingController.class);

	@Autowired
	CoursesRatingInterface coursesRatingService;

	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<CoursesRatingDto>> fetchAll() {

		logger.info("****** FETCH *******");

		BaseResponseDto<List<CoursesRatingDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<CoursesRatingDto> valutazioni = coursesRatingService.fetchAll();

		if (valutazioni.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(valutazioni);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@PostMapping(value = "{pagina}...{quantita}", produces = "application/json")
	public BaseResponseDto<List<CoursesRatingDto>> filter(@PathVariable("pagina") int pagina,
			@PathVariable("quantita") int quantita, @RequestBody CoursesRatingDao filters) {

		logger.info("****** FILTER *******");

		BaseResponseDto<List<CoursesRatingDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<CoursesRatingDto> valutazioni = coursesRatingService.filter(filters, pagina, quantita);

		if (valutazioni.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(valutazioni);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@GetMapping(value = "{pagina}...{quantita}", produces = "application/json")
	public BaseResponseDto<List<CoursesRatingDto>> fetchPagina(@PathVariable("pagina") int pagina,
			@PathVariable("quantita") int quantita) {

		logger.info("****** FETCH IMPAGINAZIONE*******");

		BaseResponseDto<List<CoursesRatingDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<CoursesRatingDto> valutazioni = coursesRatingService.pageLayout(pagina, quantita);
		if (valutazioni.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(valutazioni);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@PostMapping(produces = "application/json")
	public BaseResponseDto<CoursesRatingDto> createRating(@RequestBody CoursesRatingDao valutazione) {

		logger.info("****** CREATE *******");
		BaseResponseDto<CoursesRatingDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		System.err.println(valutazione);

		response.setResponse(valutazione.convertToDto());

		if (coursesRatingService.create(valutazione)) {
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Rating added correctly");
		} else {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setMessage("Rating not added");
		}

		return response;
	}

	@PatchMapping(produces = "application/json")
	public BaseResponseDto<CoursesRatingDto> updateRating(@RequestBody CoursesRatingDao valutazione) {

		logger.info("****** UPDATE *******");

		BaseResponseDto<CoursesRatingDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		response.setResponse(valutazione.convertToDto());

		if (coursesRatingService.update(valutazione)) {
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Rating changed correctly");
		} else {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setMessage("Rating not changed");
		}

		return response;
	}

	@DeleteMapping(produces = "application/json", path = "/{id}")
	public BaseResponseDto<CoursesRatingDto> deleteById(@PathVariable int id) {

		logger.info("****** DELETE *******");

		BaseResponseDto<CoursesRatingDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		coursesRatingService.deleteById(id);
		response.setResponse(id);
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("Rating deleted correctly");

		return response;
	}

}
