package com.its.ipma.corsi.corsi.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.its.ipma.corsi.corsi.dto.BaseResponseDto;
import com.its.ipma.corsi.corsi.dto.TipologieDto;
import com.its.ipma.corsi.corsi.services.interfaces.TipologieInterface;

@RestController
@RequestMapping(value = "api/tipologie")
public class TipologieController {
	private static final Logger logger = LoggerFactory.getLogger(TipologieController.class);

	@Autowired
	TipologieInterface tipologieService;

	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<TipologieDto>> fetchAll() {

		logger.info("****** FETCH *******");

		BaseResponseDto<List<TipologieDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<TipologieDto> tipologie = tipologieService.fetchAll();

		if (tipologie.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(tipologie);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	
}
