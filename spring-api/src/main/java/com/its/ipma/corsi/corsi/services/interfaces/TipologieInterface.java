package com.its.ipma.corsi.corsi.services.interfaces;

import java.util.List;
import com.its.ipma.corsi.corsi.dto.TipologieDto;

public interface TipologieInterface {
	TipologieDto readById(int id);

	List<TipologieDto> fetchAll();

}
