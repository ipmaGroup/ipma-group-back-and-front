package com.its.ipma.corsi.corsi.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.its.ipma.corsi.corsi.dto.CorsiDocentiDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "corsi_docenti")
@Data
public class CorsiDocentiDao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "corsi_docenti_id")
	private Integer corsiDocentiId;

	// Chiave esterna di corso
	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "corso", referencedColumnName = "corso_id")
	@JsonBackReference(value = "corso")
	private CorsiDao corso;

	// Int ID del docente

	@Column(name = "docente")
	private Integer docente;

	// Se 1 è interno, se 0 è esterno
	@Column(name = "interno")
	private Integer interno;

	public CorsiDocentiDto convertToDto() {

		CorsiDocentiDto dto = new CorsiDocentiDto();

		dto.setCorsiDocentiId(this.getCorsiDocentiId());
		dto.setCorso(this.getCorso());
		dto.setDocente(this.getDocente());
		dto.setInterno(this.getInterno());

		return dto;
	}

}