package com.its.ipma.corsi.corsi.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.its.ipma.corsi.corsi.dto.CorsiDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "corsi")
@Data
public class CorsiDao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "corso_id")
	private Integer corsoId;

	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "tipo", referencedColumnName = "tipo_id")
	private TipiDao tipo;

	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "tipologia", referencedColumnName = "tipologia_id")
	private TipologieDao tipologia;

	@Column(name = "descrizione")
	private String descrizione;

	@Column(name = "edizione")
	private String edizione;

	@Column(name = "previsto")
	private Integer previsto;

	@Column(name = "erogato")
	private Integer erogato;

	@Column(name = "durata")
	private Integer durata;

	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "misura", referencedColumnName = "misura_id")
	private MisureDao misura;

	@Column(name = "note")
	private String note;

	@Column(name = "luogo")
	private String luogo;

	@Column(name = "ente")
	private String ente;

	@Column(name = "data_erogazione")
	private Date dataErogazione;

	@Column(name = "data_chiusura")
	private Date dataChiusura;

	@Column(name = "data_censimento")
	private Date dataCensimento;

	@Column(name = "interno")
	private Integer interno;

	@Column(name = "tipo_id")
	private Integer tipoId;

	@Column(name = "tipologia_id")
	private Integer tipologiaId;

	@Column(name = "misura_id")
	private Integer misuraId;

	public CorsiDto convertToDto() {

		CorsiDto dto = new CorsiDto();

		dto.setCorsoId(this.getCorsoId());
		dto.setTipo(this.getTipo());
		dto.setTipologia(this.getTipologia());
		dto.setDescrizione(this.getDescrizione());
		dto.setEdizione(this.getEdizione());
		dto.setPrevisto(this.getPrevisto());
		dto.setErogato(this.getErogato());
		dto.setDurata(this.getDurata());
		dto.setMisura(this.getMisura());
		dto.setNote(this.getNote());
		dto.setLuogo(this.getLuogo());
		dto.setEnte(this.getEnte());
		dto.setDataErogazione(this.getDataErogazione());
		dto.setDataChiusura(this.getDataChiusura());
		dto.setDataCensimento(this.getDataCensimento());
		dto.setInterno(this.getInterno());
		dto.setTipoId(this.getTipoId());
		dto.setTipologiaId(this.getTipologiaId());
		dto.setMisuraId(this.getMisuraId());

		return dto;
	}

}