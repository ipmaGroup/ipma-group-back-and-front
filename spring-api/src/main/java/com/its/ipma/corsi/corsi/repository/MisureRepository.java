package com.its.ipma.corsi.corsi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.its.ipma.corsi.corsi.dao.MisureDao;

public interface MisureRepository extends JpaRepository<MisureDao, Integer> {
}
