package com.its.ipma.corsi.corsi.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.its.ipma.corsi.corsi.dto.TeachersRatingDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "valutazioni_docenti")
@Data
public class TeachersRatingDao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "valutazioni_docenti_id")
	private Integer valutazioniDocentiId;

	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "corso", referencedColumnName = "corso_id")
	private CorsiDao corso;

	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "docente", referencedColumnName = "docente_id")
	private TeachersDao docente;

	@Column(name = "interno")
	private Integer interno;

	@Column(name = "criterio")
	private String criterio;

	@Column(name = "insufficente")
	private Integer insufficente;

	@Column(name = "sufficente")
	private Integer sufficente;

	@Column(name = "buono")
	private Integer buono;

	@Column(name = "molto_buono")
	private Integer moltoBuono;

	@Column(name = "non_applicabile")
	private Integer nonApplicabile;

	@Column(name = "media")
	private Double media;

	public TeachersRatingDto convertToDto() {

		TeachersRatingDto dto = new TeachersRatingDto();

		dto.setValutazioniDocentiId(this.getValutazioniDocentiId());
		dto.setCorso(this.getCorso());
		dto.setDocente(this.getDocente());
		dto.setCriterio(this.getCriterio());
		dto.setInsufficente(this.getInsufficente());
		dto.setSufficente(this.getSufficente());
		dto.setBuono(this.getBuono());
		dto.setMoltoBuono(this.getMoltoBuono());
		dto.setNonApplicabile(this.getNonApplicabile());
		dto.setMedia(this.getMedia());
		dto.setInterno(this.getInterno());

		return dto;
	}

}