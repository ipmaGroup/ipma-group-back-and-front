package com.its.ipma.corsi.corsi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.its.ipma.corsi.corsi.dao.TipiDao;

public interface TipiRepository extends JpaRepository<TipiDao, Integer> {
}
