package com.its.ipma.corsi.corsi.dto;

import com.its.ipma.corsi.corsi.dao.CorsiDao;
import com.its.ipma.corsi.corsi.dao.CoursesRatingDao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CoursesRatingDto {

	private Integer valutazioniCorsiId;
	private CorsiDao corso;
	private String criterio;
	private Integer insufficente;
	private Integer sufficente;
	private Integer buono;
	private Integer moltoBuono;
	private Integer nonApplicabile;
	private Double media;

	public CoursesRatingDao convertToDao() {

		CoursesRatingDao dao = new CoursesRatingDao();

		dao.setValutazioniCorsiId(this.getValutazioniCorsiId());
		dao.setCorso(this.getCorso());
		dao.setCriterio(this.getCriterio());
		dao.setInsufficente(this.getInsufficente());
		dao.setSufficente(this.getSufficente());
		dao.setBuono(this.getBuono());
		dao.setMoltoBuono(this.getMoltoBuono());
		dao.setNonApplicabile(this.getNonApplicabile());
		dao.setMedia(this.getMedia());

		return dao;

	}

}
