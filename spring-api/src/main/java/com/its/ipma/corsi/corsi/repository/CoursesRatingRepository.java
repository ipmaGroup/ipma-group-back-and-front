package com.its.ipma.corsi.corsi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.its.ipma.corsi.corsi.dao.CoursesRatingDao;

public interface CoursesRatingRepository extends JpaRepository<CoursesRatingDao, Integer> {
}
