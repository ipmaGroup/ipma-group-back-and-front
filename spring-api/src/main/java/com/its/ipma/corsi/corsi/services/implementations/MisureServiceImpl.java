package com.its.ipma.corsi.corsi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.its.ipma.corsi.corsi.dao.MisureDao;
import com.its.ipma.corsi.corsi.dto.MisureDto;
import com.its.ipma.corsi.corsi.repository.MisureRepository;
import com.its.ipma.corsi.corsi.services.interfaces.MisureInterface;

@Service
@Transactional
public class MisureServiceImpl implements MisureInterface {

	@Autowired
	MisureRepository misureRepository;

	@Override
	public List<MisureDto> fetchAll() {

		List<MisureDto> list = new ArrayList<>();
		try {
			for (MisureDao misure : misureRepository.findAll()) {
				list.add(misure.convertToDto());
			}
		} catch (Exception e) {
			return null;
		}
		return list;
	}

	@Override
	public MisureDto readById(int id) {

		try {
			return misureRepository.getOne(id).convertToDto();
		} catch (Exception e) {
			return null;
		}

	}

}
