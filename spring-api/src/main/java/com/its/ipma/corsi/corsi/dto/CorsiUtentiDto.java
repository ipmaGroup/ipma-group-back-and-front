package com.its.ipma.corsi.corsi.dto;

import com.its.ipma.corsi.corsi.dao.CorsiDao;
import com.its.ipma.corsi.corsi.dao.CorsiUtentiDao;
import com.its.ipma.corsi.corsi.dao.EmployeesDao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CorsiUtentiDto {

	private Integer corsiUtentiId;
	private CorsiDao corso;
	private EmployeesDao dipendente;
	private String durata;

	public CorsiUtentiDao convertToDao() {

		CorsiUtentiDao dao = new CorsiUtentiDao();

		dao.setCorsiUtentiId(this.getCorsiUtentiId());
		dao.setCorso(this.getCorso());
		dao.setDipendente(this.getDipendente());
		dao.setDurata(this.getDurata());

		return dao;

	}

}
