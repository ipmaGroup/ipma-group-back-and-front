package com.its.ipma.corsi.corsi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.its.ipma.corsi.corsi.dao.CoursesRatingDao;
import com.its.ipma.corsi.corsi.dto.CoursesRatingDto;
import com.its.ipma.corsi.corsi.repository.CoursesRatingRepository;
import com.its.ipma.corsi.corsi.services.interfaces.CoursesRatingInterface;

@Service
@Transactional
public class CoursesRatingServiceImpl implements CoursesRatingInterface {

	@Autowired
	CoursesRatingRepository coursesRatingRepository;

	@Override
	public boolean create(CoursesRatingDao dao) {

		try {
			coursesRatingRepository.save(dao);
		} catch (Exception e) {
			return false;
		}
		return true;

	}

	@Override
	public List<CoursesRatingDto> fetchAll() {

		List<CoursesRatingDto> list = new ArrayList<>();
		try {
			for (CoursesRatingDao rating : coursesRatingRepository.findAll()) {
				list.add(rating.convertToDto());
			}
		} catch (Exception e) {
			return null;
		}
		return list;
	}

	@Override
	public boolean update(CoursesRatingDao dao) {
		if (coursesRatingRepository.existsById(dao.getValutazioniCorsiId())) {
			try {
				coursesRatingRepository.save(dao);
			} catch (Exception e) {
				return false;
			}
			return true;
		}
		return false;

	}

	@Override
	public boolean deleteById(int id) {

		if (coursesRatingRepository.existsById(id)) {
			coursesRatingRepository.deleteById(id);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public CoursesRatingDto readById(int id) {

		try {
			return coursesRatingRepository.getOne(id).convertToDto();
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public List<CoursesRatingDto> filter(CoursesRatingDao filters, int pagina, int quantita) {

		Pageable p = PageRequest.of(pagina - 1, quantita);

		ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher.matchingAll().withIgnoreCase()
				.withMatcher("valutazioniUtentiId", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("corso.tipo.descrizione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("corso.tipologia.descrizione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("corso.descrizione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("corso.luogo", ExampleMatcher.GenericPropertyMatchers.contains());
		Example<CoursesRatingDao> example = Example.of(filters, caseInsensitiveExampleMatcher);

		Page<CoursesRatingDao> ratingDao = coursesRatingRepository.findAll(example, p);
		ArrayList<CoursesRatingDto> ratingDto = new ArrayList<CoursesRatingDto>();
		for (CoursesRatingDao rating : ratingDao) {
			ratingDto.add(rating.convertToDto());
		}
		return ratingDto;
	}

	@Override
	public List<CoursesRatingDto> pageLayout(int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina - 1, quantita);
		Page<CoursesRatingDao> listaPagina = coursesRatingRepository.findAll(p);
		ArrayList<CoursesRatingDto> coursesDto = new ArrayList<CoursesRatingDto>();
		for (CoursesRatingDao e : listaPagina) {
			coursesDto.add(e.convertToDto());
		}
		return coursesDto;
	}

}
