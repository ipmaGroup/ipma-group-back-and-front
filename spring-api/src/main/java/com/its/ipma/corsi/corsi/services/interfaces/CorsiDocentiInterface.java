package com.its.ipma.corsi.corsi.services.interfaces;

import java.util.List;

import com.its.ipma.corsi.corsi.dao.CorsiDocentiDao;
import com.its.ipma.corsi.corsi.dto.CorsiDocentiDto;

public interface CorsiDocentiInterface {
	CorsiDocentiDto readById(int id);

	List<CorsiDocentiDto> filter(CorsiDocentiDao filters);

	List<CorsiDocentiDto> fetchAll();
	
	List<CorsiDocentiDto> pageLayout(int pagina, int quantita);

	boolean update(CorsiDocentiDao dao);

	boolean deleteById(int id);

	boolean create(CorsiDocentiDao dao);
}
