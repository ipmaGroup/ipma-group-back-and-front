package com.its.ipma.corsi.corsi.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.its.ipma.corsi.corsi.dto.CoursesRatingDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "valutazioni_corsi")
@Data
public class CoursesRatingDao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "valutazioni_corsi_id")
	private Integer valutazioniCorsiId;

	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "corso", referencedColumnName = "corso_id")
	private CorsiDao corso;

	@Column(name = "criterio")
	private String criterio;

	@Column(name = "insufficente")
	private Integer insufficente;

	@Column(name = "sufficente")
	private Integer sufficente;

	@Column(name = "buono")
	private Integer buono;

	@Column(name = "molto_buono")
	private Integer moltoBuono;

	@Column(name = "non_applicabile")
	private Integer nonApplicabile;

	@Column(name = "media")
	private Double media;

	public CoursesRatingDto convertToDto() {

		CoursesRatingDto dto = new CoursesRatingDto();

		dto.setValutazioniCorsiId(this.getValutazioniCorsiId());
		dto.setCriterio(this.getCriterio());
		dto.setCorso(this.getCorso());
		dto.setInsufficente(this.getInsufficente());
		dto.setSufficente(this.getSufficente());
		dto.setBuono(this.getBuono());
		dto.setMoltoBuono(this.getMoltoBuono());
		dto.setNonApplicabile(this.getNonApplicabile());
		dto.setMedia(this.getMedia());

		return dto;
	}

}