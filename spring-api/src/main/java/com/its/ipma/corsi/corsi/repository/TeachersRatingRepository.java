package com.its.ipma.corsi.corsi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.its.ipma.corsi.corsi.dao.TeachersRatingDao;

public interface TeachersRatingRepository extends JpaRepository<TeachersRatingDao, Integer> {
}
