package com.its.ipma.corsi.corsi.services.interfaces;

import java.util.List;

import com.its.ipma.corsi.corsi.dao.CoursesRatingDao;
import com.its.ipma.corsi.corsi.dto.CoursesRatingDto;

public interface CoursesRatingInterface {

	CoursesRatingDto readById(int id);

	List<CoursesRatingDto> filter(CoursesRatingDao filters, int pagina, int quantita);

	List<CoursesRatingDto> fetchAll();

	boolean update(CoursesRatingDao dao);

	boolean deleteById(int id);

	boolean create(CoursesRatingDao dao);

	List<CoursesRatingDto> pageLayout(int pagina, int quantita);
}
