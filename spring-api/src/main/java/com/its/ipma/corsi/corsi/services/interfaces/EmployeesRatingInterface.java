package com.its.ipma.corsi.corsi.services.interfaces;

import java.util.List;

import com.its.ipma.corsi.corsi.dao.EmployeesRatingDao;
import com.its.ipma.corsi.corsi.dto.EmployeesRatingDto;

public interface EmployeesRatingInterface {
	EmployeesRatingDto readById(int id);

	List<EmployeesRatingDto> filter(EmployeesRatingDao filters, int pagina, int quantita);

	List<EmployeesRatingDto> fetchAll();

	boolean update(EmployeesRatingDao dao);

	boolean deleteById(int id);

	boolean create(EmployeesRatingDao dao);

	List<EmployeesRatingDto> pageLayout(int pagina, int quantita);
}
