package com.its.ipma.corsi.corsi.dto;

import com.its.ipma.corsi.corsi.dao.TipologieDao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TipologieDto {

	private Integer tipologiaId;
	private String descrizione;

	public TipologieDao convertToDao() {

		TipologieDao dao = new TipologieDao();

		dao.setTipologiaId(this.getTipologiaId());
		dao.setDescrizione(this.getDescrizione());

		return dao;

	}

}
