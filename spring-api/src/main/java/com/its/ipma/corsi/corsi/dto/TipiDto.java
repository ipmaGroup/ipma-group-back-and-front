package com.its.ipma.corsi.corsi.dto;

import com.its.ipma.corsi.corsi.dao.TipiDao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TipiDto {

	private Integer tipoId;
	private String descrizione;

	public TipiDao convertToDao() {

		TipiDao dao = new TipiDao();

		dao.setTipoId(this.getTipoId());
		dao.setDescrizione(this.getDescrizione());

		return dao;

	}

}
