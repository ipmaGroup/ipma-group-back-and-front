package com.its.ipma.corsi.corsi.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.its.ipma.corsi.corsi.dto.CorsiUtentiDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "corsi_utenti")
@Data
public class CorsiUtentiDao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "corsi_utenti_id")
	private Integer corsiUtentiId;

	// Chiave esterna di corso
	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "corso", referencedColumnName = "corso_id")
	@JsonBackReference(value="corso")
	private CorsiDao corso;
	
	// Chiave esterna del dipendente
	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "dipendente", referencedColumnName = "dipendente_id")
	@JsonBackReference(value="dipendente")
	private EmployeesDao dipendente;

	@Column(name = "durata")
	private String durata;
	

	public CorsiUtentiDto convertToDto() {

		CorsiUtentiDto dto = new CorsiUtentiDto();

		dto.setCorsiUtentiId(this.getCorsiUtentiId());
		dto.setCorso(this.getCorso());
		dto.setDipendente(this.getDipendente());
		dto.setDurata(this.getDurata());

		return dto;
	}

}