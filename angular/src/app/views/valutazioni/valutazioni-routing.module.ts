import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ValutazioniComponent } from './valutazioni/valutazioni.component';


const routes: Routes = [{
  path: '',
  component: ValutazioniComponent,
  data: {
    title: 'Valutazioni'
  }
},];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ValutazioniRoutingModule { }
