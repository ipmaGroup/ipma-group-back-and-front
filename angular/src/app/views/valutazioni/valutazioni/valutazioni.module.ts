import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ValutazioniRoutingModule } from './valutazioni-routing.module';
import { ValutazioniComponent } from './valutazioni.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SidebarModule } from 'primeng/sidebar';
import { TabViewModule } from 'primeng/tabview';


@NgModule({
  declarations: [ValutazioniComponent],
  imports: [
    CommonModule,
    ValutazioniRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SidebarModule,
    TabViewModule
  ]
})
export class ValutazioniModule { }
