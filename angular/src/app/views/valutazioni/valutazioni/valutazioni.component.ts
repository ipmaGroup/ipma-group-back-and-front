import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { VoteServices } from "../../../core/services/vote.service";
import { FormBuilder, FormGroup } from "@angular/forms";
import { CourseUsersService } from "../../../core/services/course_users.service";
import { CourseService } from "../../../core/services/course.service";
import { TeachersService } from "../../../core/services/teachers.service";
import { EmployeesServices } from "../../../core/services/employees.services";
import { element } from "protractor";
import { Footer } from "primeng/api/shared";
import { elementEventFullName } from "@angular/compiler/src/view_compiler/view_compiler";

@Component({
  selector: "app-valutazioni",
  templateUrl: "./valutazioni.component.html",
  styleUrls: ["./valutazioni.component.css"],
})
export class ValutazioniComponent implements OnInit {

  public corsoParam: any[] = ["Usefulness of the course for professional growth", "Usefulness of the course in the current working environment",
    "Choice of specific topics", "Deepening level of specific topics", "Course duration",
    "Correct division between theory and practice", "Exercises effectiveness", "Teaching material quality"];

  public docenteParam: any[] = ["Completeness", "Clarity in the exposition", "Availability"];

  public dipendenteParam: any[] = ["Commitment", "Logic", "Learning", "Speed", "Order and precision"];

  public formGroupCorsi: FormGroup;

  public formGroupDocente: FormGroup;

  public formGroupDip: FormGroup;

  public listaVotiCorso: any[] = [];

  public listaDipendenti: any[] = [];

  public listaVotiDocente: any[] = [];

  public listaVotiDipendente: any[] = [];

  public idCorso: any;

  public interno: any;

  public idDocente: any;

  public allDatacorso: any;

  public allTeachersData: any;

  public nome: any;

  public desc: any;

  constructor(
    public courseUserServices: CourseUsersService,
    public router: Router,
    public route: ActivatedRoute,
    public voteServices: VoteServices,
    public fb: FormBuilder,
    public courseServices: CourseService,
    public teachersServices: TeachersService,
    public employeesServices: EmployeesServices
  ) { }

  ngOnInit(): void {

    this.idDocente = this.route.snapshot.params["idDocente"];
    this.idCorso = this.route.snapshot.params["id"];
    this.interno = this.route.snapshot.params["interno"];

    this.loadAllCorsi();
    this.loadDocenti();
    this.loadDipendenti();

    //prende descrizione corso
    let corso = { corsoId: this.idCorso };
    let idOBJ = Object.assign({}, corso);
    this.courseServices.getById(idOBJ, 1, 1).subscribe((element) => {
      this.allDatacorso = element.response[0];
      this.desc = this.allDatacorso.descrizione;
    });

    //prende nome insegnate
    if (this.interno == 0) {
      let docente = { docenteId: this.idDocente };
      let idOBJ = Object.assign({}, docente);
      this.teachersServices.getById(idOBJ, 1, 1).subscribe((element) => {
        this.allTeachersData = element.response;
        this.nome = this.allTeachersData[0].titolo;
      });
    } else {
      let dipendente = { dipendenteId: this.idDocente };
      let idOBJ = Object.assign({}, dipendente);
      this.employeesServices.getById(idOBJ, 1, 1).subscribe((element) => {
        this.allTeachersData = element.response;
        this.nome = this.allTeachersData[0].nome + " " + this.allTeachersData[0].cognome;
      });
    }

    //prende tutti i dipendenti
    this.courseUserServices.getAllCourseUsers().subscribe((element) => {
      for (let i = 0; i < element.response.length; i++) {
        if (element.response[i].corso.corsoId == this.idCorso) {
          this.listaDipendenti.push(element.response[i]);
        }
      }
    });

    this.formGroupCorsi = this.fb.group({
      corso: {
        corsoId: this.idCorso,
      },
      criterio: [""],
      insufficente: [""],
      sufficente: [""],
      buono: [""],
      moltoBuono: [""],
      nonApplicabile: [""],
      media: [""]
    });

    this.formGroupDocente = this.fb.group({
      corso: {
        corsoId: this.idCorso,
      },
      docente: {
        docenteId: this.idDocente,
      },
      criterio: [],
      insufficente: [],
      sufficente: [],
      buono: [],
      moltoBuono: [],
      nonApplicabile: [],
      media: [],
      interno: this.interno,
    });

    this.formGroupDip = this.fb.group({
      corso: {
        corsoId: this.idCorso,
      },
      dipendente: {
        dipendenteId: [],
      },
      criterio: [],
      totalmenteInsufficente: [],
      insufficente: [],
      sufficente: [],
      discreto: [],
      buono: [],
      ottimo: [],
      media: []
    });
  }

  creaVoto(param: any) {
    let nVoti = this.formGroupCorsi.value["insufficente"] + this.formGroupCorsi.value["sufficente"] + this.formGroupCorsi.value["buono"] + this.formGroupCorsi.value["moltoBuono"];
    let somma = this.formGroupCorsi.value["insufficente"] + (this.formGroupCorsi.value["sufficente"] * 2) + (this.formGroupCorsi.value["buono"] * 3) + (this.formGroupCorsi.value["moltoBuono"] * 4);
    this.formGroupCorsi.value["media"] = somma / nVoti;
    this.formGroupCorsi.value["criterio"] = param;
    let newFinale = Object.assign({}, this.formGroupCorsi.value);
    this.voteServices.addCourses(newFinale).subscribe(() => {
      this.loadAllCorsi();
    });
  }

  creaVotoDocente(param: any) {
    let nVoti = this.formGroupDocente.value["insufficente"] + this.formGroupDocente.value["sufficente"] + this.formGroupDocente.value["buono"] + this.formGroupDocente.value["moltoBuono"];
    let somma = this.formGroupDocente.value["insufficente"] + (this.formGroupDocente.value["sufficente"] * 2) + (this.formGroupDocente.value["buono"] * 3) + (this.formGroupDocente.value["moltoBuono"] * 4);
    this.formGroupDocente.value["media"] = somma / nVoti;
    this.formGroupDocente.value["criterio"] = param;
    let newFinale = Object.assign({}, this.formGroupDocente.value);
    this.voteServices.addTeachers(newFinale).subscribe(() => {
      this.loadDocenti();
    });
  }

  creaVotoDipendente(id: any) {
    let voti = this.formGroupDip.value["totalmenteInsufficente"] + this.formGroupDip.value["insufficente"] + this.formGroupDip.value["sufficente"] + this.formGroupDip.value["discreto"] + this.formGroupDip.value["buono"];
    this.formGroupDip.value["media"] = voti / this.dipendenteParam.length;
    this.formGroupDip.value.dipendente["dipendenteId"] = id;
    let newFinale = Object.assign({}, this.formGroupDip.value);
    this.voteServices.addEmployees(newFinale).subscribe(() => {
      this.loadDipendenti();
    });
  }

  loadDocenti() {
    let list = [];
    let votiDocente = { docente: { docenteId: this.idDocente } };
    let voti = Object.assign({}, votiDocente);
    this.voteServices.getAllTeachers(voti, 1, 999).subscribe(e => {
      list = e.response;
      for (let i = e.response?.length - 1; i >= 0; i--) {
        let b = false;
        for (let j = 0; j < e.response?.length; j++) {
          if (list[i].criterio == this.listaVotiDocente[j]?.criterio) {
            b = true;
          }
        }
        if (!b) {
          for (let k = 0; k < this.docenteParam.length; k++) {
            if (this.docenteParam[k] == list[i].criterio) {
              this.listaVotiDocente[k] = list[i];
            }
          }
        }
      }
    });
  }

  loadAllCorsi() {
    let list = [];
    let votiCorso = { corso: { corsoId: this.idCorso } };
    let voti = Object.assign({}, votiCorso);
    this.voteServices.getAllCourses(voti, 1, 999).subscribe(e => {
      list = e.response;
      for (let i = e.response?.length - 1; i >= 0; i--) {
        let b = false;
        for (let j = 0; j < e.response?.length; j++) {
          if (list[i].criterio == this.listaVotiCorso[j]?.criterio) {
            b = true;
          }
        }
        if (!b) {
          for (let k = 0; k < this.corsoParam.length; k++) {
            if (this.corsoParam[k] == list[i].criterio) {
              this.listaVotiCorso[k] = list[i];
            }
          }
        }
      }
    });
  }

  loadDipendenti() {
    let list = [];
    let votiDipendente = { corso: { corsoId: this.idCorso } };
    let voti = Object.assign({}, votiDipendente);
    this.voteServices.getAllEmployees(voti, 1, 999).subscribe(e => {
      list = e.response;
      for (let i = e.response?.length - 1; i >= 0; i--) {
        let b = false;
        for (let j = 0; j < e.response?.length; j++) {
          if (list[i].dipendente.dipendenteId == this.listaVotiDipendente[j]?.dipendente.dipendenteId) {
            b = true;
          }
        }
        if (!b) {
          this.listaVotiDipendente.push(list[i]);
          this.listaVotiDipendente.reverse();
        }
      }
    });
  }
}
