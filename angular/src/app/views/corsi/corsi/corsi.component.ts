import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { Message, ConfirmationService } from "primeng/api";
import { ApiService } from "../../../core/services/api.service";
import { CourseService } from "../../../core/services/course.service";
import { Router } from "@angular/router";
import { paix } from "paix";
import { DatePipe, formatDate } from "@angular/common";
import { pipe } from "rxjs/internal/util/pipe";
import { CourseUsersService } from "../../../core/services/course_users.service";
import { TeachersService } from "../../../core/services/teachers.service";
import { element } from "protractor";
import { EmployeesServices } from "../../../core/services/employees.services";

@Component({
  selector: "app-corsi",
  templateUrl: "./corsi.component.html",
  styleUrls: ["./corsi.component.css"],
  providers: [ConfirmationService],
})
export class CorsiComponent implements OnInit {
  public filtroSelezionato: any;

  public visibleSidebartop: boolean;

  public pipe: any;

  public tipo: any[] = [];

  public pagina: any = 1;

  public dati: any = 8;

  public nPagine: any = 0;

  public idDocente: any;

  public formGroup: FormGroup;
  public formGroup2: FormGroup;

  public visibleSidebar5: boolean;

  public displayMaximizable: boolean;

  public filt: any[] = [];

  msgs: Message[] = [];

  public utCorr: any;

  public lista: any[] = [];
  public lista2: any[] = [];

  public listaAllCourses: any[] = [];
  public listaAllCoursesTeachers: any[] = [];

  public listaAllTeachers: any[] = [];

  public listaAllEmployees: any[] = [];

  public docCorso: any[] = [];

  public varDaMandare: any;

  public risposta: any;

  public dtf = new Intl.DateTimeFormat('en', { year: 'numeric', month: 'short', day: '2-digit' })

  public dataErogazione: Date;

  public dataChiusura: Date;

  public dataCensimento: Date;

  public dataFormatted: any;
  public dataFormatted1: any;
  public dataFormatted2: any;

  constructor(
    public api: ApiService,
    public corsiService: CourseService,
    public router: Router,
    private confirmationService: ConfirmationService,
    public fb: FormBuilder,
    public courseUserServices: CourseUsersService,
    public teacherServices: TeachersService,
    public employeesServices: EmployeesServices
  ) {
    this.tipo = [
      { name: "id", code: "corsoId" },
      { name: "description", code: "descrizione" },
      { name: "edition", code: "edizione" },
      { name: "expected", code: "previsto" },
      { name: "provided", code: "erogato" },
      { name: "duration", code: "durata" },
      { name: "notes", code: "note" },
      { name: "location", code: "luogo" },
      { name: "enity", code: "ente" },
      { name: "delivery date", code: "dataErogazione" },
      { name: "closure date", code: "dataChiusura" },
      { name: "census date", code: "dataCensimento" },
      { name: "internal", code: "interno" },
    ];
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      soggFiltrato: [""],
    });

    this.formGroup2 = this.fb.group({
      corsoId: [""],
      descrizione: [""],
      edizione: [""],
      previsto: [""],
      erogato: [""],
      durata: [""],
      note: [""],
      luogo: [""],
      ente: [""],
      dataErogazione: [""],
      dataChiusura: [""],
      dataCensimento: [""],
      interno: [""],
      tipoId: [""],
      tipologiaId: [""],
      misuraId: [""],
    });

    this.corsiService.getAll().subscribe((resp) => {
      this.lista2 = resp.response;
    });


    this.loadAll();
  }

  confirm1(val: any) {
    this.confirmationService.confirm({
      message: "Sei sicuro di voler eliminare il corso?",
      header: "Conferma",
      icon: "pi pi-exclamation-triangle",
      key: "conferma",
      accept: () => {
        this.msgs = [
          {
            severity: "success",
            summary: "Confermato",
            detail: "Eliminazione Completata",
          },
        ];
        this.delete(val);
        this.confirmationService.close();
      },
      reject: () => {
        this.msgs = [
          {
            severity: "info",
            summary: "Annullato",
            detail: "Operazione Annullata",
          },
        ];
        this.confirmationService.close();
      },
    });
  }

  /*   loadAll() {
      this.corsiService.getAllPage(this.pagina, this.dati).subscribe((resp) => {
        this.lista = resp.response;
      });
    } */

  loadAll() {
    this.lista = [];
    this.corsiService.getAllPage(this.pagina, this.dati).subscribe((resp) => {
      this.risposta = resp.response;
      if (this.risposta) {
        this.risposta.forEach((element) => {
          this.lista.unshift({
            descrizione: element.descrizione,
            tipo: element.tipo.descrizione,
            tipologia: element.tipologia.descrizione,
            corsoId: element.corsoId,
            value: { element },
          });
        });
        console.log("Val ", this.lista)
      }
    });
  }

  show(valore: any) {
    this.visibleSidebar5 = true;

    this.utCorr = valore;
    if(this.utCorr.value.element.dataErogazione!=null){
      this.dataErogazione = new Date(this.utCorr.value.element.dataErogazione);
      console.log("Data1: " , this.dataErogazione);      
      this.dtf.formatToParts(this.dataErogazione);
      console.log("Data2: " , this.dataErogazione);
      this.dataFormatted = (this.dataErogazione.getDate() + 1) + "/" + (this.dataErogazione.getMonth() + 1) + "/" + this.dataErogazione.getFullYear()
      this.dataErogazione = this.dataFormatted;
      console.log("Data3: " , this.dataErogazione);
    }
    if(this.utCorr.value.element.dataChiusura!=null){
      this.dataChiusura = new Date(this.utCorr.value.element.dataChiusura);      
      this.dtf.formatToParts(this.dataChiusura);
      this.dataFormatted1 = (this.dataChiusura.getDate() + 1) + "/" + (this.dataChiusura.getMonth() + 1) + "/" + this.dataChiusura.getFullYear()
      this.dataChiusura = this.dataFormatted;
    }
    if(this.utCorr.value.element.dataCensimento!=null){
      this.dataCensimento = new Date(this.utCorr.value.element.dataCensimento);      
      this.dtf.formatToParts(this.dataCensimento);
      this.dataFormatted2 = (this.dataCensimento.getDate() + 1) + "/" + (this.dataCensimento.getMonth() + 1) + "/" + this.dataCensimento.getFullYear()
      this.dataCensimento = this.dataFormatted;
    }
    this.showCorsesteachers(valore);
    this.showCorsesEmployees(valore);
  }

  showMaximizableDialog() {
    this.displayMaximizable = true;
  }

  new() {
    this.router.navigate(["/new-course"]);
  }

  delete(val: any) {
    this.corsiService.deleteById(val).subscribe(() => {
      if (this.filtroSelezionato == null) {
        this.loadAll();
      } else {
        this.filtra();
      }
    });
  }

  edit(id: any) {
    this.router.navigate(["/edit-course/" + id]);
  }

  filtra() {
    let res = { nome: this.formGroup.value.soggFiltrato };
    let replacement = { nome: this.filtroSelezionato.code };
    let modificato = paix(res, replacement);

    this.corsiService
      .getById(modificato, this.pagina, this.dati)
      .subscribe((resp) => {
        this.lista = resp.response;
        this.lista2 = resp.response;
      });
  }

  avanti() {
    this.nPagine = this.lista2.length / this.dati;

    if (this.pagina < this.nPagine) this.pagina++;

    if (this.filtroSelezionato == null) {
      this.loadAll();
    } else {
      this.filtra();
    }
  }

  dietro() {
    if (this.pagina - 1 > 0) {
      this.pagina--;

      if (this.filtroSelezionato == null) {
        this.loadAll();
      } else {
        this.filtra();
      }
    }
  }
  removeFilter() {
    this.loadAll();
    this.filtroSelezionato = null;
    this.formGroup = this.fb.group({
      soggFiltrato: [""],
    });

    this.formGroup2 = this.fb.group({
      corsoId: [""],
      descrizione: [""],
      edizione: [""],
      previsto: [""],
      erogato: [""],
      durata: [""],
      note: [""],
      luogo: [""],
      ente: [""],
      dataErogazione: [""],
      dataChiusura: [""],
      dataCensimento: [""],
      interno: [""],
      tipoId: [""],
      tipologiaId: [""],
      misuraId: [""],
    });
  }

  filtraAvanzato() {
    this.visibleSidebartop = true;
  }

  filtraA() {
    var primo = [];
    var secondo = [];
    for (const field in this.formGroup2.controls) {
      if (this.formGroup2.controls[field].value != "") {
        primo.push(field);
        secondo.push(this.formGroup2.controls[field].value);
      }
    }

    var finale = paix(secondo, primo);

    let newFinale = Object.assign({}, finale);

    this.corsiService
      .getById(newFinale, this.pagina, this.dati)
      .subscribe((resp) => {
        this.lista = resp.response;
        this.lista2 = resp.response;
      });
    this.visibleSidebartop = false;
  }

  valutazioni(id: any) {
    let i = 0;
    this.courseUserServices.getAllCourseTeachers().subscribe((element) => {
      for (i = 0; i < element.response.length; i++) {
        if (element.response[i].corso.corsoId == id) {
          this.router.navigate([
            "/valutazioni/" +
            id +
            "/" +
            element.response[i].docente +
            "/" +
            element.response[i].interno,
          ]);
        }
      }
    });
  }

  showCorsesEmployees(valore: any) {
    this.listaAllCourses = [];
    this.courseUserServices.getAllCourseUsers().subscribe((element) => {
      for (let i = 0; i < element.response.length; i++)
        if (element.response[i].corso.corsoId == valore.corsoId)
          this.listaAllCourses.push(element.response[i].dipendente);
    });
  }

  showCorsesteachers(valore: any) {
    console.log(valore);
    let i = 0;
    this.listaAllCoursesTeachers = [];
    this.listaAllEmployees = [];
    this.docCorso = [];


    this.teacherServices.getAll().subscribe((element2) => {
      for (let k = 0; k < element2.response.length; k++)
        this.listaAllTeachers.push(element2.response[k]);
    });

    this.employeesServices.getAll().subscribe((element2) => {
      for (let k = 0; k < element2.response.length; k++)
        this.listaAllEmployees.push(element2.response[k]);
    });

    console.log(this.listaAllEmployees);

    this.courseUserServices.getAllCourseTeachers().subscribe((element) => {
      console.log(element);
      for (i = 0; i < element.response.length; i++) {
        if (element.response[i].interno == 0) {
          for (let k = 0; k < this.listaAllTeachers.length; k++) {
            if (
              this.listaAllTeachers[k].docenteId ==
              element.response[i].docente &&
              element.response[i].corso.corsoId == valore.corsoId
            ) {
              this.docCorso.push(this.listaAllTeachers[k]);
              this.varDaMandare = {
                nome: this.listaAllTeachers[k].titolo,
                cognome: this.listaAllTeachers[k].ragioneSociale,
                id: this.listaAllTeachers[k].docenteId
              }
            }
          }
        }
        else {
          for (let k = 0; k < this.listaAllEmployees.length; k++) {
            if (
              this.listaAllEmployees[k].dipendenteId ==
              element.response[i].docente &&
              element.response[i].corso.corsoId == valore.corsoId
            ) {
              this.docCorso.push(this.listaAllEmployees[k] + "doc");
              this.varDaMandare = {
                nome: this.listaAllEmployees[k].nome,
                cognome: this.listaAllEmployees[k].cognome,
                id: this.listaAllEmployees[k].dipendenteId
              }
            }
          }
        }
      }
    });
    console.log(this.docCorso);
  }
}
