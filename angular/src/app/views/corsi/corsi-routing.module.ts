import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CorsiComponent } from './corsi/corsi.component';


const routes: Routes = [{ 
  path: '', 
  component: CorsiComponent,
  data: {
    title: 'corsi'
  }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CorsiRoutingModule { }
