import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CorsiRoutingModule } from './corsi-routing.module';
import { CorsiComponent } from './corsi/corsi.component';
import { TableModule } from "primeng/table";
import { SidebarModule } from "primeng/sidebar";
import { DropdownModule } from "primeng/dropdown";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { HttpClientModule } from "@angular/common/http";
import { HttpModule } from "@angular/http";
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ButtonModule} from 'primeng/button';
import {MessagesModule} from 'primeng/messages';
import {DialogModule} from 'primeng/dialog';
import { CalendarModule } from 'primeng/calendar';


@NgModule({
  declarations: [
    CorsiComponent
  ],
  imports: [
    CommonModule,
    CorsiRoutingModule,
    TableModule,
    SidebarModule,
    DropdownModule,
    FormsModule,
    ReactiveFormsModule,
    CalendarModule,

    HttpClientModule,
    HttpModule,
    ConfirmDialogModule,
    DialogModule,
    ButtonModule,
    MessagesModule
  ]
})
export class CorsiModule { }
