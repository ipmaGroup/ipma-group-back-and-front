import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { CourseService } from "../../../core/services/course.service";
import { Router, ActivatedRoute } from "@angular/router";
import { EmployeesServices } from '../../../core/services/employees.services';
import { TeachersService } from '../../../core/services/teachers.service';
import { CourseUsersService } from '../../../core/services/course_users.service';
import { DataTableOptions } from '../../../api/datatable.options';
import { ConfirmationService } from "primeng/api";
import { Message } from "primeng/api";
import { element } from 'protractor';

@Component({
  selector: "app-edit-course",
  templateUrl: "./edit-course.component.html",
  styleUrls: ["./edit-course.component.scss"],
  providers: [ConfirmationService],
})
export class EditCourseComponent implements OnInit {
  public formGroup: FormGroup;
  public formGroupDocenti: FormGroup;
  public formGroupDipendenti: FormGroup;
  public contatore1: any = 0;
  public corsoAgg: any;
  public docentiAgg: any;
  public dipendentiAgg: any;
  public durataN: any;
  public scritta: any = "External";
  public scritta1: any = "External";
  public scritta2: any = "Expected";

  public descrizioneMis: any[];
  public descrizioneTipo: any[];
  public descrizioneTipologia: any[];
  public descrizioneDipendente: any[];
  public descrizioneDocente: any[];
  public descrizioneDipendente1: any[];

  public listaMisure: any[];
  public listaTipi: any[];
  public listaTipologie: any[];
  public listaDipendenti: any[];
  public listaDocenti: any[];
  public listaDipendenti1: any[];

  public listaDocentiCorsi: any[];
  public listaTuttiDocenti: any[];
  public idDocente: any;
  public docenteInterno: any;
  public docenteCorso: any;


  public listaAlunniCorsi: any[];
  public listaTuttiAlunni: any[];
  public alunniCorso: any[];
  public alunniNonCorso: any[];

  public internoControl: boolean = false;
  public internoControl1: boolean = false;
  public erogatoControl: boolean = false;
  public internoInt: any;
  public internoInt1: any = 0;
  public erogatoInt: any;

  public doppione: boolean;
  public verifica1: boolean = false;
  public boolean: boolean = false;

  public dipendenteSel: any;
  public dipendentiSelezionati: any[];

  public arrayValori: any[]

  public dtf = new Intl.DateTimeFormat('en', { year: 'numeric', month: 'short', day: '2-digit' })
  public dataEr: Date;
  public dataCens: Date;
  public dataChiu: Date;
  public dataFormatted: any;
  public dataFormatted1: any;
  public dataFormatted2: any;

  msgs: Message[] = [];

  public displayBasic: boolean;


  public statuses: any[];

  public displayMaximizable: boolean;


  public options: DataTableOptions;

  public tipoCheck: boolean = false;
  public tipologiaCheck: boolean = false;
  public misuraCheck: boolean = false;

  public rowData: any;

  constructor(
    public fb: FormBuilder,
    public courseService: CourseService,
    public employeeService: EmployeesServices,
    public teacherService: TeachersService,
    public courseUsersService: CourseUsersService,
    private confirmationService: ConfirmationService,
    public router: Router,
    public route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.prendiErogato();
    this.prendiDocente();
    this.getMisura();
    this.getTipo();
    this.getTipologia();
    this.getDipendenti();
    /* this.getDocenti();
    this.getDocenteCorso(); */
    this.getAlunniCorso();


    this.options = {
      colsOptions: [
        { label: 'Surname', name: 'cognome' },
        { label: 'Name', name: 'nome' },
        { label: 'Freshman', name: 'matricola' },
        { label: 'Actions', name: 'operazioni' }
      ]
    };



    this.formGroup = this.fb.group({
      tipo: [""],
      tipologia: [""],
      descrizione: [""],
      edizione: [""],
      previsto: [""],
      erogato: [""],
      durata: [""],
      misura: [""],
      note: [""],
      luogo: [""],
      ente: [""],
      dataErogazione: [""],
      dataChiusura: [""],
      dataCensimento: [""],
      interno: [""],
      tipoId: [""],
      tipologiaId: [""],
      misuraId: [""],
    });

    this.formGroupDocenti = this.fb.group({
      corsiDocentiId: [""],
      corso: [""],
      docente: [""],
      interno: [""]

    });

    this.formGroupDipendenti = this.fb.group({
      corso: [""],
      dipendente: [""],
      durata: [""]

    });

    let value = {
      corsoId: this.route.snapshot.params["id"],
    };

    this.dipendentiSelezionati = [{ corso: null, dipendente: { dipendenteId: 0 }, durata: null }];

    this.courseService.getById(value, 1, 1).subscribe((resp) => {
      /* resp.response[0].dataErogazione = this.dtf.formatToParts(resp.response[0].dataErogazione);
      this.dataEr = resp.response[0].dataErogazione; */
      console.log("ooo: " , resp.response[0].dataErogazione);
      if(resp.response[0].dataErogazione!=null){
        this.dataEr = new Date(resp.response[0].dataErogazione);
        
        /* this.dataEr = this.addDays(this.dataEr, 1); */
        this.dtf.formatToParts(this.dataEr);
        this.dataFormatted = (this.dataEr.getDate() + 1) + "/" + (this.dataEr.getMonth() + 1) + "/" + this.dataEr.getFullYear()
        console.log("Dataer: " + this.dataEr);
        console.log("DataForm: " + this.dataFormatted);
        console.log("WHAT " , this.dataEr);
      }
      if(resp.response[0].dataChiusura!=null){
        this.dataChiu = new Date(resp.response[0].dataChiusura);
        /* this.dataChiu = this.addDays(this.dataChiu, 1); */
        this.dtf.formatToParts(this.dataChiu);
        this.dataFormatted1 = (this.dataChiu.getDate() + 1) + "/" + (this.dataChiu.getMonth() + 1) + "/" + this.dataChiu.getFullYear()
      }
      if(resp.response[0].dataCensimento!=null){
        this.dataCens = new Date(resp.response[0].dataCensimento);
        /* this.dataCens = this.addDays(this.dataCens, 1); */
        this.dtf.formatToParts(this.dataCens);
        this.dataFormatted2 = (this.dataCens.getDate() + 1) + "/" + (this.dataCens.getMonth() + 1) + "/" + this.dataCens.getFullYear() 
      }
      
      console.log("Data: " , this.dataEr);
      console.log("Data2: " , this.dataChiu);
      console.log("Data3: " , this.dataCens);
      this.formGroup = this.fb.group({
        corsoId: [this.route.snapshot.params["id"]],
        tipo: [resp.response[0].tipo],
        tipologia: [resp.response[0].tipologia],
        descrizione: [resp.response[0].descrizione],
        edizione: [resp.response[0].edizione],
        previsto: [resp.response[0].previsto],
        erogato: [resp.response[0].erogato],
        durata: [resp.response[0].durata],
        misura: [resp.response[0].misura],
        note: [resp.response[0].note],
        luogo: [resp.response[0].luogo],
        ente: [resp.response[0].ente],
        dataErogazione: /* this.dataFormatted */null,
        dataChiusura: /* this.dataChiu */null,
        dataCensimento: /* this.dataCens */null,
        interno: [resp.response[0].interno],
        tipoId: [resp.response[0].tipoId],
        tipologiaId: [resp.response[0].tipologiaId],
        misuraId: [resp.response[0].misuraId],
      });
    });

    this.courseUsersService.getAllCourseTeachers().subscribe((resp) => {
      for (let i = 0; i < resp.response.length; i++) {
        if (resp.response[i].corso.corsoId == [this.route.snapshot.params["id"]]) {
          this.formGroupDocenti = this.fb.group({
            corsiDocentiId: [resp.response[i].corsiDocentiId],
            corso: [resp.response[i].corso],
            docente: [resp.response[i].docente],
            interno: [resp.response[i].interno]
          });
        }

      }
    });

    this.courseService.getById(value, 1, 1).subscribe((resp) => {
      console.log("Resp: " , resp.response)
      this.formGroup = this.fb.group({
        corsoId: [this.route.snapshot.params["id"]],
        tipo: [resp.response[0].tipo],
        tipologia: [resp.response[0].tipologia],
        descrizione: [resp.response[0].descrizione],
        edizione: [resp.response[0].edizione],
        previsto: [resp.response[0].previsto],
        erogato: [resp.response[0].erogato],
        durata: [resp.response[0].durata],
        misura: [resp.response[0].misura],
        note: [resp.response[0].note],
        luogo: [resp.response[0].luogo],
        ente: [resp.response[0].ente],
        dataErogazione: [resp.response[0].dataErogazione],
        dataChiusura: [resp.response[0].dataChiusura],
        dataCensimento: [resp.response[0].dataCensimento],
        interno: [resp.response[0].interno],
        tipoId: [resp.response[0].tipoId],
        tipologiaId: [resp.response[0].tipologiaId],
        misuraId: [resp.response[0].misuraId],
      });
    });
    
  }

    addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }

  removeDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() - days);
    return result;
  }
  
  prendiErogato(){
    let value = {
      corsoId: this.route.snapshot.params["id"],
    };
    this.courseService.getById(value, 1, 1).subscribe((resp)=>{
      console.log(resp.response[0].erogato);
      if(resp.response[0].erogato==1){
        console.log("FO: " ,  this.formGroup.value);
        this.erogatoControl=true;
        this.metodoErogato();
      }
      else{
        this.metodoErogato();
      }
    }); 
  }
  
  prendiDocente(){
    let value = {
      corsoId: this.route.snapshot.params["id"],
    };
    this.courseUsersService.getAllCourseTeachers().subscribe((resp)=>{
      console.log("Resp: ",resp.response);
      resp.response.forEach(element => {
        console.log("Element: " , element);
        if(element.corso.corsoId == this.route.snapshot.params["id"] && element.interno == 1){
          this.internoControl = true;
          console.log("Control2 " , this.internoControl);
        }
      });
    });
     
    
    setTimeout(() => { this.getDocenti(); }, 1000);
    setTimeout(() => { this.getDocenteCorso(); }, 1000);
  }

  showMaximizableDialog() {
    this.displayMaximizable = true;
  }

  conferma(): void {
    console.log("Da: " , this.dataEr);
    console.log("FORMM  " , this.formGroup.value);
    this.formGroup.value.dataErogazione = this.dataEr;
    this.formGroup.value.dataChiusura = this.dataChiu;
    this.formGroup.value.dataCensimento = this.dataCens;
    if(this.erogatoControl){
      this.formGroup.value.erogato = 1;
      this.formGroup.value.previsto = 0;
    }
    else{
      this.formGroup.value.erogato = 0;
      this.formGroup.value.previsto = 1;
    }
    console.log(this.internoInt1);
    this.formGroup.value.interno = this.internoInt1;
    console.log("Form: " , this.formGroup.value.interno);
    console.log("Form: " , this.formGroup.value);
    this.corsoAgg = this.formGroup.value;
    if (this.internoControl == false) {
      this.internoInt = 0;
    }
    else {
      this.internoInt = 1;
    }
    this.corsoAgg.interno = this.internoInt;
    if (this.corsoAgg) {
      console.log("Corso: ", this.corsoAgg);
      if(this.tipoCheck){
        this.corsoAgg.tipo = this.formGroup.value.tipo.value.element;
      }
      else{
        this.corsoAgg.tipo = this.formGroup.value.tipo;
      }
      if(this.tipologiaCheck){
        this.corsoAgg.tipologia = this.formGroup.value.tipologia.value.element;
      }
      else{
        this.corsoAgg.tipologia = this.formGroup.value.tipologia;
      }
      if(this.misuraCheck){
        this.corsoAgg.misura = this.formGroup.value.misura.value.element;
      }
      else{
        this.corsoAgg.misura = this.formGroup.value.misura;
      }
      this.corsoAgg.ente = this.formGroup.value.ente;
      this.corsoAgg.note = this.formGroup.value.note;
      console.log("Coco: " , this.corsoAgg);
      this.courseService.update(this.corsoAgg).subscribe(() => {
        this.router.navigate(["/corsi"]);
      });
    }
  }

  conferma2(): void {
    this.docentiAgg = this.formGroupDocenti.value;
    if (this.internoControl == false) {
      this.internoInt = 0;
    }
    else {
      this.internoInt = 1;
    }
    this.docentiAgg.interno = this.internoInt;
    this.docentiAgg.docente = this.formGroupDocenti.value.docente
    if (this.docentiAgg) {
      /* this.corsoAgg.tipo = this.corsoAgg.tipo.value.element;
      this.corsoAgg.tipologia = this.corsoAgg.tipologia.value.element;
      this.corsoAgg.misura = this.corsoAgg.misura.value.element; */
      this.courseService.update(this.docentiAgg).subscribe(() => {
        console.log("DOC: " , this.docentiAgg);
        this.router.navigate(["/corsi"]);
      });
    }
  }

  cambiaTipo(){
    this.tipoCheck = true;
  }

  cambiaTipologia(){
    this.tipologiaCheck = true;
  }

  cambiaMisura(){
    this.misuraCheck = true;
  }

  getMisura() {
    this.descrizioneMis = [{ label: "Select Measure", value: null }];
    this.courseService.getMisura().subscribe((resp) => {
      this.listaMisure = resp.response;
      if (this.listaMisure) {
        this.listaMisure.forEach((element) => {
          this.descrizioneMis.push({
            label: element.descrizione,
            value: { element },
          });
        });
      }
    });
  }

  getTipo() {
    this.descrizioneTipo = [{ label: "Select Type", value: null }];
    this.courseService.getTipo().subscribe((resp) => {
      this.listaTipi = resp.response;
      if (this.listaTipi) {
        this.listaTipi.forEach((element) => {
          this.descrizioneTipo.push({
            label: element.descrizione,
            value: { element },
          });
        });
      }
    });
  }

  getTipologia() {
    this.descrizioneTipologia = [{ label: "Select Typology", value: null }];
    this.courseService.getTipologia().subscribe((resp) => {
      this.listaTipologie = resp.response;
      if (this.listaTipologie) {
        this.listaTipologie.forEach((element) => {
          this.descrizioneTipologia.push({
            label: element.descrizione,
            value: { element },
          });
        });
      }
    });
  }

  metodoInterno(){
    if(!this.internoControl1){
      this.internoInt1 = 0;
      this.scritta1 = "External";
    }
    else{
      this.internoInt1 = 1;
      this.scritta1 = "Internal";
    }
  }

  metodoErogato(){
    if(!this.erogatoControl){
      this.erogatoInt = 0;
      this.scritta2 = "Expected";
    }
    else{
      this.erogatoInt = 1;
      this.scritta2 = "Erogated";
    }
  }

  getDocenti() {
    if (this.internoControl) {
      console.log("Control3 " , this.internoControl);
      this.scritta = "Internal";
      this.listaDocenti = null;
      this.descrizioneDocente = null;
      this.descrizioneDipendente = [{ label: "Select Teacher", value: null }];
      this.employeeService.getAll().subscribe((resp) => {
        this.listaDipendenti = resp.response;
        if (this.listaDipendenti) {
          this.listaDipendenti.forEach((element) => {
            this.descrizioneDipendente.push({
              label: element.cognome + " " + element.nome,
              value: { element },
            });
          });
        }
      });
    }
    else {
      console.log("Control3 " , this.internoControl);
      this.scritta = "External";
      this.listaDipendenti = null;
      this.descrizioneDipendente = null;
      this.descrizioneDocente = [{ label: "Select Teacher", value: null }];
      this.teacherService.getAll().subscribe((resp) => {
        this.listaDocenti = resp.response;
        if (this.listaDocenti) {
          this.listaDocenti.forEach((element) => {
            this.descrizioneDocente.push({
              label: element.titolo + " " + element.ragioneSociale,
              value: { element },
            });
          });
        }
      });
    }
  }

  getDipendenti() {
    this.descrizioneDipendente1 = [{ label: "Seleziona Alunni", value: null }];
    this.employeeService.getAll().subscribe((resp) => {
      this.listaDipendenti1 = resp.response;
      if (this.listaDipendenti1) {
        this.listaDipendenti1.forEach((element) => {
          this.descrizioneDipendente1.push({
            label: element.cognome + " " + element.nome,
            value: { element },
          });
        });
      }
    });
  }

  getDocenteCorso() {
    this.courseUsersService.getAllCourseTeachers().subscribe((resp) => {
      this.listaDocentiCorsi = resp.response;
      if (this.listaDocentiCorsi) {
        this.listaDocentiCorsi.forEach(element => {
          if (element.corso.corsoId == this.route.snapshot.params["id"]) {
            this.idDocente = element.docente;
            this.docenteInterno = element.interno;
          }
        });
      }
      if (this.docenteInterno == 1) {
        this.employeeService.getAll().subscribe((resp) => {
          this.listaTuttiAlunni = resp.response;
          if (this.listaTuttiAlunni) {
            this.listaTuttiAlunni.forEach(element => {
              if (element.dipendenteId == this.idDocente) {
                this.docenteCorso = element;
              }
            });
          }
        });
      }
      else {
        this.teacherService.getAll().subscribe((resp) => {
          this.listaTuttiDocenti = resp.response;
          if (this.listaTuttiDocenti) {
            this.listaTuttiDocenti.forEach(element => {
              if (element.docenteId == this.idDocente) {
                this.docenteCorso = element
              }
            });
          }
        });
      }
    })

  };


  getAlunniCorso() {
    this.alunniCorso = [{ cognome: "Seleziona Alunni", nome: "Si", matricola: "Si", dipendente: { dipendenteId: 0 }, value: 0 }];
    this.alunniNonCorso = [{ cognome: "Seleziona Alunni", nome: "Si", matricola: "Si", value: 0 }];
    this.courseUsersService.getAllCourseUsers().subscribe((resp) => {
      this.listaAlunniCorsi = resp.response;
      if (this.listaAlunniCorsi) {
        this.listaAlunniCorsi.forEach(element => {
          if (element.corso.corsoId == this.route.snapshot.params["id"]) {
            this.alunniCorso.push({
              cognome: element.dipendente.cognome,
              nome: element.dipendente.nome,
              matricola: element.dipendente.matricola,
              value: { element }
            });
          }
        });
        this.alunniCorso.splice(0, 1);
        if (this.alunniCorso) {
          /*  this.doppione = false;
           console.log("HHH ", this.alunniCorso);
           this.alunniCorso.forEach(element => {
             if (element.dipendente.dipendenteId == this.alunniCorso){
 
             }
           }); */
          let i = 0;
          this.listaDipendenti1.forEach(element => {
            for (i = 0; i < this.listaDipendenti1.length - 1; i++) {
              if (this.listaAlunniCorsi[i].corso.corsoId == this.route.snapshot.params["id"] && element.dipendenteId != this.listaAlunniCorsi[i].dipendente) {
                this.alunniNonCorso.push({
                  cognome: element.cognome,
                  nome: element.nome,
                  matricola: element.matricola,
                  value: { element }
                });
              }
            }

          });
          this.alunniNonCorso.splice(0, 1);
          let cont = this.alunniNonCorso.length - this.alunniCorso.length;
          for (let j = 0; j < this.alunniNonCorso.length; j++) {
            for (let index = 0; index < this.alunniCorso.length; index++) {
              if (this.alunniNonCorso[j].matricola == this.alunniCorso[index].matricola) {
                this.alunniNonCorso.splice(j, cont);
                j - 1;
              }

            }

          }
        }
      }
    });
  }

  showBasicDialog() {
    this.displayBasic = true;
    console.log("Console " , this.displayBasic);
}

  confirm1(val: any) {
    this.confirmationService.confirm({
      message: "Sei sicuro di voler eliminare l'alunno?",
      header: "Conferma",
      icon: "pi pi-exclamation-triangle",
      key: "conferma",
      accept: () => {
        this.msgs = [
          {
            severity: "success",
            summary: "Confermato",
            detail: "Eliminazione Completata",
          },
        ];
        this.deleteAlunno(val);
        this.confirmationService.close();
      },
      reject: () => {
        this.msgs = [
          {
            severity: "info",
            summary: "Annullato",
            detail: "Operazione Annullata",
          },
        ];
        this.confirmationService.close();
      },
    });
  }



  deleteAlunno(val: any) {
    this.courseUsersService.deleteByIdUsers(val).subscribe(() => {
      this.getAlunniCorso();
      if(this.alunniCorso.length==0){
        this.alunniCorso = [{ cognome: "Nessun alunno nel corso", nome: null, matricola: null, value: { element: { dipendente: { dipendenteId: 0 } } } }];
        this.getAlunniCorso();        
      }
    });
  }

  selezionaAlunno(val: any) {
    console.log(this.formGroupDocenti.value);
    console.log(this.contatore1);
    let verifica: boolean = false;
    this.employeeService.getAll().subscribe((resp) => {
      resp.response.forEach(element => {
        if (element.dipendenteId == val) {
          this.dipendenteSel = element;
          if (this.dipendentiSelezionati) {
            for (let i = 0; i < this.dipendentiSelezionati.length; i++) {
              if (this.dipendentiSelezionati[i].dipendente.dipendenteId == this.dipendenteSel.dipendenteId) {
                verifica = true;
              }
            }
            if (verifica == false) {
              this.dipendentiSelezionati.push({
                corso: this.formGroupDocenti.value.corso,
                dipendente: this.dipendenteSel,
                durata: this.durataN
              });
            }
          }

        }
      });
    })
  }

  confermaDocente() {
    if (this.formGroupDocenti.value.docente != null) {
      console.log("DOCENTE: " , this.formGroupDocenti.value);
      this.docentiAgg = this.formGroupDocenti.value;
      if (this.docentiAgg) {
        this.docentiAgg.corso = this.formGroupDocenti.value.corso;
        if (this.internoControl) {
          this.internoInt = 1;
          this.docentiAgg.docente = this.docentiAgg.docente.value.element.dipendenteId;
        }
        else {
          this.internoInt = 0;
          this.docentiAgg.docente = this.docentiAgg.docente.value.element.docenteId;
        }
        this.docentiAgg.interno = this.internoInt;
        let idOBJ = Object.assign({}, this.docentiAgg);
        this.courseUsersService.updateTeachers(this.docentiAgg).subscribe(() => {
          this.router.navigate(['/corsi']);
        });

      }
    }
    else {
      this.router.navigate(['/corsi']);
    }
  }
  confermaAlunni() {
    let verifica = false;
    let verifica2 = false;
    if (!this.verifica1) {
      this.dipendentiSelezionati.splice(0, 1);
      this.verifica1 = true;
    }
    if (this.alunniCorso.length == 0) {
      this.boolean = true;
      this.alunniCorso = [{ cognome: "Seleziona Alunni", nome: "Si", matricola: "Si", value: { element: { dipendente: { dipendenteId: 0 } } } }];
    }
    this.dipendentiSelezionati.forEach(element => {
      for (let i = 0; i < this.alunniCorso.length; i++) {
        if (element.dipendente.dipendenteId == this.alunniCorso[i].value.element.dipendente.dipendenteId) {
          verifica = true;
        }
        if(this.contatore1==0 && element.dipendente.dipendenteId == this.docenteCorso.dipendenteId){
          verifica2 = true;
        }
        if(this.contatore1>0 && element.dipendente.dipendenteId == this.formGroupDocenti.value.docente.value.element.dipendenteId){
          verifica2 = true;
        }
      }
      if (!verifica && !verifica2) {
        this.courseUsersService.addUsers(element).subscribe(() => {
        });
      }
      verifica = false;
      verifica2 = false;
    });
    /* this.getAlunniCorso(); */
    if (this.boolean && this.dipendentiSelezionati.length > 0) {
      setTimeout(() => {
        this.alunniCorso.splice(0, 1);
        this.boolean = false;
      }, 300);

    }
    setTimeout(() => { this.getAlunniCorso(); }, 300);
  }

  contatore(){
    this.contatore1 ++;
    console.log(this.contatore1);
  }

}
