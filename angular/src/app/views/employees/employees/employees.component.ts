import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../../core/services/api.service";
import { EmployeesServices } from "../../../core/services/employees.services";
import { FormGroup, FormBuilder } from "@angular/forms";
import { paix } from "paix";
import { CourseUsersService } from "../../../core/services/course_users.service";

@Component({
  selector: "app-employees",
  templateUrl: "./employees.component.html",
  styleUrls: ["./employees.component.css"],
})
export class EmployeesComponent implements OnInit {
  public pagina: any = 1;

  public filtroSelezionato: any;

  public visibleSidebartop: boolean;

  public tipo2: any[] = [];

  public dati: any = 8;

  public nPagine: any = 0;

  public formGroup: FormGroup;
  public formGroup2: FormGroup;

  public lista: any[] = [];

  public lista2: any[] = [];

  public filt: any[] = [];

  public visibleSidebar5: boolean;

  public utCorr: any;

  public listaAllCourses: any[] = [];

  constructor(
    public api: ApiService,
    public employeesService: EmployeesServices,
    public fb: FormBuilder,
    public courseUserServices: CourseUsersService
  ) {
    this.tipo2 = [
      { name: "employees Id", code: "dipendenteId" },
      { name: "freshman", code: "matricola" },
      { name: "surname", code: "cognome" },
      { name: "first name", code: "nome" },
      { name: "gender", code: "sesso" },
      { name: "date of birth", code: "dataNascita" },
      { name: "birth place", code: "luogoNascita" },
      { name: "marital status", code: "statoCivile" },
      { name: "educational qualification", code: "titoloStudio" },
      { name: "achieved at", code: "conseguitoPresso" },
      { name: "Year Graduated", code: "annoConseguimento" },
      { name: "Employee type", code: "tipoDipendente" },
      { name: "qualification", code: "qualifica" },
      { name: "level", code: "livello" },
      { name: "assumption date", code: "dataAssunzione" },
      { name: "area manager", code: "responsabileRisorsa" },
      { name: "Resource manager", code: "responsabileArea" },
      { name: "End of relationship date", code: "dataFineRapporto" },
      { name: "Contract Expiration date", code: "dataScadenzaContratto" },
    ];
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      soggFiltrato: [""],
    });

    this.formGroup2 = this.fb.group({
      dipendenteId: [""],
      matricola: [""],
      cognome: [""],
      nome: [""],
      sesso: [""],
      dataNascita: [""],
      luogoNascita: [""],
      statoCivile: [""],
      titoloStudio: [""],
      conseguitoPresso: [""],
      annoConseguimento: [""],
      tipoDipendente: [""],
      qualifica: [""],
      livello: [""],
      dataAssunzione: [""],
      responsabileRisorsa: [""],
      responsabileArea: [""],
      dataFineRapporto: [""],
      dataScadenzaContratto: [""],
    });

    this.employeesService.getAll().subscribe((resp) => {
      this.lista2 = resp.response;
    });

    this.loadAll();
  }

  loadAll() {
    this.lista= [];
    this.employeesService
      .getAllPage(this.pagina, this.dati)
      .subscribe((resp) => {
        this.lista = resp.response;
      });
  }

  show(valore: any) {
    this.visibleSidebar5 = true;
    this.showCorses(valore);
    this.utCorr = valore;
  }

  filtra() {
    let res = { nome: this.formGroup.value.soggFiltrato };
    let replacement = { nome: this.filtroSelezionato.code };
    let modificato = paix(res, replacement);
    this.employeesService
      .getById(modificato, this.pagina, this.dati)
      .subscribe((resp) => {
        this.lista = resp.response;
        this.lista2 = resp.response;
      });
  }

  avanti() {
    this.nPagine = this.lista2.length / this.dati;

    if (this.pagina < this.nPagine) this.pagina++;

    if (this.filtroSelezionato == null) {
      this.loadAll();
    } else {
      this.filtra();
    }
  }

  dietro() {
    if (this.pagina - 1 > 0) {
      this.pagina--;

      if (this.filtroSelezionato == null) {
        this.loadAll();
      } else {
        this.filtra();
      }
    }
  }

  removeFilter() {
    this.loadAll();
    this.filtroSelezionato = null;
    this.formGroup = this.fb.group({
      soggFiltrato: [""],
    });
    this.formGroup2 = this.fb.group({
      dipendenteId: [""],
      matricola: [""],
      cognome: [""],
      nome: [""],
      sesso: [""],
      dataNascita: [""],
      luogoNascita: [""],
      statoCivile: [""],
      titoloStudio: [""],
      conseguitoPresso: [""],
      annoConseguimento: [""],
      tipoDipendente: [""],
      qualifica: [""],
      livello: [""],
      dataAssunzione: [""],
      responsabileRisorsa: [""],
      responsabileArea: [""],
      dataFineRapporto: [""],
      dataScadenzaContratto: [""],
    });
  }

  filtraAvanzato() {
    this.visibleSidebartop = true;
  }

  filtraA() {
    var primo = [];
    var secondo = [];
    for (const field in this.formGroup2.controls) {
      if (this.formGroup2.controls[field].value != "") {
        primo.push(field);
        secondo.push(this.formGroup2.controls[field].value);
      }
    }

    var finale = paix(secondo, primo);

    let newFinale = Object.assign({}, finale);

    this.employeesService
      .getById(newFinale, this.pagina, this.dati)
      .subscribe((resp) => {
        this.lista = resp.response;
        this.lista2 = resp.response;
      });
    this.visibleSidebartop = false;
  }

  showCorses(valore: any) {
    this.listaAllCourses = [];
    this.courseUserServices.getAllCourseUsers().subscribe((element) => {
      console.log(element.response);
      for (let i = 0; i < element.response.length; i++)
        if (element.response[i].dipendente.dipendenteId == valore.dipendenteId)
          this.listaAllCourses.push(element.response[i].corso);
    });
    console.log(this.listaAllCourses);
  }
}
