import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { TeachersService } from "../../../core/services/teachers.service";
import { ApiService } from "../../../core/services/api.service";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";
import { paix } from "paix";
import { CourseService } from "../../../core/services/course.service";

import { ConfirmationService } from "primeng/api";
import { Message } from "primeng/api";
import { CourseUsersService } from "../../../core/services/course_users.service";
import { element } from "protractor";
import { elementEventFullName } from "@angular/compiler/src/view_compiler/view_compiler";
import { EmployeesServices } from "../../../core/services/employees.services";

@Component({
  selector: "app-teachers",
  templateUrl: "./teachers.component.html",
  styleUrls: ["./teachers.component.css"],
  providers: [ConfirmationService],
})
export class TeachersComponent implements OnInit {
  public filtroSelezionato: any;

  public tipo: any[] = [];

  public corsopartecipante: any[];

  public pagina: any = 1;

  public dati: any = 8;

  public nPagine: any = 0;

  public visibleSidebartop: boolean = false;

  public filt: any[] = [];

  public formGroup: FormGroup;
  public formGroup2: FormGroup;

  public visibleSidebar5: boolean;

  public courseService: CourseService;

  msgs: Message[] = [];

  public utCorr: any;

  public lista: any[] = [];
  public lista2: any[] = [];

  public listaAllCourses: any[] = [];

  public listaAllCourseUser: any[] = [];

  public listaDocentiInterni: any[] = [];

  public allemploy: any[] = [];

  public listaDocentiInterni2: any[] = [];

  public visibleSidebarInterno: boolean = false;

  public tipo2Int: any[] = [];

  public filtroSelezionatoInt: any;

  public formGroupInt: FormGroup;

  public risposta: any []= [];

  constructor(
    public api: ApiService,
    public teachersService: TeachersService,
    public router: Router,
    private confirmationService: ConfirmationService,
    public fb: FormBuilder,
    public courseUserServices: CourseUsersService,
    public corseUserService: CourseUsersService,
    public employeesService: EmployeesServices
  ) {
    this.tipo2Int = [
      { name: "employees Id", code: "dipendenteId" },
      { name: "freshman", code: "matricola" },
      { name: "surname", code: "cognome" },
      { name: "first name", code: "nome" },
      { name: "gender", code: "sesso" },
      { name: "date of birth", code: "dataNascita" },
      { name: "birth place", code: "luogoNascita" },
      { name: "marital status", code: "statoCivile" },
      { name: "educational qualification", code: "titoloStudio" },
      { name: "achieved at", code: "conseguitoPresso" },
      { name: "Year Graduated", code: "annoConseguimento" },
      { name: "Employee type", code: "tipoDipendente" },
      { name: "qualification", code: "qualifica" },
      { name: "level", code: "livello" },
      { name: "assumption date", code: "dataAssunzione" },
      { name: "area manager", code: "responsabileRisorsa" },
      { name: "Resource manager", code: "responsabileArea" },
      { name: "End of relationship date", code: "dataFineRapporto" },
      { name: "Contract Expiration date", code: "dataScadenzaContratto" },
    ];

    this.tipo = [
      { name: "id", code: "docenteId" },
      { name: "title", code: "titolo" },
      { name: "business name", code: "ragioneSociale" },
      { name: "address", code: "indirizzo" },
      { name: "resort", code: "localita" },
      { name: "province", code: "provincia" },
      { name: "nation", code: "nazione" },
      { name: "phone number", code: "telefono" },
      { name: "fax", code: "fax" },
      { name: "partita iva", code: "partitaIva" },
      { name: "referent", code: "referente" },
    ];
  }

  ngOnInit(): void {

    this.formGroupInt = this.fb.group({
      soggFiltrato: [""],
    });

    this.formGroup2 = this.fb.group({
      docenteId: [""],
      titolo: [""],
      ragioneSociale: [""],
      indirizzo: [""],
      localita: [""],
      provincia: [""],
      nazione: [""],
      telefono: [""],
      fax: [""],
      partitaIva: [""],
      referente: [""],
    });

    this.formGroup = this.fb.group({
      soggFiltrato: [""],
    });

    this.teachersService.getAll().subscribe((resp) => {
      this.lista2 = resp.response;
    });

    this.loadAll();
    this.showAllEmployeesTeacher();
  }

  confirm1(val: any) {
    this.confirmationService.confirm({
      message: "Sei sicuro di voler eliminare il docente?",
      header: "Conferma",
      icon: "pi pi-exclamation-triangle",
      key: "conferma",
      accept: () => {
        this.msgs = [
          {
            severity: "success",
            summary: "Confermato",
            detail: "Eliminazione Completata",
          },
        ];
        this.delete(val);
        this.confirmationService.close();
      },
      reject: () => {
        this.msgs = [
          {
            severity: "info",
            summary: "Annullato",
            detail: "Operazione Annullata",
          },
        ];
        this.confirmationService.close();
      },
    });
  }

/*   loadAll() {
    this.teachersService
      .getAllPage(this.pagina, this.dati)
      .subscribe((resp) => {
        this.lista = resp.response;
      });
  } */

  loadAll() {
    this.teachersService.getAllPage(this.pagina, this.dati).subscribe((resp) => {
      this.lista= [];
      this.risposta = resp.response;
      console.log("Risposta: " , this.risposta);
      if(this.risposta){
        this.risposta.forEach((element) => {
          this.lista.unshift({
            titolo: element.titolo,
            ragioneSociale: element.ragioneSociale,
            provincia: element.provincia,
            docenteId: element.docenteId,
            value: { element },
          });
        });
      }
    });
  }

  show(valore: any) {
    this.visibleSidebar5 = true;

    this.utCorr = valore;

    this.showCorses(valore);
  }

  new() {
    this.router.navigate(["/new"]);
  }

  delete(val: any) {
    this.teachersService.deleteById(val).subscribe(() => {
      if (this.filtroSelezionato == null) {
        this.loadAll();
      } else {
        this.filtra();
      }
    });
  }

  edit(id: any) {
    this.router.navigate(["/edit/" + id]);
  }

  filtra() {
    let res = { nome: this.formGroup.value.soggFiltrato };
    let replacement = { nome: this.filtroSelezionato.code };
    let modificato = paix(res, replacement);

    this.teachersService
      .getById(modificato, this.pagina, this.dati)
      .subscribe((resp) => {
        this.lista = resp.response;
        this.lista2 = resp.response;
      });
  }

  avanti() {
    this.nPagine = this.lista2.length / this.dati;

    if (this.pagina < this.nPagine) this.pagina++;

    if (this.filtroSelezionato == null) {
      this.loadAll();
    } else {
      this.filtra();
    }
  }

  dietro() {
    if (this.pagina - 1 > 0) {
      this.pagina--;

      if (this.filtroSelezionato == null) {
        this.loadAll();
      } else {
        this.filtra();
      }
    }
  }
  removeFilter() {
    this.loadAll();
    this.filtroSelezionato = null;
    this.formGroup = this.fb.group({
      soggFiltrato: [""],
    });
    this.formGroup2 = this.fb.group({
      docenteId: [""],
      titolo: [""],
      ragioneSociale: [""],
      indirizzo: [""],
      localita: [""],
      provincia: [""],
      nazione: [""],
      telefono: [""],
      fax: [""],
      partitaIva: [""],
      referente: [""],
    });
  }

  filtraAvanzato() {
    this.visibleSidebartop = true;
  }

  filtraA() {
    var primo = [];
    var secondo = [];
    for (const field in this.formGroup2.controls) {
      if (this.formGroup2.controls[field].value != "") {
        primo.push(field);
        secondo.push(this.formGroup2.controls[field].value);
      }
    }

    var finale = paix(secondo, primo);

    let newFinale = Object.assign({}, finale);

    this.teachersService
      .getById(newFinale, this.pagina, this.dati)
      .subscribe((resp) => {
        this.lista = resp.response;
        this.lista2 = resp.response;
      });
    this.visibleSidebartop = false;
  }

  showCorses(valore: any) {
    this.listaAllCourses = [];
    this.courseUserServices.getAllCourseTeachers().subscribe((element) => {
      for (let i = 0; i < element.response.length; i++)
        if (
          element.response[i].docente == valore.docenteId &&
          element.response[i].interno == 0
        )
          this.listaAllCourses.push(element.response[i].corso);
    });
  }

  showAllEmployeesTeacher() {
    this.corseUserService.getAllCourseTeachers().subscribe((element) => {
      this.employeesService.getAll().subscribe((element2) => {
        for (let i = 0; i < element2.response.length; i++) {
          this.allemploy.push(element2.response[i]);
        }

        for (let i = 0; i < element.response.length; i++) {
          if (element.response[i].interno == 1) {
            this.listaAllCourseUser.push(element.response[i]);
          }
        }

        for (let i = 0; i < this.allemploy.length; i++) {
          for (let k = 0; k < this.listaAllCourseUser.length; k++) {
            if (
              this.listaAllCourseUser[k].docente ==
              this.allemploy[i].dipendenteId
            )
              this.listaDocentiInterni.push(this.allemploy[i]);
          }
        }

        let flag = false;

        for (let i = 0; i < this.listaDocentiInterni.length; i++) {
          flag = false;
          for (let k = 0; k < this.listaDocentiInterni.length; k++) {
            if (
              this.listaDocentiInterni[i].dipendenteId ==
              this.listaDocentiInterni2[k]?.dipendenteId
            )
              flag = true;
          }
          if (!flag) {
            this.listaDocentiInterni2.push(this.listaDocentiInterni[i]);
          }
        }
      });
    });

    console.log(this.listaDocentiInterni2);
  }

  showCorsesint(valore: any) {
    this.listaAllCourses = [];
    this.courseUserServices.getAllCourseTeachers().subscribe((element) => {
      for (let i = 0; i < element.response.length; i++)
        if (
          element.response[i].docente == valore.dipendenteId &&
          element.response[i].interno == 1
        )
          this.listaAllCourses.push(element.response[i].corso);
    });
  }

  showInterno(valore: any) {
    this.utCorr = [];
    this.visibleSidebarInterno = true;
    console.log(valore);
    this.showCorsesint(valore);
    this.utCorr = valore;
  }

  removeFilterInt(){
    this.formGroupInt = this.fb.group({
      soggFiltrato: [""],
    });

    this.showAllEmployeesTeacher();
  }

  filtraInt(){
    let res = { nome: this.formGroupInt.value.soggFiltrato };
    let replacement = { nome: this.filtroSelezionatoInt.code };
    let modificato = paix(res, replacement);
    this.employeesService
      .getById(modificato, this.pagina, this.dati)
      .subscribe((resp) => {
        this.listaDocentiInterni2 = resp.response;
      });
  }
}
