import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditTeacherComponent } from './edit-teacher/edit-teacher.component';


const routes: Routes = [
  {
    path: '',
    component: EditTeacherComponent,
    data: {
      title: 'edit'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditTeacherRoutingModule { }
