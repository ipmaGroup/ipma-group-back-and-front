import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewCourseRoutingModule } from './new-course-routing.module';
import { NewCourseComponent } from './new-course.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {DropdownModule} from 'primeng/dropdown';
import {CalendarModule} from 'primeng/calendar';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {SpinnerModule} from 'primeng/spinner';
import {InputSwitchModule} from 'primeng/inputswitch';


@NgModule({
  declarations: [NewCourseComponent],
  imports: [
    CommonModule,
    NewCourseRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule,
    CalendarModule,
    InputTextareaModule,
    SpinnerModule,
    InputSwitchModule
  ]
})
export class NewCourseModule { }
