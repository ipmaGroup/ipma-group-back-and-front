import { Component, OnInit } from '@angular/core';
import { CourseService } from '../../../core/services/course.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-course',
  templateUrl: './new-course.component.html',
  styleUrls: ['./new-course.component.css']
})
export class NewCourseComponent implements OnInit {
  public formGroup: FormGroup;
  public corsoAgg: any;
  public descrizioneMis: any[];
  public descrizioneTipo: any[];
  public descrizioneTipologia: any[];
  public listaMisure: any[];
  public listaTipi: any[];
  public listaTipologie: any[];

  public scritta1: any = "External";
  public scritta2: any = "Expected";
  public internoControl1: boolean = false;
  public erogatoControl: boolean = false;
  public internoInt1: any;

  constructor(public fb: FormBuilder, public courseService: CourseService, public router: Router) { }

  ngOnInit(): void {

    this.getMisura();
    this.getTipo();
    this.getTipologia();
    this.metodoInterno();
    this.metodoErogato();

  
    this.formGroup = this.fb.group({
      tipo: [''],
      tipologia: [''],
      descrizione: [''],
      edizione: [''],
      previsto: [''],
      erogato: [''],
      durata: [''],
      misura: [''],
      note: [''],
      luogo: [''],
      ente: [''],
      dataErogazione: [''],
      dataChiusura: [''],
      dataCensimento: [''],
      interno: [''],
      tipoId: [''],
      tipologiaId: [''],
      misuraId: ['']
    });
  }



  conferma(): void {
    if(this.erogatoControl){
      this.formGroup.value.erogato = 1;
      this.formGroup.value.previsto = 0;
    }
    else{
      this.formGroup.value.erogato = 0;
      this.formGroup.value.previsto = 1;
    }
    this.formGroup.value.interno = this.internoInt1;
    this.formGroup.value.tipo = this.formGroup.value.tipo.value.element;
    this.formGroup.value.tipologia = this.formGroup.value.tipologia.value.element;
    this.formGroup.value.misura = this.formGroup.value.misura.value.element;
    console.log(" fd " ,this.formGroup.value);
    this.corsoAgg = this.formGroup.value;
    if(this.corsoAgg){
      console.log("B ",this.corsoAgg)
    this.corsoAgg.tipo = this.corsoAgg.tipo;
    this.corsoAgg.tipologia = this.corsoAgg.tipologia;
    this.corsoAgg.misura = this.corsoAgg.misura;
    this.courseService.add(this.corsoAgg).subscribe(() => {
      this.router.navigate(['/new-course-users']);
    });
    console.log(this.corsoAgg);
  }
}

getMisura(){
  this.descrizioneMis = [
    {label:'Select Measure', value:null},
];
this.courseService.getMisura().subscribe((resp)=>{
  this.listaMisure = resp.response;
  if(this.listaMisure){
    this.listaMisure.forEach(element => {
      this.descrizioneMis.push({label: element.descrizione, value:{element}});
    });
  }
});
}

getTipo(){
  this.descrizioneTipo = [
    {label:'Select Type', value:null},
];
this.courseService.getTipo().subscribe((resp)=>{
  this.listaTipi = resp.response;
  if(this.listaTipi){
    this.listaTipi.forEach(element => {
      this.descrizioneTipo.push({label: element.descrizione, value:{element}});
    });
  }
});
}

getTipologia(){
  this.descrizioneTipologia = [
    {label:'Select Typology', value:null},
];
this.courseService.getTipologia().subscribe((resp)=>{
  this.listaTipologie = resp.response;
  if(this.listaTipologie){
    this.listaTipologie.forEach(element => {
      this.descrizioneTipologia.push({label: element.descrizione, value:{element}});
    });
  }
});
}

metodoInterno(){
  if(!this.internoControl1){
    this.internoInt1 = 0;
    this.scritta1 = "External";
  }
  else{
    this.internoInt1 = 1;
    this.scritta1 = "Internal";
  }
}

metodoErogato(){
  if(!this.erogatoControl){
    this.scritta2 = "Expected";
  }
  else{
    this.scritta2 = "Erogated";
  }
}

}
