import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewCourseUsersRoutingModule } from './new-course-users-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NewCourseUsersRoutingModule
  ]
})
export class NewCourseUsersModule { }
