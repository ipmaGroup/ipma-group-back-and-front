import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewCourseUsersRoutingModule } from './new-course-users-routing.module';
import { NewCourseUsersComponent } from './new-course-users.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { InputTextareaModule } from 'primeng/inputtextarea';
import {TabViewModule} from 'primeng/tabview';
import {InputSwitchModule} from 'primeng/inputswitch';
import {ListboxModule} from 'primeng/listbox';
import {SpinnerModule} from 'primeng/spinner';


@NgModule({
  declarations: [NewCourseUsersComponent],
  imports: [
    CommonModule,
    NewCourseUsersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule,
    CalendarModule,
    InputTextareaModule,
    TabViewModule,
    InputSwitchModule,
    ListboxModule,
    SpinnerModule
  ]
})
export class NewCourseUsersModule { }
