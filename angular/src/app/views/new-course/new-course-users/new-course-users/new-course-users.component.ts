import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CourseService } from '../../../../core/services/course.service';
import { EmployeesServices } from '../../../../core/services/employees.services';
import { TeachersService } from '../../../../core/services/teachers.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CourseUsersService } from '../../../../core/services/course_users.service';

@Component({
  selector: 'app-new-course-users',
  templateUrl: './new-course-users.component.html',
  styleUrls: ['./new-course-users.component.css']
})
export class NewCourseUsersComponent implements OnInit {

  public formGroup: FormGroup;
  public formGroupDocenti: FormGroup;
  public formGroupDipendenti: FormGroup;
  public scritta: any = "External";
  public durataN: any;
  public verifica: boolean;
  public i: any = 0;

  public docentiAgg: any;
  public dipendentiAgg: any;

  public descrizioneMis: any[];
  public descrizioneTipo: any[];
  public descrizioneTipologia: any[];
  public descrizioneDipendente: any[];
  public descrizioneDocente: any[];
  public descrizioneDipendente1: any[];

  public listaCorsi: any[];
  public listaDipendenti: any[];
  public listaDocenti: any[];
  public listaDipendenti1: any[];

  public internoControl: boolean;
  public internoInt: any;
  public verificaDocente: boolean = false;

  public ultimoCorso: any;

  constructor(public fb: FormBuilder, public courseUsersService: CourseUsersService, public courseService: CourseService, public employeeService: EmployeesServices, public teacherService: TeachersService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit(): void {

    this.getDipendenti();
    this.getDocenti();
    this.getCorso();

    this.formGroupDocenti = this.fb.group({
      corso: [''],
      docente: [''],
      interno: [''],
    });

    this.formGroupDipendenti = this.fb.group({
      corso: [''],
      dipendente: [''],
      durata: [''],
    });


  }



  conferma(): void {
    this.docentiAgg = this.formGroupDocenti.value;
    console.log(this.docentiAgg);
    if (this.docentiAgg) {
      this.docentiAgg.corso = this.ultimoCorso;
      if (this.internoControl) {
        this.internoInt = 1;
        this.docentiAgg.docente = this.docentiAgg.docente.value.element.dipendenteId;
      }
      else {
        this.internoInt = 0;
        this.docentiAgg.docente = this.docentiAgg.docente.value.element.docenteId;
      }
      this.docentiAgg.interno = this.internoInt;
      let idOBJ = Object.assign({}, this.docentiAgg);
      this.courseUsersService.addTeachers(this.docentiAgg).subscribe(() => {
      });
    }
    this.dipendentiAgg = this.formGroupDipendenti.value;
    if (this.dipendentiAgg) {
      this.dipendentiAgg.dipendente.forEach(element => {
        this.dipendentiAgg.corso = this.ultimoCorso;
        this.dipendentiAgg.dipendente = element.value.element;
        this.dipendentiAgg.durata = this.durataN;
        this.courseUsersService.addUsers(this.dipendentiAgg).subscribe(() => {
          this.verifica = true;
          this.router.navigate(['/corsi']);
        });
      });
    }
  }

  getDocenti() {
    if (this.internoControl) {
      this.scritta = "Internal";
      this.internoInt = 1;
      this.listaDocenti = null;
      this.descrizioneDocente = null;
      this.descrizioneDipendente = [{ label: "Select Teacher", value: null }];
      this.employeeService.getAll().subscribe((resp) => {
        this.listaDipendenti = resp.response;
        if (this.listaDipendenti) {
          this.listaDipendenti.forEach((element) => {
            this.descrizioneDipendente.push({
              label: element.cognome + " " + element.nome,
              value: { element },
            });
          });
        }
      });
    }
    else {
      this.scritta = "External"
      this.internoInt = 0;
      this.listaDipendenti = null;
      this.descrizioneDipendente = null;
      this.descrizioneDocente = [{ label: "Select Teacher", value: null }];
      this.teacherService.getAll().subscribe((resp) => {
        this.listaDocenti = resp.response;
        if (this.listaDocenti) {
          this.listaDocenti.forEach((element) => {
            this.descrizioneDocente.push({
              label: element.titolo + " " + element.ragioneSociale,
              value: { element },
            });
          });
        }
      });
    }
  }

  getDipendenti() {
    this.descrizioneDipendente1 = [{ label: "Seleziona Alunni", value: null }];
    this.employeeService.getAll().subscribe((resp) => {
      this.listaDipendenti1 = resp.response;
      if (this.listaDipendenti1) {
        this.listaDipendenti1.forEach((element) => {
          this.descrizioneDipendente1.push({
            label: element.cognome + " " + element.nome,
            value: { element },
          });
        });
      }
      if (this.descrizioneDipendente1.length > 1) {
        this.descrizioneDipendente1.splice(0, 1);
      }
    });
  }

  getCorso() {
    this.courseService.getAll().subscribe((resp) => {
      this.listaCorsi = resp.response;
      if (this.listaCorsi) {
        this.ultimoCorso = this.listaCorsi[this.listaCorsi.length - 1]
      }
    });
  }

  togliDocente() {
    this.descrizioneDipendente1 = null;
    this.descrizioneDipendente1 = [{ label: "Seleziona Alunni", value: null }];
    if (this.internoControl == true) {
      if (this.listaDipendenti1) {
        this.listaDipendenti1.forEach((element) => {
          if (element.dipendenteId == this.formGroupDocenti.value.docente.value.element.dipendenteId) {
            this.verificaDocente = true;
          }
          if (this.verificaDocente == false) {
            this.descrizioneDipendente1.push({
              label: element.cognome + " " + element.nome,
              value: { element },
            });
          }
          else {
            this.verificaDocente = false;
          }
        });
      }
    }
    else {
      if (this.listaDipendenti1) {
        this.listaDipendenti1.forEach((element) => {
          this.descrizioneDipendente1.push({
            label: element.cognome + " " + element.nome,
            value: { element },
          });
        });
      }
    }

    if (this.descrizioneDipendente1.length > 1) {
      this.descrizioneDipendente1.splice(0, 1);
    }
  }

}
