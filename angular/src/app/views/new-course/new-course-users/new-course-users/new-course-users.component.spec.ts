import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCourseUsersComponent } from './new-course-users.component';

describe('NewCourseUsersComponent', () => {
  let component: NewCourseUsersComponent;
  let fixture: ComponentFixture<NewCourseUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCourseUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCourseUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
