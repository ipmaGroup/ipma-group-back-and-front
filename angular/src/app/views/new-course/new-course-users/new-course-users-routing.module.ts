import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewCourseUsersComponent } from './new-course-users/new-course-users.component';


const routes: Routes = [{
  path: '',
  component: NewCourseUsersComponent,
  data: {
    title: 'new-course-users'
  }
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewCourseUsersRoutingModule { }
