import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'corsi',
    pathMatch:'full'
  },


  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },

  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'teachers',
        loadChildren: () => import('./views/teachers/teachers.module').then(m => m.TeachersModule)
      },
      {
        path: 'new',
        loadChildren: () => import('./views/new-teacher/new-teacher.module').then(m => m.NewTeacherModule)
      },
      {
        path: 'edit/:id',
        loadChildren: () => import('./views/edit-teacher/edit-teacher.module').then(m => m.EditTeacherModule)
      },
      {
        path: 'corsi',
        loadChildren: () => import('./views/corsi/corsi.module').then(m => m.CorsiModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'employees',
        loadChildren: () => import('./views/employees/employees.module').then(m => m.EmployeesModule)
      },
      {
        path: 'new-course',
        loadChildren: () => import('./views/new-course/new-course.module').then(m => m.NewCourseModule)
      },
      {
        path: 'edit-course/:id',
        loadChildren: () => import('./views/edit-course/edit-course.module').then(m => m.EditCourseModule)
      },
      {
        path: 'new-course-users',
        loadChildren: () => import('./views/new-course/new-course-users/new-course-users.module').then(m => m.NewCourseUsersModule)
      },
      {
        path: 'valutazioni/:id/:idDocente/:interno',
        loadChildren: () => import('./views/valutazioni/valutazioni.module').then(m => m.ValutazioniModule)
      }
	  
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
