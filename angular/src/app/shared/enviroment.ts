
export const environment = {
    host: 'http://localhost:8090',
    endpoint: {
        teachersController: 'api/teachers',
        teachersControllerFilter: 'api/teachers/',
        employeesController: 'api/employees',
        employeesControllerFilter: 'api/employees/',
        courseController : 'api/corsi',
        misureController : 'api/misure',
        tipiController: 'api/tipi',
        tipologieController: 'api/tipologie',
        courseControllerFilter : 'api/corsi/',
        courseTeachersController : 'api/corsi-docenti',
        courseTeachersControllerFilter: 'api/corsi-docenti/filter',
        courseUsersController : 'api/corsi-utenti',
        courseUsersControllerFilter: 'api/corsi-utenti/filter',
        voteControl: 'api/rating/',
    }
}