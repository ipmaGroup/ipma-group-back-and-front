import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";
import { environment } from "../../shared/enviroment";

@Injectable({
  providedIn: "root",
})
export class CourseService {
  private readonly path = environment.endpoint.courseController;
  private readonly misure = environment.endpoint.misureController;
  private readonly tipi = environment.endpoint.tipiController;
  private readonly tipologie = environment.endpoint.tipologieController;
  private readonly pathFilter = environment.endpoint.courseControllerFilter;
  public listSoggetti: any[] = [];

  constructor(private api: ApiService) {}

  public getAll(): Observable<any> {
    return this.api.get(this.path);
  }
  public getAllPage(primo:any , secondo:any): Observable<any> {
    return this.api.getByPage(this.path , primo , secondo);
  }
  public getById(ut: any, inizio:any , fine:any): Observable<any> {
    return this.api.getById(this.pathFilter + inizio + "..." + fine , ut);
  }
  public add(item: any): Observable<any> {
    return this.api.post(this.path, item);
  }
  public deleteById(body: any): Observable<any> {
    return this.api.delete(this.path, body);
  }
  public update(soggetto: any): Observable<any> {
    return this.api.patch(this.path, soggetto);
  }


  public getMisura(): Observable<any> {
    return this.api.get(this.misure);
  }
  public getTipo(): Observable<any> {
    return this.api.get(this.tipi);
  }
  public getTipologia(): Observable<any> {
    return this.api.get(this.tipologie);
  }
}
