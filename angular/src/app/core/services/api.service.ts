import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../shared/enviroment";

@Injectable({
  providedIn: "root",
})
export class ApiService {
  private readonly host = environment.host;

  constructor(private http: HttpClient) {}

  public get(path: string): Observable<any> {
    return this.http.get<any>(this.host + "/" + path);
  }

  public getById(path: string, id: any) : Observable<any> {
    return this.http.post<any>(this.host + '/' + path , id);
  }

  public getByPage(path: string, primo:any , secondo:any) : Observable<any> {
    return this.http.get<any>(this.host + '/' + path + '/' + primo + "..." + secondo);
  }

  public post(path: string, body: any): Observable<any> {
    return this.http.post<any>(this.host + "/" + path, body);
  }

  public delete(path: string, body: any): Observable<any> {
    return this.http.delete<any>(this.host + "/" + path + "/" + body);
  }

  public patch(path: string, body: any): Observable<any> {
    return this.http.patch<any>(this.host + "/" + path, body);
  }
}
