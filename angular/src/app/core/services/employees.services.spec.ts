import { TestBed } from '@angular/core/testing';

import { EmployeesServices } from './employees.services';

describe('EmployeesServices', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmployeesServices = TestBed.get(EmployeesServices);
    expect(service).toBeTruthy();
  });
});
