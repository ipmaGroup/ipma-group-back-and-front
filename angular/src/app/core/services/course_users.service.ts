import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";
import { environment } from "../../shared/enviroment";

@Injectable({
  providedIn: "root",
})
export class CourseUsersService {
  private readonly teachersPath = environment.endpoint.courseTeachersController;
  private readonly usersPath = environment.endpoint.courseUsersController;
  private readonly teachersPathFilter = environment.endpoint.courseTeachersControllerFilter;
  private readonly usersPathFilter = environment.endpoint.courseUsersControllerFilter;
  public listSoggetti: any[] = [];

  constructor(private api: ApiService) { }

  public getAllCourseTeachers(): Observable<any> {
    return this.api.get(this.teachersPath);
  }
  public getAllCourseUsers(): Observable<any> {
    return this.api.get(this.usersPath);
  }

  public getAllPageTeachers(primo: any, secondo: any): Observable<any> {
    return this.api.getByPage(this.teachersPath, primo, secondo);
  }
  public getAllPageUsers(primo: any, secondo: any): Observable<any> {
    return this.api.getByPage(this.usersPath, primo, secondo);
  }

  public getByIdTeachers(ut: any, inizio: any, fine: any): Observable<any> {
    return this.api.getById(this.teachersPathFilter + inizio + "..." + fine, ut);
  }
  public getByIdUsers(ut: any, inizio: any, fine: any): Observable<any> {
    return this.api.getById(this.usersPathFilter + inizio + "..." + fine, ut);
  }

  public addTeachers(item: any): Observable<any> {
    return this.api.post(this.teachersPath, item);
  }
  public addUsers(item: any): Observable<any> {
    return this.api.post(this.usersPath, item);
  }

  public deleteByIdTeachers(body: any): Observable<any> {
    return this.api.delete(this.teachersPath, body);
  }
  public deleteByIdUsers(body: any): Observable<any> {
    return this.api.delete(this.usersPath, body);
  }

  public updateTeachers(soggetto: any): Observable<any> {
    return this.api.patch(this.teachersPath, soggetto);
  }
  public updateUsers(soggetto: any): Observable<any> {
    return this.api.patch(this.usersPath, soggetto);
  }

}
