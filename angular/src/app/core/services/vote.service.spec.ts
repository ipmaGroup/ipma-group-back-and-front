import { TestBed } from '@angular/core/testing';

import { VoteServices } from './vote.service';

describe('VoteServices', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VoteServices = TestBed.get(VoteServices);
    expect(service).toBeTruthy();
  });
});
