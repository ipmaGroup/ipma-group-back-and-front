import { TestBed } from '@angular/core/testing';

import { CourseUsersService } from './course_users.service';

describe('CourseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CourseUsersService = TestBed.get(CourseUsersService);
    expect(service).toBeTruthy();
  });
});
