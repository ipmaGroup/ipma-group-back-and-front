import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";
import { environment } from "../../shared/enviroment";

@Injectable({
  providedIn: "root",
})
export class VoteServices {
  private readonly path = environment.endpoint.voteControl;
  public listSoggetti: any[] = [];

  constructor(private api: ApiService) {}


  public getAllCourses(id:any , primo:any , secondo:any): Observable<any> {
    return this.api.getById(this.path + "courses/" + primo + "..." + secondo , id);
  }

  public addCourses(body : any): Observable<any>{
    return this.api.post(this.path + "courses/" ,body);
  }

  public getAllEmployees(id:any, primo:any , secondo:any): Observable<any> {
    return this.api.getById(this.path + "employees/" + primo + "..." + secondo , id);
  }

  public addEmployees(body : any): Observable<any>{
    return this.api.post(this.path + "employees/" ,body);
  }

  public getAllTeachers(id:any, primo:any , secondo:any): Observable<any> {
    return this.api.getById(this.path + "teachers/" + primo + "..." + secondo , id);
  }

  
  public addTeachers(body : any): Observable<any>{
    return this.api.post(this.path + "teachers/" ,body);
  }
 
}
