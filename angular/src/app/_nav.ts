import { INavData } from "@coreui/angular";

export const navItems: INavData[] = [
 
  {
    name:"Teachers",
    url:"/teachers",
    icon:"icon-user"
  },
  {
  name:"Courses",
  url:"/corsi",
  icon:"icon-graduation"

  },
  {
    name:"Employees",
    url:"/employees",
    icon:"icon-people"
  
    }
];
