CREATE DATABASE IF NOT EXISTS project_work;
USE project_work;

DROP TABLE IF EXISTS tipi;
CREATE TABLE tipi (
	tipo_id INTEGER AUTO_INCREMENT PRIMARY KEY,
	descrizione VARCHAR(50) NOT NULL
);

DROP TABLE IF EXISTS tipologie;
CREATE TABLE tipologie (
	tipologia_id INTEGER AUTO_INCREMENT PRIMARY KEY,
	descrizione VARCHAR(50) NOT NULL
);

DROP TABLE IF EXISTS misure;
CREATE TABLE misure (
	misura_id INTEGER AUTO_INCREMENT PRIMARY KEY,
	descrizione VARCHAR(50) NOT NULL
);

DROP TABLE IF EXISTS societa;
CREATE TABLE societa (
	societa_id INTEGER PRIMARY KEY AUTO_INCREMENT,
	ragione_sociale VARCHAR(60) NOT NULL,
	indirizzo VARCHAR(60),
	localita VARCHAR(60),
	provincia VARCHAR(60),
	nazione VARCHAR(60),
	telefono VARCHAR(60) NOT NULL,
	fax VARCHAR(60),
	partita_iva VARCHAR(11) NOT NULL
);

DROP TABLE IF EXISTS corsi;
CREATE TABLE corsi(
   corso_id INTEGER PRIMARY KEY AUTO_INCREMENT,
   tipo INT NOT NULL,
   tipologia INT NOT NULL,
   descrizione VARCHAR(200) NOT NULL,
   edizione VARCHAR(60) NOT NULL,
   previsto INT NOT NULL,
   erogato INT NOT NULL,
   durata INT NOT NULL,
	misura INT NOT NULL,
   note VARCHAR (60),
   luogo VARCHAR (60),
   ente VARCHAR (60),
   data_erogazione DATE,
   data_chiusura DATE,
   data_censimento DATE,
   interno INT,
   tipo_id INT,
   tipologia_id INT,
   misura_id INT,

   FOREIGN KEY (tipo) REFERENCES tipi(tipo_id) ON DELETE CASCADE,
   FOREIGN KEY (tipologia) REFERENCES tipologie(tipologia_id) ON DELETE CASCADE,
   FOREIGN KEY (misura) REFERENCES misure(misura_id) ON DELETE CASCADE
);

DROP TABLE IF EXISTS dipendenti;
CREATE TABLE dipendenti (
   dipendente_id INTEGER PRIMARY KEY AUTO_INCREMENT,
   cognome VARCHAR(70),
   nome VARCHAR(70),
   matricola INTEGER,
   societa INTEGER,
   sesso ENUM('maschio','femmina','preferisco non specificare'),
   data_nascita DATE,
   luogo_nascita VARCHAR(80),
   stato_civile VARCHAR(80),
   titolo_studio VARCHAR(100),
   conseguito_presso VARCHAR(80),
   anno_conseguimento INTEGER,
   tipo_dipendente VARCHAR(100),
   qualifica VARCHAR(100),
   livello VARCHAR(100),
   data_assunzione DATE,
   responsabile_risorsa INTEGER,
   responsabile_area INTEGER,
   data_fine_rapporto DATE,
   data_scadenza_contratto DATE,
    
   FOREIGN KEY (societa) REFERENCES societa(societa_id) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS docenti;
CREATE TABLE docenti(
	docente_id INTEGER PRIMARY KEY AUTO_INCREMENT,
	titolo VARCHAR(60),
	ragione_sociale VARCHAR(60),
	indirizzo VARCHAR(60),
	localita VARCHAR(60),
	provincia VARCHAR(60),
	nazione VARCHAR(60),
	telefono VARCHAR(60) NOT NULL,
	fax VARCHAR(60),
	partita_iva VARCHAR(11),
	referente VARCHAR(60)
);

DROP TABLE IF EXISTS corsi_utenti;
CREATE TABLE corsi_utenti(
	corsi_utenti_id INTEGER PRIMARY KEY AUTO_INCREMENT,
  	corso INT,
	dipendente INT,
 	durata VARCHAR(60),

  	FOREIGN KEY (corso) REFERENCES corsi(corso_id) ON DELETE CASCADE ON UPDATE CASCADE,
  	FOREIGN KEY (dipendente) REFERENCES dipendenti(dipendente_id) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS corsi_docenti;
CREATE TABLE corsi_docenti (
	corsi_docenti_id INT AUTO_INCREMENT PRIMARY KEY,
	corso INT NOT NULL,
	docente INT NOT NULL,
	interno INT NOT NULL,
	
	FOREIGN KEY (corso) REFERENCES corsi(corso_id) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS valutazioni_docenti;
CREATE TABLE valutazioni_docenti (
  valutazioni_docenti_id int(11) NOT NULL AUTO_INCREMENT,
  corso int(11) NOT NULL,
  docente int(11) NOT NULL,
  criterio varchar(60) DEFAULT NULL,
  interno int(11) NOT NULL,
  insufficente int(11) DEFAULT '0',
  sufficente int(11) DEFAULT '0',
  buono int(11) DEFAULT '0',
  molto_buono int(11) DEFAULT '0',
  non_applicabile int(11) DEFAULT '0',
  media double DEFAULT NULL,
  PRIMARY KEY (valutazioni_docenti_id),
  KEY corso (corso),
  KEY docente (docente),
  CONSTRAINT valutazioni_docenti_ibfk_1 FOREIGN KEY (corso) REFERENCES corsi (corso_id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT valutazioni_docenti_ibfk_2 FOREIGN KEY (docente) REFERENCES docenti (docente_id) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS valutazioni_corsi;
CREATE TABLE valutazioni_corsi (
  valutazioni_corsi_id int(11) NOT NULL AUTO_INCREMENT,
  corso int(11) NOT NULL,
  insufficente int(11) DEFAULT '0',
  sufficente int(11) DEFAULT '0',
  buono int(11) DEFAULT '0',
  molto_buono int(11) DEFAULT '0',
  non_applicabile int(11) DEFAULT '0',
  media double DEFAULT NULL,
  criterio varchar(100) DEFAULT NULL,
  PRIMARY KEY (valutazioni_corsi_id),
  KEY corso (corso),
  CONSTRAINT valutazioni_corsi_ibfk_1 FOREIGN KEY (corso) REFERENCES corsi (corso_id) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS valutazioni_utenti;
CREATE TABLE valutazioni_utenti (
  valutazioni_utenti_id int(11) NOT NULL AUTO_INCREMENT,
  corso int(11) DEFAULT NULL,
  dipendente int(11) DEFAULT NULL,
  criterio varchar(100) DEFAULT NULL,
  totalmente_insufficente int(11) DEFAULT '0',
  insufficente int(11) DEFAULT '0',
  sufficente int(11) DEFAULT '0',
  discreto int(11) DEFAULT '0',
  buono int(11) DEFAULT '0',
  ottimo int(11) DEFAULT '0',
  media double DEFAULT NULL,
  PRIMARY KEY (valutazioni_utenti_id),
  KEY corso (corso),
  KEY dipendente (dipendente),
  CONSTRAINT valutazioni_utenti_ibfk_1 FOREIGN KEY (corso) REFERENCES corsi (corso_id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT valutazioni_utenti_ibfk_2 FOREIGN KEY (dipendente) REFERENCES dipendenti (dipendente_id) ON DELETE CASCADE ON UPDATE CASCADE
);


INSERT INTO societa (societa_id, ragione_sociale, telefono, partita_iva) VALUES (null, 'Corvallis Spa', '0498434511','02070900283');
INSERT INTO societa (societa_id, ragione_sociale, telefono, partita_iva) VALUES (null, 'Foreign Company', '4008434698','56387924613');
INSERT INTO dipendenti (matricola, societa, cognome, nome, sesso, anno_conseguimento) VALUES (123, 1,'Lazic','Igor', 'maschio', 2019);
INSERT INTO dipendenti (matricola, societa, cognome, nome, sesso, anno_conseguimento) VALUES (234, 1, 'Maraga','Manuel', 'maschio', 2019);
INSERT INTO dipendenti (matricola, societa, cognome, nome, sesso, anno_conseguimento) VALUES (345, 1, 'Panarotto','Michele', 'maschio', 2019);
INSERT INTO dipendenti (matricola, societa, cognome, nome, sesso, anno_conseguimento) VALUES (456, 1, 'Manuc','Adrian', 'maschio', 2019);
INSERT INTO dipendenti (matricola, societa, cognome, nome, sesso, anno_conseguimento) VALUES (101, 1, 'Bernardi','Alessia', 'femmina', 2019);
INSERT INTO dipendenti (matricola, societa, cognome, nome, sesso, anno_conseguimento) VALUES (102, 1, 'Amato','Giacomo', 'maschio', 2019);
INSERT INTO dipendenti (matricola, societa, cognome, nome, sesso, anno_conseguimento) VALUES (678, 2, 'Ferrari','Luisa', 'femmina', 2008);
INSERT INTO dipendenti (matricola, societa, cognome, nome, sesso, anno_conseguimento) VALUES (789, 2, 'Bianchi','Nicolo', 'maschio', 2015);
INSERT INTO dipendenti (matricola, societa, cognome, nome, sesso, anno_conseguimento) VALUES (103, 2, 'Neri','Giorgio', 'maschio', 2015);
INSERT INTO dipendenti (matricola, societa, cognome, nome, sesso, anno_conseguimento) VALUES (104, 2, 'Rossetti','Chiara', 'femmina', 2018);

INSERT INTO docenti (titolo, ragione_sociale, indirizzo, localita, provincia, nazione, telefono, partita_iva) VALUES ('Dr', 'Martens', 'Via Europa 34', 'Monselice', 'Padova', 'Italia', '345963198', '25473198654');
INSERT INTO docenti (titolo, ragione_sociale, indirizzo, localita, provincia, nazione, telefono, partita_iva) VALUES ('Ingegner', 'Luigi', 'Via America 92', 'Abano Terme', 'Padova', 'Italia', '3478963214', '25473198611');

INSERT INTO tipi (tipo_id, descrizione) VALUES 
(1,'Java'),
(2,'Internet'),
(3,'Client-Server'),
(4,'Programmazione Mainframe'),
(5,'RPG'),
(6,'Cobol'),
(7,'Programmazione di Sistemi EDP'),
(8,'Object-Oriented'),
(9,'Sistema di Gestione Qualità'),
(10,'Privacy'),
(11,'Analisi e Programmazione'),
(12,'Programmazione PC'),
(13,'Project Management'),
(14,'Programmazione'),
(15,'CICS'),
(16,'DB2'),
(17,'Linguaggio C'),
(18,'Data Base'),
(19,'Introduzione all''Informatica'),
(20,'Office'),
(21,'JCL'),
(22,'Marketing'),
(23,'Oracle'),
(24,'Formazione Sicurezza (626/94; 81/2008)'),
(25,'Inglese'),
(26,'Amministrazione/Finanza'),
(27,'Windows'),
(28,'Word'),
(29,'Office'),
(30,'Excel'),
(31,'Easytrieve'),
(32,'Comunicazione'),
(33,'Access'),
(34,'Formazione per Formatori'),
(35,'Tecnica Bancaria'),
(36,'Lotus Notes'),
(37,'Visualage'),
(38,'Websphere'),
(39,'Reti/Elettronica'),
(40,'SAS'),
(41,'UML'),
(42,'ViaSoft'),
(43,'Visual Basic'),
(44,'Sistemi Informativi'),
(45,'Cartografia Vettoriale'),
(46,'Cartografia Raster'),
(47,'Gestione Elettronica Documenti-Modulistica'),
(48,'Territorio e Ambiente'),
(49,'Sistemi Informativi Territoriali'),
(50,'Editoria'),
(51,'VARIE'),
(52,'AS/400'),
(53,'Gestione Progetti'),
(55,'Organizzazione'),
(56,'Funzionale Normativo'),
(57,'Funzionale'),
(58,'Manageriale'),
(60,'Metodologia'),
(61,'Prodotti'),
(62,'Business Object'),
(63,'Sistemi di Business Intelligence'),
(64,'Formazione Apprendisti'),
(66,'Primavera'),
(67,'XML'),
(68,'Cattolica – Servizi in ambito Sinistri'),
(69,'Ambito Assicurativo'),
(70,'Gestione della Sicurezza delle Informazioni (SGSI)'),
(71,'PHP'),
(72,'CMS/E-commerce'),
(73,'Microsoft Azure'),
(75,'Eurotech IoT'),
(79,'Infrastrutture'),
(80,'Infrastrutture'),
(81,'Android'),
(82,'SQL'),
(83,'Risorse Umane'),
(74,'Middleware - Sistemi Distribuiti'),
(76,'Blockchain'),
(77,'Angular'),
(78,'Soft Skill');

INSERT INTO tipologie (tipologia_id, descrizione) VALUES 
(1,'Ingresso'),
(2,'Aggiornamento'),
(3,'Seminario');

INSERT INTO misure (misura_id, descrizione) VALUES 
(1,'Ore'),
(2,'Giorni');

INSERT INTO corsi (tipo, tipologia, descrizione, edizione, previsto, erogato, durata, misura, tipo_id) VALUES (1, 1, 'Java', 'Boh', 1,1,1,1,1);
INSERT INTO corsi (tipo, tipologia, descrizione, edizione, previsto, erogato, durata, misura, tipo_id) VALUES (1, 1, 'Phyton', 'Boh', 2,1,1,1,1);
INSERT INTO corsi (tipo, tipologia, descrizione, edizione, previsto, erogato, durata, misura, tipo_id) VALUES (1, 1, 'Spring', 'Boh', 2,1,1,1,1);

INSERT INTO corsi_docenti (corso, docente, interno) VALUES (1, 1, 0);
INSERT INTO corsi_docenti (corso, docente, interno) VALUES (2, 2, 0);
INSERT INTO corsi_docenti (corso, docente, interno) VALUES (3, 1, 1);

INSERT INTO corsi_utenti (corso, dipendente, durata) VALUES (1, 1, 3);
INSERT INTO corsi_utenti (corso, dipendente, durata) VALUES (2, 2, 3);
INSERT INTO corsi_utenti (corso, dipendente, durata) VALUES (3, 3, 3);
